webpackJsonp([4],{

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPageModule", function() { return WalletPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__wallet__ = __webpack_require__(529);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var WalletPageModule = /** @class */ (function () {
    function WalletPageModule() {
    }
    WalletPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__wallet__["a" /* WalletPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__wallet__["a" /* WalletPage */]),
            ],
        })
    ], WalletPageModule);
    return WalletPageModule;
}());

//# sourceMappingURL=wallet.module.js.map

/***/ }),

/***/ 529:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WalletPage = /** @class */ (function () {
    function WalletPage(navCtrl, navParams, menu, iab, apiCall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.iab = iab;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.showbutton = false;
        this.orderID = "1fgddfasf45445fdgdfg3dfsfsdew";
        this.hidePaybutton = true;
        this.showCoupenDiv = false;
        this.buttonStr = "Pay";
        this.disablehideMoney = true;
        this.toast = this.toastCtrl.create({
            duration: 5000,
            position: 'middle'
        });
        this.tempfunc = function () {
            var responseMsg = "Inside function";
            return responseMsg;
        };
        this.paytmlowbalance = this.navParams.get("lowBalance");
        this.menu.enable(true);
    }
    WalletPage.prototype.ionViewDidLoad = function () {
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        this.getPaytmBalance(userId);
        this.amounttobepaid = localStorage.getItem('tripFare') ? JSON.parse(localStorage.getItem('tripFare')) : 0;
        this.paytmregNum = localStorage.getItem('paytmregNum');
    };
    WalletPage.prototype.amount = function (cash) {
        this.disablehideMoney = false;
        this.enteredAmt = cash;
    };
    WalletPage.prototype.checkAmount = function (entrdamt) {
        if (entrdamt) {
            this.disablehideMoney = false;
        }
        else {
            this.disablehideMoney = true;
        }
    };
    WalletPage.prototype.changeNum = function () {
        this.navCtrl.push("PaytmwalletloginPage", {
            "chnagenumParam": "chnagenumParam"
        });
    };
    WalletPage.prototype.addMoney = function () {
        var _this = this;
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        this.apiCall.startLoading();
        var addAmountObj = {
            "CUST_ID": userId,
            "ORDER_ID": this.orderID,
            "TXN_AMOUNT": this.enteredAmt,
            "app_id": "zogo"
        };
        this.apiCall.addMoneyPaytm(addAmountObj)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            console.log(res);
            var resTemplate = res['_body'];
            _this.InappBrowserWindow(resTemplate);
        }, function (err) {
            _this.apiCall.stopLoading();
            _this.toast['message'] = "Server error on adding amount";
            _this.toast.present();
        });
    };
    WalletPage.prototype.InappBrowserWindow = function (ress) {
        var outerThis = this;
        var pageContentUrl = 'data:text/html;base64,' + btoa(ress);
        var browser = this.iab.create(pageContentUrl, '_self');
        browser.on('loadstop').subscribe(function (event) {
            console.log(event);
            if (event.url == 'https://www.oneqlik.in/paytm/callback') {
                console.log("Inside paytm callback");
                browser.close();
                outerThis.chacktransectionStatus();
            }
        });
    };
    WalletPage.prototype.chacktransectionStatus = function () {
        var _this = this;
        this.apiCall.startLoading();
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        this.apiCall.paytmbalance(userId)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            var amountAfterCashAdded = res.response.amount;
            var statusCode = res.statusCode;
            _this.paytmAmount += amountAfterCashAdded;
            if (amountAfterCashAdded > _this.paytmAmount) {
                _this.toast.setMessage("Amount added successfuly");
                if (localStorage.getItem("navigationFrom") == "NotRegisteredWithPaytm") {
                    _this.navCtrl.setRoot("DropLocationPage");
                }
                _this.toast.present();
            }
            else if (amountAfterCashAdded == _this.paytmAmount) {
                _this.toast.setMessage("Transection failure ");
                _this.toast.present();
            }
            else {
                _this.toast.setMessage("Transection failure");
                _this.toast.present();
            }
        }, function (err) {
            _this.toast.setMessage("Server Error while checking balance");
            _this.toast.present();
            _this.apiCall.stopLoading();
        });
    };
    WalletPage.prototype.promocodeFunc = function () {
        this.showCoupenDiv = true;
    };
    WalletPage.prototype.applyCode = function () {
        var _this = this;
        if (this.promocodeentered != undefined) {
            var useDetails = JSON.parse(localStorage.getItem('details'));
            var userId = useDetails._id;
            this.apiCall.startLoading();
            this.apiCall.promocodeService(userId, this.amounttobepaid, this.orderID, this.promocodeentered)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                var that = _this;
                if (data.status == 'available') {
                    that.imp_id = data._id;
                    if (data.isPercent) {
                        that.amounttobepaid = that.amounttobepaid - ((data.amount / 100) * that.amounttobepaid);
                    }
                    else {
                        if (that.amounttobepaid >= data.amount) {
                            that.amounttobepaid = that.amounttobepaid - data.amount;
                        }
                        else {
                            if (that.amounttobepaid < data.amount) {
                                that.amounttobepaid = 0;
                            }
                        }
                    }
                    that.showCoupenDiv = false;
                    var toast = _this.toastCtrl.create({
                        message: "Code applied successfully.",
                        duration: 2000,
                        position: 'bottom'
                    });
                    toast.present();
                    console.log("Amount to be paid", _this.amounttobepaid);
                    if (_this.amounttobepaid == 0) {
                        _this.buttonStr = "Proceed";
                        _this.promocodePassCode = false;
                    }
                    if (_this.amounttobepaid > _this.paytmAmount) {
                        _this.disablehideMoney = false;
                    }
                    else {
                        _this.disablehideMoney = true;
                        // this.promocodePassCode = false;
                    }
                }
                else {
                    if (data.status == 'No code found') {
                        var toast = _this.toastCtrl.create({
                            message: "Invalid code",
                            duration: 2000,
                            position: 'bottom'
                        });
                        toast.present();
                        toast.onDidDismiss(function () {
                            that.showCoupenDiv = false;
                        });
                    }
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                console.log("error found=> ", err);
            });
        }
        else {
            var toast = this.toastCtrl.create({
                message: 'Please enter valid coupen code',
                duration: 2500,
                position: 'bottom'
            });
            toast.present();
        }
    };
    // ==================================================================================
    // method added after removing preauth==================================================
    WalletPage.prototype.releaseAMT = function () {
        var that = this;
        if (that.amounttobepaid == 0) {
            this.relAMT();
        }
        else {
            that.navCtrl.push('PaymentSecurePage', {
                'amtPayble': that.amounttobepaid,
                "imp_id": that.imp_id
            });
        }
    };
    WalletPage.prototype.relAMT = function () {
        var _this = this;
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        var releaseObj = {
            CUST_ID: userId,
            app_id: 'zogo'
        };
        this.apiCall.startLoading();
        this.apiCall.releaseAmount(releaseObj)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            if (res.STATUS == "TXN_SUCCESS") {
                _this.proceedSecurely();
            }
            else {
                _this.toasterror1("try aghain");
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            _this.toasterror1("try aghain");
        });
    };
    WalletPage.prototype.toasterror1 = function (errstatus) {
        var toastCtrl1 = this.toastCtrl.create({
            duration: 3000,
            position: 'bottom',
            cssClass: 'toastStyle'
        });
        toastCtrl1['message'] = errstatus;
        return toastCtrl1.present();
    };
    WalletPage.prototype.proceedSecurely = function () {
        var _this = this;
        this.paytmregNum = localStorage.getItem('paytmregNum');
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        var toastCtrl = this.toastCtrl.create({
            message: "Trip amount paid, Thanks for using zogo ride !!",
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var toastCtrlfailure = this.toastCtrl.create({
            message: "Payment failed , Try again",
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var updatePaymentStatus = this.toastCtrl.create({
            message: "Unable to update Payment Status ",
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        this.apiCall.startLoading();
        this.apiCall.updateTripStatus(userId, this.imp_id, "payment", "0").subscribe(function (res) {
            _this.apiCall.stopLoading();
            if (res.payment_status == true) {
                toastCtrl.present();
                toastCtrl.onDidDismiss(function () {
                    localStorage.removeItem('flag');
                    _this.navCtrl.setRoot('FeedbackPage');
                });
            }
            else {
                updatePaymentStatus.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("updateTripStatus: ", err);
        });
    };
    // ===========================================================================
    WalletPage.prototype.toasterror = function (errstatus) {
        var toastCtrl1 = this.toastCtrl.create({
            duration: 3000,
            position: 'bottom',
            cssClass: 'toastStyle'
        });
        toastCtrl1['message'] = errstatus;
        return toastCtrl1.present();
    };
    WalletPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    WalletPage.prototype.getPaytmBalance = function (uId) {
        var _this = this;
        var toast_validation = this.toastCtrl.create({
            duration: 3000,
            position: 'middle'
        });
        this.apiCall.startLoading();
        this.apiCall.paytmbalance(uId)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            if (_this.paytmlowbalance == "addAmt") {
                console.log("localstorage", _this.paytmlowbalance);
                _this.hidePaybutton = true;
            }
            else if (res.code == "ETIMEDOUT") {
                toast_validation.setMessage("Etimeout server error");
                toast_validation.present();
            }
            else {
                console.log("Inside");
                _this.paytmAmount = res.response.amount;
                if (Number(_this.amounttobepaid) < Number(_this.paytmAmount)) {
                    _this.promocodePassCode = false;
                }
                else {
                    _this.promocodePassCode = true;
                }
                if ((Number(_this.paytmAmount) != 0) && (Number(_this.amounttobepaid) < Number(_this.paytmAmount))) {
                    _this.hidePaybutton = false;
                }
            }
        });
    };
    WalletPage.prototype.getBlockedAmt = function () {
        var _this = this;
        this.apiCall.startLoading();
        this.apiCall.getblockamt()
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            _this.blockedAmt = res['Block Amount'];
            if (_this.paytmAmount < _this.blockedAmt) {
                _this.hideAddbutton = true;
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    WalletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-wallet',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/wallet/wallet.html"*/'<ion-header no-border>\n\n    <ion-navbar color="custCol"></ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding>\n\n    <div width="100%" height="100%">\n\n            <div style="background: #8eda4a;" height="50%">\n\n                    <ion-card style="margin: 0px;width: 28%;border-radius: 15px; padding-top: 0%;margin-left: 37%;">\n\n                        <img src="assets/icon/zogologo.png" style="width: 60%;margin: 18%;">\n\n                    </ion-card>\n\n                    <h5 style="color: white;font-weight: 500;text-align: center;font-size: 2.0rem;padding-top: 0%;padding-bottom: 7%;">PAYTM WALLET</h5>\n\n                </div>\n\n    <ion-grid>\n\n        <p style="text-align: center">Available balance\n\n            <span style="margin-left: 2%;">{{paytmAmount}}</span>\n\n        </p>\n\n        <ion-row style="margin: 8px 30px 0px 30px;">\n\n            <ion-col col-8 no-padding>\n\n                <ion-item style="padding: 0">\n\n                    <ion-label color="primary" floating>Trip Fare</ion-label>\n\n                    <ion-input type="text" disabled="true" [(ngModel)]="amounttobepaid">\n\n                        <i class="fa fa-rupee"></i>\n\n                    </ion-input>\n\n\n\n                </ion-item>\n\n            </ion-col>\n\n            <ion-col col-4 no-padding style="text-align: right;margin-top: 7%;color: gray;">\n\n                <button ion-button style="width: 90px;background: #30a0ff;color:white;" (click)="releaseAMT()"\n\n                    [disabled]="promocodePassCode">{{buttonStr}}</button>\n\n            </ion-col>\n\n        </ion-row>\n\n        <ion-row style="margin: 8px 30px 0px 30px;">\n\n            <p style="color: rgb(247, 156, 156);" (tap)="promocodeFunc()">Do you have Promo Code?</p>\n\n        </ion-row>\n\n        <ion-row style="margin: 8px 30px 0px 30px;" *ngIf="showCoupenDiv">\n\n            <ion-col col-8 no-padding>\n\n                <ion-item style="padding: 0">\n\n                    <ion-label color="primary" floating>Promo Code</ion-label>\n\n                    <ion-input type="text" [(ngModel)]="promocodeentered">\n\n                        <i class="fa fa-rupee"></i>\n\n                    </ion-input>\n\n                </ion-item>\n\n            </ion-col>\n\n            <ion-col col-4 no-padding style="text-align: right;margin-top: 7%;color: gray;">\n\n                <button ion-button style="width: 90px;background: #30a0ff;color:white;"\n\n                    (click)="applyCode()">Apply</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n    <!-- <ion-row style="padding: 0px 36px 14px 32px;">\n\n        <ion-col col-8 no-padding style="text-align: center;color: #867e7e;margin-top: 15px;">\n\n            <ion-item style="padding: 0">\n\n                <ion-label color="primary" floating>Phone Number</ion-label>\n\n                <ion-input type="number" disabled="true" [(ngModel)]="paytmregNum">\n\n                    <i class="fa fa-rupee"></i>\n\n                </ion-input>\n\n            </ion-item>\n\n        </ion-col>\n\n        <ion-col col-4 no-padding style="text-align: right;margin-top: 12%;color: gray;">\n\n            <button ion-button style="width: 90px;background: #30a0ff;color:white;"\n\n                (click)="changeNum()">Change</button>\n\n        </ion-col>\n\n    </ion-row> -->\n\n    <div *ngIf="hidePaybutton">\n\n        <ion-row style="padding: 0px 23px;">\n\n            <ion-col col-8>\n\n                <ion-item style="padding-left: 3px;">\n\n                    <ion-input type="number" [(ngModel)]="enteredAmt" placeholder="Enter Amount" (ionChange)="checkAmount(enteredAmt)"></ion-input>\n\n                </ion-item>\n\n            </ion-col>\n\n        </ion-row>\n\n        <ion-row style="padding:10px;">\n\n            <ion-col col-4>\n\n                <button ion-button round color="light" (click)="amount(100)"\n\n                    style="color: #000;background-color: #f4f4f4;border-radius:0px;">\n\n                    +\n\n                    <i class="fa fa-rupee"></i> 100 </button>\n\n            </ion-col>\n\n            <ion-col col-4>\n\n                <button ion-button round color="light" (click)="amount(200)"\n\n                    style="margin-left:5px;color: #000;background-color: #f4f4f4;border-radius:0px;">\n\n                    +\n\n                    <i class="fa fa-rupee"></i> 200 </button>\n\n            </ion-col>\n\n            <ion-col col-4>\n\n                <button ion-button round color="light" (click)="amount(500)"\n\n                    style="margin-left:5px;color: #000;background-color: #f4f4f4;border-radius:0px;">+\n\n                    <i class="fa fa-rupee"></i> 500 </button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </div>\n\n</div>\n\n</ion-content>\n\n<ion-footer style="width: 90%;\n\nheight: 100px;\n\nbackground: #ffffff;\n\nmargin-left: 5%;\n\nborder-top-left-radius: 8px;\n\nborder-top-right-radius: 8px;\n\nbox-shadow: 0px 1px 15px 3px #cfcfcf;" *ngIf="hidePaybutton">\n\n    <ion-row style="margin-top: 22px;width: 90%;margin-left: 5%;">\n\n        <button [disabled]="disablehideMoney" ion-button full style="margin: 0%;height:50px;background:#87c23f;font-size: 14px;border-radius: 6px;"\n\n            (click)="addMoney()">Add Money</button>\n\n    </ion-row>\n\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/wallet/wallet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"], __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], WalletPage);
    return WalletPage;
}());

// http://localhost:3000/paytm/preAuth
// {
// "CUST_ID":"5bc86c5285303603e1046b27",
// "ORDER_ID":"465213434335345332434366523123647",
// "TXN_AMOUNT":"23.00",
// "DURATIONHRS":"70"
// }
//# sourceMappingURL=wallet.js.map

/***/ })

});
//# sourceMappingURL=4.js.map