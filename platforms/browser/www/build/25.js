webpackJsonp([25],{

/***/ 306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppEntryPageModule", function() { return AppEntryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_entry__ = __webpack_require__(505);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppEntryPageModule = /** @class */ (function () {
    function AppEntryPageModule() {
    }
    AppEntryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_entry__["a" /* AppEntryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__app_entry__["a" /* AppEntryPage */]),
            ],
        })
    ], AppEntryPageModule);
    return AppEntryPageModule;
}());

//# sourceMappingURL=app-entry.module.js.map

/***/ }),

/***/ 505:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppEntryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppEntryPage = /** @class */ (function () {
    function AppEntryPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AppEntryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppEntryPage');
    };
    AppEntryPage.prototype.signUpPage = function () {
        this.navCtrl.push('SignupPage');
    };
    AppEntryPage.prototype.loginPage = function () {
        this.navCtrl.push('LoginPage');
    };
    AppEntryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-entry',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/app-entry/app-entry.html"*/'<ion-content padding>\n\n  <div class="applogo">\n\n    <img src="assets/icon/zogologo.png" style="width:40%;margin-left: 30%;">\n\n    <div style="padding-top: 20%;text-align: center;">\n\n      <button ion-button round style="width: 18.2rem;background-color: #78b242;" (click)="signUpPage()">Sign Up</button>\n\n    </div>\n\n    <div style="padding-top: 4%;text-align: center;">\n\n      <button ion-button round outline style="width: 18.2rem;color: #70aa3c;border-color: #70a83b;"\n\n        (click)="loginPage()">Sign In</button>\n\n    </div>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/app-entry/app-entry.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], AppEntryPage);
    return AppEntryPage;
}());

//# sourceMappingURL=app-entry.js.map

/***/ })

});
//# sourceMappingURL=25.js.map