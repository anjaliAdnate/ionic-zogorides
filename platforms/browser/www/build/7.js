webpackJsonp([7],{

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPage2PageModule", function() { return SignupPage2PageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_page2__ = __webpack_require__(521);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SignupPage2PageModule = /** @class */ (function () {
    function SignupPage2PageModule() {
    }
    SignupPage2PageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__signup_page2__["a" /* SignupPage2Page */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__signup_page2__["a" /* SignupPage2Page */]),
            ],
        })
    ], SignupPage2PageModule);
    return SignupPage2PageModule;
}());

//# sourceMappingURL=signup-page2.module.js.map

/***/ }),

/***/ 521:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SignupPage2Page = /** @class */ (function () {
    function SignupPage2Page(navCtrl, navParams, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.DlType = [{
                value: 'dl',
                viewValue: "Driving License"
            }, {
                value: 'Adhar',
                viewValue: "Adhar Card"
            }, {
                value: 'PAN',
                viewValue: "PAN Card"
            }, {
                value: 'voterCard',
                viewValue: "Voter ID Card"
            }
        ];
        this.signupFormNew = formBuilder.group({
            DlNo: [""],
            Name: [""]
        });
    }
    SignupPage2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SignupPage2Page');
    };
    SignupPage2Page.prototype.continue = function () { };
    SignupPage2Page.prototype.gotoLogin = function () {
        this.navCtrl.setRoot("LoginPage");
    };
    SignupPage2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-signup-page2',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/signup-page2/signup-page2.html"*/'<ion-content no-padding class="no-scroll">\n\n  <div style="height:40%;background:#8eda4a;">\n\n    <ion-card\n\n      style="margin: 0px;width: 28%;border-radius: 15px;padding-top: 46ppx;position: fixed;margin-top: 30px;margin-left: 37%;margin-bottom: 20px;">\n\n      <img src="assets/icon/zogologo.png" style="width: 60%;margin: 18%;">\n\n    </ion-card>\n\n    <h5 style="margin: 0%;color: white;font-weight: 300;padding-top: 41%;text-align: center;font-size: 1.8rem;">Sign Up\n\n    </h5>\n\n  </div>\n\n  <div>\n\n    <div>\n\n      <ion-card style="margin-top: -21%;border-radius: 4%;height: 302px;">\n\n        <form [formGroup]="signupFormNew">\n\n          <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n\n            <ion-col no-padding>\n\n              <ion-select>\n\n                <ion-option *ngFor="let type of DlType">\n\n                  {{type.viewValue}}\n\n                </ion-option>\n\n              </ion-select>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n\n            <ion-col col-10 no-padding>\n\n              <ion-input placeholder="Enter Your Driving License No." formControlName="DlNo"></ion-input>\n\n            </ion-col>\n\n            <ion-col col-2 no-padding style="text-align: right;margin-top: 7%;color: gray;">\n\n              <ion-icon name="md-create"></ion-icon>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n\n            <ion-col col-10 no-padding>\n\n              <ion-input placeholder="Name as on Driving License*" formControlName="Name"></ion-input>\n\n            </ion-col>\n\n            <ion-col col-2 no-padding style="text-align: right;margin-top: 7%;color: gray;">\n\n              <ion-icon name="person"></ion-icon>\n\n            </ion-col>\n\n          </ion-row>\n\n        </form>\n\n      </ion-card>\n\n    </div>\n\n    <div style="margin: -30px 0px 0px 95px;">\n\n      <button ion-button round\n\n        style="width: 18.2rem;color:white;background: #8eda4a;font-weight:400;text-transform: none;"\n\n        (click)="continue()">Continue</button>\n\n      <div (click)="gotoLogin()">\n\n        <p style="color:grey;margin-left: -13px;">Skip</p>\n\n      </div>\n\n    </div>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/signup-page2/signup-page2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]])
    ], SignupPage2Page);
    return SignupPage2Page;
}());

//# sourceMappingURL=signup-page2.js.map

/***/ })

});
//# sourceMappingURL=7.js.map