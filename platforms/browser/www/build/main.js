webpackJsonp([26],{

/***/ 114:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 114;

/***/ }),

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/app-entry/app-entry.module": [
		306,
		25
	],
	"../pages/bike-issue/bike-issue.module": [
		307,
		24
	],
	"../pages/bookRide/book-ride.module": [
		330,
		23
	],
	"../pages/confirm-booking/confirm-booking.module": [
		331,
		1
	],
	"../pages/drivers-rep/drivers-rep.module": [
		309,
		0
	],
	"../pages/driving-license/driving-license.module": [
		308,
		22
	],
	"../pages/drop-location-search/drop-location-search.module": [
		310,
		21
	],
	"../pages/drop-location/drop-location.module": [
		326,
		20
	],
	"../pages/feedback/feedback.module": [
		311,
		2
	],
	"../pages/live/live.module": [
		327,
		19
	],
	"../pages/lock-issue/lock-issue.module": [
		312,
		18
	],
	"../pages/login/login.module": [
		313,
		17
	],
	"../pages/my-profile/my-profile.module": [
		314,
		16
	],
	"../pages/payment-greeting/payment-greeting.module": [
		318,
		15
	],
	"../pages/payment-secure/payment-secure.module": [
		315,
		14
	],
	"../pages/paytmwalletlogin/paytmwalletlogin.module": [
		329,
		13
	],
	"../pages/pickup-location-search/pickup-location-search.module": [
		316,
		12
	],
	"../pages/report-misuse/report-misuse.module": [
		317,
		11
	],
	"../pages/ride-Details/rideDetails.module": [
		322,
		10
	],
	"../pages/ride-billing/ride-billing.module": [
		319,
		9
	],
	"../pages/ride-total-amount/ride-total-amount.module": [
		321,
		8
	],
	"../pages/signup-page2/signup-page2.module": [
		320,
		7
	],
	"../pages/signup/signup.module": [
		323,
		6
	],
	"../pages/trip/trip.module": [
		324,
		5
	],
	"../pages/wallet/wallet.module": [
		328,
		4
	],
	"../pages/zogomodal/zogomodal.module": [
		325,
		3
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 158;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuContentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__ = __webpack_require__(292);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Angular
 // tslint:disable-line
// Ionic


// This class is defined in this file because
// we don't want to make it exportable
var InnerMenuOptionModel = /** @class */ (function () {
    function InnerMenuOptionModel() {
    }
    InnerMenuOptionModel.fromMenuOptionModel = function (option, parent) {
        var innerMenuOptionModel = new InnerMenuOptionModel();
        innerMenuOptionModel.id = this.counter++;
        innerMenuOptionModel.iconName = option.iconName;
        innerMenuOptionModel.displayText = option.displayText;
        innerMenuOptionModel.badge = option.badge;
        innerMenuOptionModel.targetOption = option;
        innerMenuOptionModel.parent = parent || null;
        innerMenuOptionModel.selected = option.selected;
        if (option.suboptions) {
            innerMenuOptionModel.expanded = false;
            innerMenuOptionModel.suboptionsCount = option.suboptions.length;
            innerMenuOptionModel.subOptions = [];
            option.suboptions.forEach(function (subItem) {
                var innerSubItem = InnerMenuOptionModel.fromMenuOptionModel(subItem, innerMenuOptionModel);
                innerMenuOptionModel.subOptions.push(innerSubItem);
                // Select the parent if any
                // child option is selected
                if (subItem.selected) {
                    innerSubItem.parent.selected = true;
                    innerSubItem.parent.expanded = true;
                }
            });
        }
        return innerMenuOptionModel;
    };
    InnerMenuOptionModel.counter = 1;
    return InnerMenuOptionModel;
}());
var SideMenuContentComponent = /** @class */ (function () {
    function SideMenuContentComponent(platform, eventsCtrl, cdRef) {
        var _this = this;
        this.platform = platform;
        this.eventsCtrl = eventsCtrl;
        this.cdRef = cdRef;
        this.collapsableItems = [];
        // Outputs: return the selected option to the caller
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        // Handle the redirect event
        this.eventsCtrl.subscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */], function (data) {
            _this.updateSelectedOption(data);
        });
    }
    Object.defineProperty(SideMenuContentComponent.prototype, "options", {
        set: function (value) {
            var _this = this;
            if (value) {
                // Keep a reference to the options
                // sent to this component
                this.menuOptions = value;
                this.collapsableItems = new Array();
                // Map the options to our internal models
                this.menuOptions.forEach(function (option) {
                    var innerMenuOption = InnerMenuOptionModel.fromMenuOptionModel(option);
                    _this.collapsableItems.push(innerMenuOption);
                    // Check if there's any option marked as selected
                    if (option.selected) {
                        _this.selectedOption = innerMenuOption;
                    }
                    else if (innerMenuOption.suboptionsCount) {
                        innerMenuOption.subOptions.forEach(function (subItem) {
                            if (subItem.selected) {
                                _this.selectedOption = subItem;
                            }
                        });
                    }
                });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "settings", {
        set: function (value) {
            if (value) {
                this.menuSettings = value;
                this.mergeSettings();
            }
        },
        enumerable: true,
        configurable: true
    });
    SideMenuContentComponent.prototype.ngOnDestroy = function () {
        this.eventsCtrl.unsubscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */]);
    };
    // ---------------------------------------------------
    // PUBLIC methods
    // ---------------------------------------------------
    // Send the selected option to the caller component
    SideMenuContentComponent.prototype.select = function (option) {
        if (this.menuSettings.showSelectedOption) {
            this.setSelectedOption(option);
        }
        // Return the selected option (not our inner option)
        this.change.emit(option.targetOption);
    };
    // Toggle the sub options of the selected item
    SideMenuContentComponent.prototype.toggleItemOptions = function (targetOption) {
        if (!targetOption)
            return;
        // If the accordion mode is set to true, we need
        // to collapse all the other menu options
        if (this.menuSettings.accordionMode) {
            this.collapsableItems.forEach(function (option) {
                if (option.id !== targetOption.id) {
                    option.expanded = false;
                }
            });
        }
        // Toggle the selected option
        targetOption.expanded = !targetOption.expanded;
    };
    // Reset the entire menu
    SideMenuContentComponent.prototype.collapseAllOptions = function () {
        this.collapsableItems.forEach(function (option) {
            if (!option.selected) {
                option.expanded = false;
            }
            if (option.suboptionsCount) {
                option.subOptions.forEach(function (subItem) {
                    if (subItem.selected) {
                        // Expand the parent if any of
                        // its childs is selected
                        subItem.parent.expanded = true;
                    }
                });
            }
        });
        // Update the view since there wasn't
        // any user interaction with it
        this.cdRef.detectChanges();
    };
    Object.defineProperty(SideMenuContentComponent.prototype, "subOptionIndentation", {
        // Get the proper indentation of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.subOptionIndentation.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.subOptionIndentation.wp;
            return this.menuSettings.subOptionIndentation.md;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "optionHeight", {
        // Get the proper height of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.optionHeight.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.optionHeight.wp;
            return this.menuSettings.optionHeight.md;
        },
        enumerable: true,
        configurable: true
    });
    // ---------------------------------------------------
    // PRIVATE methods
    // ---------------------------------------------------
    // Method that set the selected option and its parent
    SideMenuContentComponent.prototype.setSelectedOption = function (option) {
        if (!option.targetOption.component)
            return;
        // Clean the current selected option if any
        if (this.selectedOption) {
            this.selectedOption.selected = false;
            this.selectedOption.targetOption.selected = false;
            if (this.selectedOption.parent) {
                this.selectedOption.parent.selected = false;
                this.selectedOption.parent.expanded = false;
            }
            this.selectedOption = null;
        }
        // Set this option to be the selected
        option.selected = true;
        option.targetOption.selected = true;
        if (option.parent) {
            option.parent.selected = true;
            option.parent.expanded = true;
        }
        // Keep a reference to the selected option
        this.selectedOption = option;
        // Update the view if needed since we may have
        // expanded or collapsed some options
        this.cdRef.detectChanges();
    };
    // Update the selected option
    SideMenuContentComponent.prototype.updateSelectedOption = function (data) {
        if (!data.displayText)
            return;
        var targetOption;
        this.collapsableItems.forEach(function (option) {
            if (option.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                targetOption = option;
            }
            else if (option.suboptionsCount) {
                option.subOptions.forEach(function (subOption) {
                    if (subOption.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                        targetOption = subOption;
                    }
                });
            }
        });
        if (targetOption) {
            this.setSelectedOption(targetOption);
        }
    };
    // Merge the settings received with the default settings
    SideMenuContentComponent.prototype.mergeSettings = function () {
        var defaultSettings = {
            accordionMode: false,
            optionHeight: {
                ios: 50,
                md: 50,
                wp: 50
            },
            arrowIcon: 'ios-arrow-down',
            showSelectedOption: false,
            selectedOptionClass: 'selected-option',
            indentSubOptionsWithoutIcons: false,
            subOptionIndentation: {
                ios: 16,
                md: 16,
                wp: 16
            }
        };
        if (!this.menuSettings) {
            // Use the default values
            this.menuSettings = defaultSettings;
            return;
        }
        if (!this.menuSettings.optionHeight) {
            this.menuSettings.optionHeight = defaultSettings.optionHeight;
        }
        else {
            this.menuSettings.optionHeight.ios = this.isDefinedAndPositive(this.menuSettings.optionHeight.ios) ? this.menuSettings.optionHeight.ios : defaultSettings.optionHeight.ios;
            this.menuSettings.optionHeight.md = this.isDefinedAndPositive(this.menuSettings.optionHeight.md) ? this.menuSettings.optionHeight.md : defaultSettings.optionHeight.md;
            this.menuSettings.optionHeight.wp = this.isDefinedAndPositive(this.menuSettings.optionHeight.wp) ? this.menuSettings.optionHeight.wp : defaultSettings.optionHeight.wp;
        }
        this.menuSettings.showSelectedOption = this.isDefined(this.menuSettings.showSelectedOption) ? this.menuSettings.showSelectedOption : defaultSettings.showSelectedOption;
        this.menuSettings.accordionMode = this.isDefined(this.menuSettings.accordionMode) ? this.menuSettings.accordionMode : defaultSettings.accordionMode;
        this.menuSettings.arrowIcon = this.isDefined(this.menuSettings.arrowIcon) ? this.menuSettings.arrowIcon : defaultSettings.arrowIcon;
        this.menuSettings.selectedOptionClass = this.isDefined(this.menuSettings.selectedOptionClass) ? this.menuSettings.selectedOptionClass : defaultSettings.selectedOptionClass;
        this.menuSettings.indentSubOptionsWithoutIcons = this.isDefined(this.menuSettings.indentSubOptionsWithoutIcons) ? this.menuSettings.indentSubOptionsWithoutIcons : defaultSettings.indentSubOptionsWithoutIcons;
        if (!this.menuSettings.subOptionIndentation) {
            this.menuSettings.subOptionIndentation = defaultSettings.subOptionIndentation;
        }
        else {
            this.menuSettings.subOptionIndentation.ios = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.ios) ? this.menuSettings.subOptionIndentation.ios : defaultSettings.subOptionIndentation.ios;
            this.menuSettings.subOptionIndentation.md = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.md) ? this.menuSettings.subOptionIndentation.md : defaultSettings.subOptionIndentation.md;
            this.menuSettings.subOptionIndentation.wp = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.wp) ? this.menuSettings.subOptionIndentation.wp : defaultSettings.subOptionIndentation.wp;
        }
    };
    SideMenuContentComponent.prototype.isDefined = function (property) {
        return property !== null && property !== undefined;
    };
    SideMenuContentComponent.prototype.isDefinedAndPositive = function (property) {
        return this.isDefined(property) && !isNaN(property) && property > 0;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('options'),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], SideMenuContentComponent.prototype, "options", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('settings'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SideMenuContentComponent.prototype, "settings", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], SideMenuContentComponent.prototype, "change", void 0);
    SideMenuContentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'side-menu-content',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/shared/side-menu-content/side-menu-content.component.html"*/'<ion-list no-margin no-lines>\n\n    <ng-template ngFor let-option [ngForOf]="collapsableItems" let-i="index">\n\n\n\n        <!-- It is a simple option -->\n\n        <ng-template [ngIf]="!option.suboptionsCount">\n\n            <ion-item class="option" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n\n                (tap)="select(option)" tappable>\n\n                <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon>\n\n                {{ option.displayText }}\n\n                <ion-badge item-right *ngIf="option.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n\n            </ion-item>\n\n        </ng-template>\n\n\n\n        <!-- It has nested options -->\n\n        <ng-template [ngIf]="option.suboptionsCount">\n\n\n\n            <ion-list no-margin class="accordion-menu">\n\n\n\n                <!-- Header -->\n\n                <ion-item class="header" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n\n                    (tap)="toggleItemOptions(option)" tappable><ion-icon [class.rotate]="option.expanded" class="header-icon" [name]="option.iconName || menuSettings.arrowIcon" item-left></ion-icon><!-- <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon> -->\n\n                    {{ option.displayText }} \n\n                </ion-item>\n\n\n\n                <!-- Sub items -->\n\n                <div [style.height]="option.expanded ? ((optionHeight + 1) * option.suboptionsCount) + \'px\' : \'0px\'" class="options">\n\n                    <ng-template ngFor let-item [ngForOf]="option.subOptions">\n\n                        <ion-item class="sub-option" [style.padding-left]="subOptionIndentation + \'px\'" [class.no-icon]="menuSettings?.indentSubOptionsWithoutIcons && !item.iconName"\n\n                            [ngClass]="menuSettings?.showSelectedOption && item.selected ? menuSettings.selectedOptionClass : null"\n\n                            tappable (tap)="select(item)">\n\n                            <ion-icon *ngIf="item.iconName" [name]="item.iconName" item-left></ion-icon>\n\n                            {{ item.displayText }}\n\n                            <ion-badge item-right *ngIf="item.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n\n                        </ion-item>\n\n                    </ng-template>\n\n                </div>\n\n            </ion-list>\n\n\n\n        </ng-template>\n\n\n\n    </ng-template>\n\n</ion-list>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/shared/side-menu-content/side-menu-content.component.html"*/,
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], SideMenuContentComponent);
    return SideMenuContentComponent;
}());

//# sourceMappingURL=side-menu-content.component.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuProvider = /** @class */ (function () {
    function MenuProvider(http) {
        this.http = http;
    }
    MenuProvider.prototype.getSideMenus = function () {
        return [{
                title: 'Home', component: 'DashboardPage', icon: 'home'
            }, {
                title: 'Groups', component: 'GroupsPage', icon: 'people'
            }, {
                title: 'Customers', component: 'CustomersPage', icon: 'contacts'
            }];
    };
    MenuProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
    ], MenuProvider);
    return MenuProvider;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ConnectionStatusEnum */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(204);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ConnectionStatusEnum;
(function (ConnectionStatusEnum) {
    ConnectionStatusEnum[ConnectionStatusEnum["Online"] = 0] = "Online";
    ConnectionStatusEnum[ConnectionStatusEnum["Offline"] = 1] = "Offline";
})(ConnectionStatusEnum || (ConnectionStatusEnum = {}));
var NetworkProvider = /** @class */ (function () {
    function NetworkProvider(alertCtrl, network, eventCtrl) {
        this.alertCtrl = alertCtrl;
        this.network = network;
        this.eventCtrl = eventCtrl;
        console.log('Hello NetworkProvider Provider');
        this.previousStatus = ConnectionStatusEnum.Online;
    }
    NetworkProvider.prototype.initializeNetworkEvents = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function () {
            if (_this.previousStatus === ConnectionStatusEnum.Online) {
                _this.eventCtrl.publish('network:offline');
            }
            _this.previousStatus = ConnectionStatusEnum.Offline;
        });
        this.network.onConnect().subscribe(function () {
            if (_this.previousStatus === ConnectionStatusEnum.Offline) {
                _this.eventCtrl.publish('network:online');
            }
            _this.previousStatus = ConnectionStatusEnum.Online;
        });
    };
    NetworkProvider.prototype.getNetworkType = function () {
        return this.network.type;
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=network.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OfflineModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OfflineModalPage = /** @class */ (function () {
    function OfflineModalPage(navparam, viewCtrl) {
        this.navparam = navparam;
        this.viewCtrl = viewCtrl;
    }
    OfflineModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-modal-offline',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/offline-modal/offline-modal.html"*/'<ion-content></ion-content>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/offline-modal/offline-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], OfflineModalPage);
    return OfflineModalPage;
}());

//# sourceMappingURL=offline-modal.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BookingDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BookingDetailComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var BookingDetailComponent = /** @class */ (function () {
    function BookingDetailComponent(params) {
        this.tripData = params.get('tripdata');
    }
    BookingDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'booking-detail',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/components/booking-detail/booking-detail.html"*/'<!-- Generated template for the BookingDetailComponent component -->\n\n<div class="mainStyle">\n\n  <!-- <p>Booking Detail</p> -->\n\n <div class="row">\n\n   <div col-7 style="padding-left: 20px;">\n\n     <p>Trip start at</p>\n\n     <p>Trip end at</p>\n\n     <p>Trip distance</p>\n\n     <p>Fare</p>\n\n     <p>Pickup station</p>\n\n     <p>Drop Station</p>\n\n     <p>Duration</p>\n\n   </div>\n\n   <div col-5>\n\n    <p>{{tripData.Actual_startTime }}</p>\n\n    <p>{{tripData.Actual_endTime }}</p>\n\n    <p>10 Kms</p>\n\n    <p>220 Rs.</p>\n\n    <p>{{tripData.Loading_Site }}</p>\n\n    <p>{{tripData.Unloading_Site }}</p>\n\n    <p>{{tripData.durations }}</p>\n\n    \n\n   </div>\n\n </div>\n\n</div>\n\n'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/components/booking-detail/booking-detail.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], BookingDetailComponent);
    return BookingDetailComponent;
}());

//# sourceMappingURL=booking-detail.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupOtp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_android_permissions__ = __webpack_require__(159);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// declare var SMS :any;
// declare var cordova: any;
var SignupOtp = /** @class */ (function () {
    function SignupOtp(navCtrl, navParams, androidPermissions, formBuilder, apiService, platform, toastCtrl, alerCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.androidPermissions = androidPermissions;
        this.formBuilder = formBuilder;
        this.apiService = apiService;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.viewCtrl = viewCtrl;
        this.mobileNum = localStorage.getItem('mobnum');
        this.signupForm = formBuilder.group({
            otp1: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            otp2: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            otp3: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            otp4: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    // ionViewWillEnter() {
    //     this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(   
    //       success => console.log('Permission granted'),
    //       err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
    //     );  
    //     this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);     
    //     this.autoOtpReader();
    //   }
    // autoOtpReader() {
    //     this.platform.ready().then((readySource) => {
    //         if (SMS) SMS.startWatch(() => {
    //             console.log('watching started');
    //         }, Error => {
    //             console.log('failed to start watching');
    //         });
    //         document.addEventListener('onSMSArrive', (e: any) => {
    //             var sms = e.data;
    //             console.log(sms);
    //             this.fetchOtp = e.data.body.match(/[0-9]+/g);
    //             console.log(this.fetchOtp);
    //             this.Otp = this.fetchOtp[0];
    //             console.log(this.Otp);
    //         })
    //     })
    // }
    SignupOtp.prototype.getCodeBoxElement = function (index) {
        var inputValue = document.getElementById('codeBox' + index);
        return inputValue;
    };
    SignupOtp.prototype.onKeyUpEvent = function (index, event) {
        var eventCode = event.which || event.keyCode;
        if (this.getCodeBoxElement(index).value.length === 1) {
            if (index !== 4) {
                this.getCodeBoxElement(index + 1).focus();
            }
            else {
                this.getCodeBoxElement(index).blur();
            }
        }
        if (eventCode === 8 && index !== 1) {
            this.getCodeBoxElement(index - 1).focus();
        }
    };
    SignupOtp.prototype.onFocusEvent = function (index) {
        for (var item = 1; item < index; item++) {
            var currentElement = this.getCodeBoxElement(item);
            if (!currentElement.value) {
                currentElement.focus();
                break;
            }
        }
    };
    SignupOtp.prototype.signupUser = function () {
        var _this = this;
        var otp = this.signupForm.value.otp1.toString() + this.signupForm.value.otp2.toString() + this.signupForm.value.otp3.toString() + this.signupForm.value.otp4.toString();
        var usersignup = {
            "phone": this.mobileNum,
            "otp": otp
        };
        this.apiService.startLoading();
        this.apiService.siginupverifyCall(usersignup)
            .subscribe(function (data) {
            _this.apiService.stopLoading();
            _this.signupverify = data;
            _this.signupverify.phoneNum = _this.mobileNum;
            var toast = _this.toastCtrl.create({
                message: 'Mobile Verified',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.navCtrl.push("DrivingLicensePage", { signupObj: _this.signupverify });
            });
            toast.present();
        }, function (err) {
            _this.apiService.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var toastErr = _this.toastCtrl.create({
                message: msg.message,
                position: 'top',
                duration: 2000
            });
            toastErr.present();
        });
    };
    SignupOtp.prototype.resendOtp = function () {
        var _this = this;
        var phoneNumber = {
            ph_num: this.mobileNum
        };
        this.apiService.resendOtp(phoneNumber)
            .subscribe(function (res) {
            if (res) {
                var toast = _this.toastCtrl.create({
                    message: "OTP sent successfully",
                    duration: 2000,
                    position: "top"
                });
                toast.present();
            }
        }, function (err) {
            console.log(err);
        });
    };
    SignupOtp.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    SignupOtp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-signup',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/signup/signupOtp.html"*/'<ion-header no-border>\n\n    <ion-navbar color="custCol">\n\n        <button ion-button menuToggle style="color:white;">\n\n            <ion-icon name="md-arrow-back"></ion-icon>\n\n        </button>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding class="no-scroll">\n\n    <div width="100%" height="100%">\n\n            <div style="background: #8eda4a;padding-bottom: 10%;" height="50%">\n\n                <ion-card style="margin: 0px;width: 28%;border-radius: 15px; padding-top: 0%;margin-left: 37%;">\n\n                    <img src="assets/icon/zogologo.png" style="width: 60%;margin: 18%;">\n\n                </ion-card>\n\n                <h5 style="color: white;font-weight: 500;text-align: center;font-size: 3.6rem;padding-top: 0%;padding-bottom: 7%;">\n\n                    SIGN UP</h5>\n\n            </div>\n\n    <!-- <div> -->\n\n        <div class="row">\n\n            <ion-card style="margin-top: -12%;border-radius: 4%;height: auto;" class="col-sm-12 col-12 col-md-12">\n\n                <p style="font-size: 1.7rem;margin: 25px;color:#605b5b">Verify Code</p>\n\n                <p style="margin: 25px;margin-bottom: 0px;color: grey;">Please type verification code send to your mobile no. {{mobileNum}}</p>\n\n                <form [formGroup]="signupForm">\n\n                    <!-- <ion-row style="width: 58%;height: 36px;margin: 27px 30px 42px 78px;">\n\n                        <ion-input type="number" maxlength="1" style="border: 1px solid #b6b2b2;margin-right: 15px;" formControlName="otp1"></ion-input>\n\n                        <ion-input type="number" maxlength="1" style="border: 1px solid #b6b2b2; margin-right: 15px;" formControlName="otp2"></ion-input>\n\n                        <ion-input type="number" maxlength="1" style="border: 1px solid #b6b2b2; margin-right: 15px;" formControlName="otp3"></ion-input>\n\n                        <ion-input type="number" maxlength="1" style="border: 1px solid #b6b2b2;" formControlName="otp4"></ion-input>\n\n                    </ion-row> -->\n\n                    <div class="otpBox">\n\n                        <input id="codeBox1" type="number" maxlength="1" (keyup)="onKeyUpEvent(1, $event)" (onfocus)="onFocusEvent(1)" formControlName="otp1"\n\n                        />\n\n                        <input id="codeBox2" type="number" maxlength="1" (keyup)="onKeyUpEvent(2, $event)" (onfocus)="onFocusEvent(2)" formControlName="otp2"\n\n                        />\n\n                        <input id="codeBox3" type="number" maxlength="1" (keyup)="onKeyUpEvent(3, $event)" (onfocus)="onFocusEvent(3)" formControlName="otp3"\n\n                        />\n\n                        <input id="codeBox4" type="number" maxlength="1" (keyup)="onKeyUpEvent(4, $event)" (onfocus)="onFocusEvent(4)" formControlName="otp4"\n\n                        />\n\n                    </div>\n\n                </form>\n\n            </ion-card>\n\n            <div class="col-sm-12 col-12 col-md-12 " style="margin:auto;text-align: center">\n\n                    <button ion-button round style="width: 18.2rem;color:white;background: #8eda4a;margin: -45px 0px 0px -12px;font-weight:400;text-transform: none;" [disabled]="!signupForm.valid" (click)="signupUser()">Submit</button>\n\n                    <div style="padding-top: 25px;">\n\n                        <p style="color:grey;margin-left: -13px;">OTP not recieved ?\n\n                            <span style="color: black;margin-left: 5px;" (click)="resendOtp()">Resend OTP</span>\n\n                        </p>\n\n                    </div>\n\n                </div>\n\n        </div>      \n\n    <!-- </div> -->\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/signup/signupOtp.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], SignupOtp);
    return SignupOtp;
}());

//# sourceMappingURL=signupOtp.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(241);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 241:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_transfer__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_network__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_network_interface__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_google_maps__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__node_modules_ion_bottom_drawer__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_geolocation__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_barcode_scanner__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_location_accuracy__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__ = __webpack_require__(303);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_components_module__ = __webpack_require__(304);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_booking_detail_booking_detail__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_android_permissions__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_sms__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_in_app_browser__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_social_sharing__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_file_path__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_file__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_camera__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_signup_signupOtp__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_api_service_api_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_network_network__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__providers_menu_menu__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_offline_modal_offline_modal__ = __webpack_require__(205);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_14__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */],
                __WEBPACK_IMPORTED_MODULE_28__pages_signup_signupOtp__["a" /* SignupOtp */],
                __WEBPACK_IMPORTED_MODULE_32__pages_offline_modal_offline_modal__["a" /* OfflineModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_7__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/app-entry/app-entry.module#AppEntryPageModule', name: 'AppEntryPage', segment: 'app-entry', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/bike-issue/bike-issue.module#BikeIssuePageModule', name: 'BikeIssuePage', segment: 'bike-issue', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/driving-license/driving-license.module#DrivingLicensePageModule', name: 'DrivingLicensePage', segment: 'driving-license', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/drivers-rep/drivers-rep.module#DriversRepPageModule', name: 'DriversRepPage', segment: 'drivers-rep', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/drop-location-search/drop-location-search.module#DropLocationSearchPageModule', name: 'DropLocationSearchPage', segment: 'drop-location-search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/feedback/feedback.module#FeedbackPageModule', name: 'FeedbackPage', segment: 'feedback', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/lock-issue/lock-issue.module#LockIssuePageModule', name: 'LockIssuePage', segment: 'lock-issue', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/my-profile/my-profile.module#MyProfilePageModule', name: 'MyProfilePage', segment: 'my-profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payment-secure/payment-secure.module#PaymentSecurePageModule', name: 'PaymentSecurePage', segment: 'payment-secure', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pickup-location-search/pickup-location-search.module#PickupLocationSearchPageModule', name: 'PickupLocationSearchPage', segment: 'pickup-location-search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/report-misuse/report-misuse.module#ReportMisusePageModule', name: 'ReportMisusePage', segment: 'report-misuse', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payment-greeting/payment-greeting.module#PaymentGreetingPageModule', name: 'PaymentGreetingPage', segment: 'payment-greeting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ride-billing/ride-billing.module#RideBillingPageModule', name: 'RideBillingPage', segment: 'ride-billing', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup-page2/signup-page2.module#SignupPage2PageModule', name: 'SignupPage2Page', segment: 'signup-page2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ride-total-amount/ride-total-amount.module#RideTotalAmountPageModule', name: 'RideTotalAmountPage', segment: 'ride-total-amount', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ride-Details/rideDetails.module#RideDetailsPageModule', name: 'RideDetailspage', segment: 'rideDetails', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip/trip.module#TripPageModule', name: 'TripPage', segment: 'trip', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/zogomodal/zogomodal.module#ZogomodalPageModule', name: 'ZogomodalPage', segment: 'zogomodal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/drop-location/drop-location.module#DropLocationPageModule', name: 'DropLocationPage', segment: 'drop-location', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/live.module#LivePageModule', name: 'LivePage', segment: 'live', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/wallet/wallet.module#WalletPageModule', name: 'WalletPage', segment: 'wallet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/paytmwalletlogin/paytmwalletlogin.module#PaytmwalletloginPageModule', name: 'PaytmwalletloginPage', segment: 'paytmwalletlogin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/bookRide/book-ride.module#BookRidesModule', name: 'BookRidesPage', segment: 'book-ride', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/confirm-booking/confirm-booking.module#ConfirmBookingPageModule', name: 'ConfirmBookingPage', segment: 'confirm-booking', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_13__node_modules_ion_bottom_drawer__["a" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_19__components_components_module__["a" /* ComponentsModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_20__components_booking_detail_booking_detail__["a" /* BookingDetailComponent */],
                __WEBPACK_IMPORTED_MODULE_28__pages_signup_signupOtp__["a" /* SignupOtp */],
                __WEBPACK_IMPORTED_MODULE_32__pages_offline_modal_offline_modal__["a" /* OfflineModalPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_2__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["IonicErrorHandler"] },
                __WEBPACK_IMPORTED_MODULE_29__providers_api_service_api_service__["a" /* ApiServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_30__providers_network_network__["a" /* NetworkProvider */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_network_interface__["a" /* NetworkInterface */],
                __WEBPACK_IMPORTED_MODULE_31__providers_menu_menu__["a" /* MenuProvider */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_google_maps__["a" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_google_maps__["d" /* Spherical */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_sms__["a" /* SMS */],
                __WEBPACK_IMPORTED_MODULE_0__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_0__ionic_native_transfer__["b" /* TransferObject */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_android_permissions__["a" /* AndroidPermissions */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_ReplaySubject__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_ReplaySubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_ReplaySubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_menu_menu__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_network_network__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_api_service_api_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_offline_modal_offline_modal__ = __webpack_require__(205);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, events, networkProvider, menuProvider, menuCtrl, modalCtrl, alertCtrl, app, appAPi, toast, keyboard) {
        // var s = 0;
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.events = events;
        this.networkProvider = networkProvider;
        this.menuProvider = menuProvider;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.appAPi = appAPi;
        this.toast = toast;
        this.keyboard = keyboard;
        this.sideMenuSettings = {
            accordionMode: true,
            showSelectedOption: true,
            selectedOptionClass: 'active-side-menu-option'
        };
        this.unreadCountObservable = new __WEBPACK_IMPORTED_MODULE_5_rxjs_ReplaySubject__["ReplaySubject"](0);
        this.events.subscribe('user:updated', function (udata) {
            _this.islogin = udata;
        });
        events.subscribe('menu:opened', function () {
            console.log("menu Opened");
            _this.menuStat = true;
        });
        events.subscribe('menu:closed', function () {
            console.log("menu closed");
            _this.menuStat = false;
        });
        platform.ready().then(function () {
            statusBar.styleDefault();
            statusBar.hide();
            _this.splashScreen.hide();
            _this.checkRideStatus();
        });
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.setsmsforotp = localStorage.getItem('setsms');
        this.getSideMenuData();
        this.initializeApp();
    }
    MyApp.prototype.menuClosed = function () {
        this.events.publish('menu:closed', '');
    };
    MyApp.prototype.menuOpened = function () {
        this.events.publish('menu:opened', '');
    };
    MyApp.prototype.getSideMenuData = function () {
        this.pages = this.menuProvider.getSideMenus();
    };
    MyApp.prototype.backBtnHandler = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () {
            var nav = _this.app.getActiveNavs()[0];
            var activeView = nav.getActive();
            if (activeView.name === "LivePage") {
                if (nav.canGoBack()) {
                    nav.pop();
                }
                else {
                    if (_this.menuStat) {
                        _this.menuCtrl.close();
                    }
                    else {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'App termination',
                            message: 'Do you want to close the app?',
                            buttons: [{
                                    text: 'Cancel',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Application exit prevented!');
                                    }
                                }, {
                                    text: 'Close App',
                                    handler: function () {
                                        _this.platform.exitApp(); // Close this application
                                    }
                                }]
                        });
                        alert_1.present();
                    }
                }
            }
            else {
                if (activeView.name === "SignupPage") {
                    nav.pop();
                }
                else {
                    if (activeView.name === "DrivingLicensePage") {
                        nav.pop();
                    }
                }
            }
        });
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        var that = this;
        that.platform.ready().then(function () {
            that.networkProvider.initializeNetworkEvents();
            // Offline event
            that.events.subscribe('network:offline', function () {
                var toast = that.toast.create({
                    message: "Network offline",
                    duration: 1500,
                    position: "bottom"
                });
                toast.present();
                // that.modalCtrl.create(OfflineModalPage);
                var data = { message: 'hello world' };
                _this.modalPage = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__pages_offline_modal_offline_modal__["a" /* OfflineModalPage */], data);
                _this.modalPage.present();
            });
            // Online event
            that.events.subscribe('network:online', function () {
                // this.viewCtrl.dismiss();
                _this.modalPage.dismiss();
            });
            that.backBtnHandler();
        });
        // Initialize some options
        that.initializeOptions();
        // Change the value for the batch every 5 seconds
        setInterval(function () {
            _this.unreadCountObservable.next(Math.floor(Math.random() * 10) + 1);
        }, 5000);
    };
    MyApp.prototype.checkRideStatus = function () {
        var _this = this;
        var uID = this.islogin._id;
        if (uID) {
            this.appAPi.checkRideStatus(uID)
                .subscribe(function (res) {
                debugger;
                var currentStatus = res.status;
                if (currentStatus == "COMPLETED") {
                    if (res.payment_status == false) {
                        _this.rootPage = "RideTotalAmountPage";
                    }
                    else if (res.payment_status == true) {
                        if (res.feedbackStatus == true) {
                            _this.rootPage = "LivePage";
                        }
                        else {
                            _this.rootPage = "FeedbackPage";
                        }
                    }
                }
                else if (currentStatus == "ASSIGNED") {
                    _this.rootPage = "BookRidesPage";
                }
                else if ((currentStatus == "LOADING") || (currentStatus == "TRANSIT")) {
                    _this.rootPage = "ConfirmBookingPage";
                }
                else if (localStorage.getItem("loginflag") == "loginflag") {
                    _this.rootPage = "LivePage";
                }
                else {
                    _this.rootPage = "AppEntryPage";
                }
            }, function (err) {
                console.log("error occured: ", err);
                if (localStorage.getItem("loginflag") == "loginflag") {
                    _this.rootPage = "LivePage";
                }
                else {
                    _this.rootPage = "AppEntryPage";
                }
            });
        }
        else {
            this.rootPage = "AppEntryPage";
        }
    };
    MyApp.prototype.initializeOptions = function () {
        this.options = new Array();
        this.options.push({
            iconName: 'map',
            displayText: 'Book a trip',
            component: 'LivePage'
        });
        this.options.push({
            iconName: 'map',
            displayText: 'My Trip',
            component: 'TripPage'
        });
        // this.options.push({
        //   iconName: 'map',
        //   displayText: 'Feedback',
        //   component: 'FeedbackPage'
        // });
    };
    MyApp.prototype.onOptionSelected = function (option) {
        var _this = this;
        this.menuCtrl.close().then(function () {
            if (option.custom && option.custom.isLogin) {
                _this.presentAlert('You\'ve clicked the login option!');
            }
            else if (option.custom && option.custom.isLogout) {
                _this.presentAlert('You\'ve clicked the logout option!');
            }
            else if (option.custom && option.custom.isExternalLink) {
                var url = option.custom.externalUrl;
                window.open(url, '_blank');
            }
            else {
                var params = option.custom && option.custom.param;
                _this.nav.setRoot(option.component, params);
            }
        });
    };
    MyApp.prototype.collapseMenuOptions = function () {
        this.sideMenu.collapseAllOptions();
    };
    MyApp.prototype.presentAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: 'Information',
            message: message,
            buttons: ['Ok']
        });
        alert.present();
    };
    MyApp.prototype.openPage = function (page, index) {
        if (page.component) {
            this.nav.setRoot(page.component);
            this.menuCtrl.close();
        }
        else {
            if (this.selectedMenu) {
                this.selectedMenu = 0;
            }
            else {
                this.selectedMenu = index;
            }
        }
    };
    MyApp.prototype.logout = function () {
        localStorage.clear();
        localStorage.setItem('count', null);
        this.menuCtrl.close();
        this.nav.setRoot("LoginPage");
    };
    MyApp.prototype.gotoUserPage = function () {
        this.nav.push("MyProfilePage");
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */])
    ], MyApp.prototype, "sideMenu", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-main-page',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/app/app.html"*/'<ion-menu [content]="content" [swipeEnabled]="false" (ionOpen)="menuOpened()" (ionClose)="menuClosed()">\n\n  <ion-header>\n\n    <!-- <ion-navbar style="background-image: linear-gradient(to right top, #d80622, #c0002d, #a60033, #8b0935, #6f1133);"> -->\n\n    <div class="headProf" (click)="gotoUserPage()">\n\n      <img src="assets/imgs/dummy-user-img.png">\n\n      <div>\n\n        <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n\n        <p style="font-size: 12px">\n\n          <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}</p>\n\n        <p style="font-size: 12px">\n\n          <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}</p>\n\n      </div>\n\n    </div>\n\n    <!-- </ion-navbar> -->\n\n  </ion-header>\n\n\n\n  <ion-content id=outerNew>\n\n    <!-- side menu content -->\n\n    <side-menu-content [settings]="sideMenuSettings" [options]="options" (change)="onOptionSelected($event)"></side-menu-content>\n\n    <!-- <ion-list no-lines>\n\n      <ion-item *ngFor="let p of pages;  let i=index" (click)="openPage(p, i);" style="background-color: transparent; border-bottom: 1px solid #717775; color: #f0f0f0">\n\n\n\n        <span ion-text>\n\n          <ion-icon name="{{p.icon}}"> </ion-icon>\n\n          &nbsp;&nbsp;&nbsp;{{p.title}}\n\n          <ion-icon [name]="selectedMenu == i? \'arrow-dropdown-circle\' : \'arrow-dropright-circle\'" *ngIf="p.subPages" float-right></ion-icon>\n\n        </span>\n\n\n\n        <ion-list no-lines [hidden]="selectedMenu != i" style="margin: 0px 0 0px; padding:0px !important;">\n\n          <ion-item no-border *ngFor="let subPage of p.subPages;let i2=index" text-wrap (click)="openPage(subPage)" style="background-color: transparent;border-bottom: 1px solid #b9c2bf; color: #f0f0f0; padding-left: 40px;font-size: 0.8em;">\n\n            <span ion-text>&nbsp;{{subPage.title}}</span>\n\n          </ion-item>\n\n        </ion-list>\n\n\n\n      </ion-item>\n\n    </ion-list> -->\n\n  </ion-content>\n\n  <ion-footer>\n\n    <ion-toolbar color="light" (tap)="logout()" class="toolbarmd">\n\n      <ion-title>\n\n        <ion-icon color="danger" name="log-out"></ion-icon>&nbsp;&nbsp;&nbsp;\n\n        <span ion-text color="danger">Logout</span>\n\n      </ion-title>\n\n    </ion-toolbar>\n\n  </ion-footer>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_7__providers_network_network__["a" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_menu_menu__["a" /* MenuProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"],
            __WEBPACK_IMPORTED_MODULE_8__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Keyboard"]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuOptionSelect; });
// SideMenuOptionSelect constant
var SideMenuOptionSelect = 'sideMenu:optionSelect';
//# sourceMappingURL=side-menu-option-select-event.js.map

/***/ }),

/***/ 304:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__booking_detail_booking_detail__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__booking_detail_booking_detail__["a" /* BookingDetailComponent */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__booking_detail_booking_detail__["a" /* BookingDetailComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ApiServiceProvider = /** @class */ (function () {
    function ApiServiceProvider(http, loadingCtrl) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json; charset=utf-8' });
        this.link = "https://www.oneqlik.in/users/";
        this.appId = "zogo";
        this.pickupGeofence = {};
        console.log('Hello ApiServiceProvider Provider');
    }
    // 13.126.36.205
    ////////////////// LOADING SERVICE /////////////////
    ApiServiceProvider.prototype.startLoading = function () {
        console.log("loading Start");
        this.loading = this.loadingCtrl.create({
            content: "Please wait...",
            spinner: "bubbles"
        });
        return this.loading.present();
    };
    ApiServiceProvider.prototype.stopLoading = function () {
        return this.loading.dismiss();
    };
    ////////////////// END LOADING SERVICE /////////////
    ApiServiceProvider.prototype.feedbackData = function (payload) {
        return this.http.post("https://www.oneqlik.in/trackRouteMap/ZogorideFeedback", payload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.promocodeService = function (_id, amt, ride_id, code) {
        return this.http.get("https://www.oneqlik.in/discountCode/applycode?user=" + _id + "&amount=" + amt + "&ride_id=" + ride_id + "&code=" + code, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDriverList = function (_id) {
        return this.http.get("https://www.oneqlik.in/driver/getDrivers?userid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByDateCall = function (_id, skip, limit, dates) {
        // console.log("from date => "+ dates.fromDate.toISOString())
        // console.log("new date "+ new Date(dates.fromDate).toISOString())
        var from = new Date(dates.fromDate).toISOString();
        var to = new Date(dates.toDate).toISOString();
        return this.http.get('https://www.oneqlik.in/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByType = function (_id, skip, limit, key) {
        return this.http.get('https://www.oneqlik.in/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&type=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFilteredcall = function (_id, skip, limit, key) {
        return this.http.get('https://www.oneqlik.in/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDataOnScroll = function (_id, skip, limit) {
        return this.http.get('https://www.oneqlik.in/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleListCall = function (_id, email) {
        return this.http.get('https://www.oneqlik.in/devices/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trip_detailCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trip_details = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trackRouteDataCall = function (data) {
        return this.http.post("https://www.oneqlik.in/trackRoute", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.savetripDetails = function (data) {
        return this.http.post("https://www.oneqlik.in/trackRouteMap", data)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.canceltripDetails = function (a) {
        return this.http.post("https://www.oneqlik.in/trackRouteMap/cancelRide", a)
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.updatetripDetails = function (a) {
        console.log("update trip");
        return this.http.post("https://www.oneqlik.in/trackRouteMap/updateRide", a)
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.completetripDetails = function (a) {
        return this.http.post("https://www.oneqlik.in/trackRouteMap/rideComplete", a)
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.updateRoute = function (edata) {
        return this.http.post("https://www.oneqlik.in/trackRouteMap/updateRoute_map", edata)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gettrackRouteCall = function (link, data) {
        return this.http.post(link, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trackRouteCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getRoutesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getgeofenceCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.loginApi = function (userdata) {
        return this.http.post(this.link + "LoginWithOtp", userdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.signupApi = function (usersignupdata) {
        return this.http.post('https://www.oneqlik.in/users/signUpZogo', usersignupdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateDL = function (updateDL) {
        return this.http.post('https://www.oneqlik.in/users' + "/zogoUserUpdate", updateDL)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.resendOtp = function (phnum) {
        return this.http.post('https://www.oneqlik.in/users' + "/sendOtpZRides", phnum)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pushnotifyCall = function (pushdata) {
        return this.http.post("https://www.adnateiot.com/users/PushNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.siginupverifyCall = function (usersignup) {
        return this.http.post('https://www.oneqlik.in/users' + "/signUpZogo", usersignup, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.setGeofenceData = function (pickGeoObj) {
        this.pickupGeofence = pickGeoObj;
        console.log(this.pickupGeofence);
    };
    ApiServiceProvider.prototype.getGeofence = function () {
        console.log("getFunc");
        return this.pickupGeofence;
    };
    // paytm integration
    // https://www.oneqlik.in/paytm/sendOTP
    ApiServiceProvider.prototype.paytmValidation = function (userCred) {
        return this.http.get('https://www.oneqlik.in/paytm/ValidateToken?CUST_ID=' + userCred + "&app_id=" + this.appId, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.paytmbalance = function (userCred) {
        return this.http.get('https://www.oneqlik.in/paytm/checkBalance?CUST_ID=' + userCred + "&app_id=" + this.appId, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addMoneyPaytm = function (amtObj) {
        return this.http.post('https://www.oneqlik.in/paytm/addMoney', amtObj, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.paytmLoginWallet = function (walletcredentials) {
        return this.http.post('https://www.oneqlik.in/paytm/sendOTP', walletcredentials, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.paytmOTPvalidation = function (otpObj) {
        return this.http.post('https://www.oneqlik.in/paytm/validateOTP', otpObj, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.paytmAddMount = function (amountObj) {
        return this.http.post('https://pguat.paytm.com/oltp-web/processTransaction', amountObj, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.preAuthantication = function (preAuthObj) {
        return this.http.post('https://www.oneqlik.in/paytm/preAuth', preAuthObj, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.proceedSecurely = function (withdrawObj) {
        return this.http.post('https://www.oneqlik.in/paytm/Withdraw', withdrawObj, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getblockamt = function () {
        //   http://localhost:3000/zogo/getInnitialBlockAmount
        return this.http.get('https://www.oneqlik.in/zogo/getInnitialBlockAmount')
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.releaseAmount = function (uid) {
        return this.http.post('https://www.oneqlik.in/paytm/releaseAmount', uid, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.rideFare = function (fare) {
        return this.http.get('https://www.oneqlik.in/zogo/getBillingAmount?time=' + fare, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.checkTripStatus = function (id) {
        console.log(id);
        return this.http.get('https://www.oneqlik.in/trackRouteMap/getRideStatus?_id=' + id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pauseRide = function (payloadPause) {
        console.log(payloadPause);
        return this.http.post('https://www.oneqlik.in/trackRouteMap/zogoPause', payloadPause, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.resumeRide = function (payloadRestart) {
        console.log(payloadRestart);
        return this.http.post('https://www.oneqlik.in/trackRouteMap/zogoRestart', payloadRestart, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.checkRideStatus = function (uId) {
        return this.http.get('https://www.oneqlik.in/zogo/getLastRideStatusByUser?user=' + uId, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addcommandQueue = function (devImei) {
        return this.http.post("https://www.oneqlik.in/trackRouteMap/addCommandQueue", devImei, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateTripStatus = function (uId, imp_id, field, amt) {
        return this.http.get('https://www.oneqlik.in/zogo/updateZogoPaymentStatus?user=' + uId + '&imp_id=' + imp_id + "&field=" + field + "&amt=" + amt, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.navigateTO = function () {
        return this.http.get('https://www.google.com/maps/search/?api=1&query=47.5951518,-122.3316393&query_place_id=ChIJKxjxuaNqkFQR3CK6O1HNNqY')
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["LoadingController"]])
    ], ApiServiceProvider);
    return ApiServiceProvider;
}());

//# sourceMappingURL=api-service.js.map

/***/ })

},[221]);
//# sourceMappingURL=main.js.map