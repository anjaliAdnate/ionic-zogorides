webpackJsonp([2],{

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeedbackPageModule", function() { return FeedbackPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__feedback__ = __webpack_require__(510);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic3_star_rating__ = __webpack_require__(511);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FeedbackPageModule = /** @class */ (function () {
    function FeedbackPageModule() {
    }
    FeedbackPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__feedback__["a" /* FeedbackPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3_ionic3_star_rating__["a" /* StarRatingModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__feedback__["a" /* FeedbackPage */]),
            ],
        })
    ], FeedbackPageModule);
    return FeedbackPageModule;
}());

//# sourceMappingURL=feedback.module.js.map

/***/ }),

/***/ 504:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StarRating; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);


var HTML_TEMPLATE = "\n<div class=\"ionic3-star-rating\">\n  <button *ngFor=\"let index of [0,1,2,3,4]\" id=\"{{index}}\" type=\"button\" ion-button icon-only (click)=\"changeRating($event)\">\n    <ion-icon [ngStyle]=\"{'color':index < this.Math.round(this.parseFloat(rating)) ? activeColor : defaultColor }\" name=\"{{index < this.Math.round(this.parseFloat(rating)) ? activeIcon : defaultIcon}}\"></ion-icon>\n  </button>\n</div>\n";
var CSS_STYLE = "\n    .ionic3-star-rating .button {\n        height: 28px;\n        background: none;\n        box-shadow: none;\n        -webkit-box-shadow: none;\n        width: 28px;\n    }\n    .ionic3-star-rating .button ion-icon {\n        font-size: 32px;\n    }\n";
var StarRating = (function () {
    function StarRating(events) {
        this.events = events;
        this.rating = 3;
        this.readonly = "false";
        this.activeColor = '#488aff';
        this.defaultColor = '#f4f4f4';
        this.activeIcon = 'ios-star';
        this.defaultIcon = 'ios-star-outline';
        this.Math = Math;
        this.parseFloat = parseFloat;
    }
    StarRating.prototype.changeRating = function (event) {
        if (this.readonly && this.readonly === "true")
            return;
        // event is different for firefox and chrome
        this.rating = event.target.id ? parseInt(event.target.id) + 1 : parseInt(event.target.parentElement.id) + 1;
        // subscribe this event to get the changed value in ypour parent compoanent
        this.events.publish('star-rating:changed', this.rating);
    };
    StarRating.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"], args: [{
                    selector: 'ionic3-star-rating',
                    template: HTML_TEMPLATE,
                    styles: [CSS_STYLE]
                },] },
    ];
    /** @nocollapse */
    StarRating.ctorParameters = function () { return [
        { type: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"], },
    ]; };
    StarRating.propDecorators = {
        "rating": [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        "readonly": [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        "activeColor": [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        "defaultColor": [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        "activeIcon": [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        "defaultIcon": [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
    };
    return StarRating;
}());

//# sourceMappingURL=ionic3-star-rating-component.js.map

/***/ }),

/***/ 510:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FeedbackPage = /** @class */ (function () {
    function FeedbackPage(navParams, navCtrl, events, apiCall, toastCtrl) {
        var _this = this;
        this.navParams = navParams;
        this.navCtrl = navCtrl;
        this.events = events;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.convinient = false;
        this.drive = false;
        this.pricing = false;
        this.booking = false;
        this.starRatinginput = 5;
        this.imp_id = navParams.get("imp_id");
        if (localStorage.getItem("tripDetails")) {
            this.triID = JSON.parse(localStorage.getItem("tripDetails"))._id;
        }
        events.subscribe('star-rating:changed', function (starRating) { _this.starRatinginput = starRating; console.log("start Rating output: " + starRating); });
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    FeedbackPage.prototype.ionViewDidLoad = function () {
        this.rideStatus();
        console.log('ionViewDidLoad FeedbackPage');
    };
    FeedbackPage.prototype.comment = function () { };
    FeedbackPage.prototype.done = function () {
        var _this = this;
        console.log("==============Input feedback===========");
        console.log("convinient: ", this.convinient);
        console.log("drive: ", this.drive);
        console.log("pricing: ", this.pricing);
        console.log("booking: ", this.booking);
        console.log("starRatinginput: ", this.starRatinginput);
        console.log("additionalText: ", this.additionalText);
        // this.navCtrl.setRoot("LivePage");
        if (this.triID == undefined) {
            this.triID = this.TripDetailsdata._id;
        }
        var payload = {
            "tripID": this.triID,
            "feedbackStars": this.starRatinginput,
            "convinient": this.convinient,
            "eace_of_drive": this.drive,
            "eache_of_booking": this.booking,
            "pricing": this.pricing,
            "additionalText": this.additionalText
        };
        this.apiCall.feedbackData(payload)
            .subscribe(function (data) {
            console.log("feedback service response: ", data);
            if (data) {
                var toast = _this.toastCtrl.create({
                    message: "Thank you for your valuable feedback!",
                    duration: 2500,
                    position: "bottom"
                });
                toast.present();
                _this.statusApi();
            }
        });
    };
    FeedbackPage.prototype.statusApi = function () {
        var _this = this;
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        this.apiCall.startLoading();
        this.apiCall.updateTripStatus(userId, this.imp_id, "feedback", null).subscribe(function (res) {
            _this.apiCall.stopLoading();
            if (res.feedbackStatus == true) {
                localStorage.removeItem("tripDetails");
                _this.navCtrl.setRoot('LivePage');
            }
            else {
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    FeedbackPage.prototype.skip = function () {
        localStorage.removeItem("tripDetails");
        this.navCtrl.setRoot("LivePage");
    };
    FeedbackPage.prototype.rideStatus = function () {
        var _this = this;
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        this.apiCall.checkRideStatus(userId)
            .subscribe(function (res) {
            _this.TripDetailsdata = res;
        }, function (err) {
            console.log("Internal Server Error, Please try again !!");
        });
    };
    FeedbackPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-feedback',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/feedback/feedback.html"*/'<ion-content no-padding>\n\n  <div height="100%" width="100%">\n\n      <div style="background: #8eda4a;padding-top: 15%;padding-bottom: 10%;" height="50%">\n\n          <ion-card style="margin: 0px;width: 28%;border-radius: 15px; padding-top: 0%;margin-left: 37%;">\n\n              <img src="assets/icon/zogologo.png" style="width: 60%;margin: 18%;">\n\n          </ion-card>\n\n          <h5 style="color: white;font-weight: 500;text-align: center;font-size: 2.4rem;padding-top: 0%;padding-bottom: 7%;">FEEDBACK</h5>\n\n      </div>\n\n  <ion-card style="width: 90%;min-height: 320px;margin-top: -54px;margin-left: 5%;border-radius: 8%;padding-left: 7%;">\n\n    <!-- <ion-card-content> -->\n\n      <div>\n\n        <ion-label>1. Please Rate your Zogorides Experience.</ion-label>\n\n        <div style="text-align: center;">\n\n          <ionic3-star-rating activeIcon="ios-star" defaultIcon="ios-star-outline" activeColor="#f7f71b"\n\n            defaultColor="#f4f4f4" readonly="false" [rating]="5">\n\n          </ionic3-star-rating>\n\n        </div>\n\n      </div>\n\n      <div>\n\n        <ion-label>2. What Did you liked the most?</ion-label>\n\n        <ion-row>\n\n          <ion-col col-6>Ease of Booking</ion-col>\n\n          <ion-col col-6>\n\n            <ion-checkbox [(ngModel)]="booking"></ion-checkbox>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-6>Pricing</ion-col>\n\n          <ion-col col-6>\n\n            <ion-checkbox [(ngModel)]="pricing"></ion-checkbox>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-6>Ease of Drive</ion-col>\n\n          <ion-col col-6>\n\n            <ion-checkbox [(ngModel)]="drive"></ion-checkbox>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-6>Convinient</ion-col>\n\n          <ion-col col-6>\n\n            <ion-checkbox [(ngModel)]="convinient"></ion-checkbox>\n\n          </ion-col>\n\n        </ion-row>\n\n      </div>\n\n      <div>\n\n        <ion-label>3. Please help us to Improve.</ion-label>\n\n        <ion-input type="text" placeholder="write here..." [(ngModel)]="additionalText"></ion-input>\n\n      </div>\n\n\n\n    <!-- </ion-card-content> -->\n\n\n\n  </ion-card>\n\n  <!-- <ion-row class="userRating">\n\n    <ul class="rate-area">\n\n      <input type="radio" id="5-star" name="rating" value="5" /><label for="5-star" title="Amazing">5 stars</label>\n\n      <input type="radio" id="4-star" name="rating" value="4" /><label for="4-star" title="Good">4 stars</label>\n\n      <input type="radio" id="3-star" name="rating" value="3" /><label for="3-star" title="Average">3 stars</label>\n\n      <input type="radio" id="2-star" name="rating" value="2" /><label for="2-star" title="Not Good">2 stars</label>\n\n      <input type="radio" id="1-star" name="rating" value="1" /><label for="1-star" title="Bad">1 star</label>\n\n    </ul>\n\n  </ion-row> -->\n\n</div>\n\n</ion-content>\n\n<ion-footer style="width: 90%;\n\nheight: 100px;\n\nbackground: #ffffff;\n\nmargin-left: 5%;\n\nborder-top-left-radius: 8px;\n\nborder-top-right-radius: 8px;\n\nbox-shadow: 0px 1px 15px 3px #cfcfcf;">\n\n  <ion-row style="margin-top: 22px;width: 90%;margin-left: 5%;">\n\n    <ion-col>\n\n      <button ion-button full\n\n        style="margin: 0%;height:50px;background:#87c23f;font-size: 18px;text-transform:none;border-radius: 6px;"\n\n        (click)="done()">Submit</button>\n\n    </ion-col>\n\n    <!-- <ion-col>\n\n      <button ion-button full\n\n        style="margin: 0%;height:50px;background:#87c23f;font-size: 18px;text-transform:none;border-radius: 6px;"\n\n        (click)="skip()">Skip</button>\n\n    </ion-col> -->\n\n  </ion-row>\n\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/feedback/feedback.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], FeedbackPage);
    return FeedbackPage;
}());

//# sourceMappingURL=feedback.js.map

/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic3_star_rating_module__ = __webpack_require__(512);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__ionic3_star_rating_module__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_ionic3_star_rating_component__ = __webpack_require__(504);
/* unused harmony namespace reexport */


//# sourceMappingURL=index.js.map

/***/ }),

/***/ 512:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StarRatingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_ionic3_star_rating_component__ = __webpack_require__(504);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);



var StarRatingModule = (function () {
    function StarRatingModule() {
    }
    StarRatingModule.forRoot = function () {
        return {
            ngModule: StarRatingModule,
        };
    };
    StarRatingModule.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"], args: [{
                    imports: [
                        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"]
                    ],
                    declarations: [
                        __WEBPACK_IMPORTED_MODULE_1__components_ionic3_star_rating_component__["a" /* StarRating */]
                    ],
                    exports: [
                        __WEBPACK_IMPORTED_MODULE_1__components_ionic3_star_rating_component__["a" /* StarRating */]
                    ]
                },] },
    ];
    /** @nocollapse */
    StarRatingModule.ctorParameters = function () { return []; };
    return StarRatingModule;
}());

//# sourceMappingURL=ionic3-star-rating.module.js.map

/***/ })

});
//# sourceMappingURL=2.js.map