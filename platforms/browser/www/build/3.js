webpackJsonp([3],{

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ZogomodalPageModule", function() { return ZogomodalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__zogomodal__ = __webpack_require__(526);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ZogomodalPageModule = /** @class */ (function () {
    function ZogomodalPageModule() {
    }
    ZogomodalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__zogomodal__["a" /* ZogomodalPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__zogomodal__["a" /* ZogomodalPage */]),
            ],
        })
    ], ZogomodalPageModule);
    return ZogomodalPageModule;
}());

//# sourceMappingURL=zogomodal.module.js.map

/***/ }),

/***/ 526:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ZogomodalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ZogomodalPage = /** @class */ (function () {
    function ZogomodalPage(navCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
    }
    ZogomodalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ZogomodalPage');
    };
    ZogomodalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ZogomodalPage.prototype.lockIssues = function () {
        this.navCtrl.push("LockIssuePage");
    };
    ZogomodalPage.prototype.reportMisuse = function () {
        this.navCtrl.push("ReportMisusePage");
    };
    ZogomodalPage.prototype.bikeIssue = function () {
        this.navCtrl.push("BikeIssuePage");
    };
    ZogomodalPage.prototype.rideBilling = function () {
        this.navCtrl.push("RideBillingPage");
    };
    ZogomodalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-zogomodal',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/zogomodal/zogomodal.html"*/'<div padding style="margin-top:85%;">\n\n  <div>\n\n    <ion-row style="border-bottom:1px solid">\n\n      <ion-col>\n\n        <h3 style="text-align:center;margin-top: 4px;">SUPPORT</h3>\n\n      </ion-col>\n\n      <ion-col>\n\n        <ion-buttons end>\n\n          <button (click)="dismiss()" style="background: white;font-size:25px;margin-top:0px;">\n\n            <ion-icon name="close-circle"></ion-icon>\n\n          </button>\n\n        </ion-buttons>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row style="background:white;margin-top:3px;">\n\n      <ion-col (click)="lockIssues()">\n\n        <img src="assets/imgs/lock-issues1.png" width="50" height="50" style="margin-top:3%;margin-left:32%;">\n\n        <p style="text-align:center;">Lock Issue</p>\n\n      </ion-col>\n\n\n\n      <ion-col (click)="reportMisuse()">\n\n        <img src="assets/imgs/scooter1.png" width="50" height="50" style="margin-top:3%;margin-left:32%;">\n\n        <p style="text-align:center;">Report Misuse</p>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row style="background:white;margin-top:3px;">\n\n      <ion-col (click)="bikeIssue()">\n\n        <img src="assets/imgs/scooter.png" width="50" height="50" style="margin-top:3%;margin-left:32%;">\n\n        <p style="text-align:center;">Bike Issue</p>\n\n      </ion-col>\n\n\n\n      <ion-col (click)="rideBilling()">\n\n        <img src="assets/imgs/ridebilling1.png" width="50" height="50" style="margin-top:3%;margin-left:32%;">\n\n        <p style="text-align:center;">Ride & Billing</p>\n\n      </ion-col>\n\n    </ion-row>\n\n  </div>\n\n</div>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/zogomodal/zogomodal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], ZogomodalPage);
    return ZogomodalPage;
}());

//# sourceMappingURL=zogomodal.js.map

/***/ })

});
//# sourceMappingURL=3.js.map