webpackJsonp([20],{

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropLocationPageModule", function() { return DropLocationPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__drop_location__ = __webpack_require__(527);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DropLocationPageModule = /** @class */ (function () {
    function DropLocationPageModule() {
    }
    DropLocationPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__drop_location__["a" /* DropLocationPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__drop_location__["a" /* DropLocationPage */]),
            ],
        })
    ], DropLocationPageModule);
    return DropLocationPageModule;
}());

//# sourceMappingURL=drop-location.module.js.map

/***/ }),

/***/ 527:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DropLocationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(207);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DropLocationPage = /** @class */ (function () {
    function DropLocationPage(navCtrl, geolocation, app_api, navParams, apiCall, toast) {
        this.navCtrl = navCtrl;
        this.geolocation = geolocation;
        this.app_api = app_api;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toast = toast;
        this.mapdata = {};
        this.navOptions = {
            animation: 'ios-transition'
        };
        this.locations = [];
        this.changeColor = false;
        this.plannedStartTime = new Date();
        var newObj = {};
        newObj = this.apiCall.getGeofence();
        this.total_km = newObj['totalDist'];
        this.Vehicle_count = newObj['totalVeh'];
        this.hideButton = true;
        var uID = localStorage.getItem('details');
        this.plannedStartTime.setMinutes(this.plannedStartTime.getMinutes() + 10);
        var getData = this.app_api.getGeofence();
        this.pickupgeofenceId = getData['geofenceId'];
        this.useridd = JSON.parse(uID)._id;
        var a = new Date();
        a.setHours(this.plannedStartTime.getHours() + 1, this.plannedStartTime.getMinutes(), this.plannedStartTime.getSeconds());
        this.plannedEndTIme = new Date(a);
        var geofenceOBJ = this.apiCall.getGeofence();
        this.deviceId = geofenceOBJ['device'];
        this.loadMap();
    }
    DropLocationPage.prototype.ionViewDidLoad = function () {
        this.getBlockedAmt();
    };
    DropLocationPage.prototype.ionViewDidEnter = function () {
        this.drop_location = localStorage.getItem("dropOffLocation");
        if (this.drop_location) {
            var d = JSON.parse(this.drop_location);
            this.drop_location = JSON.parse(this.drop_location).id;
            this.drop_locationShow = d['name'];
            this.dropOffObj = this.drop_location;
            this.chooseEnd();
        }
    };
    DropLocationPage.prototype.loadMap = function () {
        this.mapdata.map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas1', {
            camera: {
                target: { lat: 20.5937, lng: 78.9629 },
                zoom: 18,
                tilt: 30
            }
        });
        var marker = this.mapdata.map.addMarkerSync({
            title: 'Current location',
            icon: 'blue',
            animation: 'DROP',
            position: {
                lat: 43.0741904,
                lng: -89.3809802
            }
        });
        this.geolocation.getCurrentPosition().then(function (resp) {
        }).catch(function (error) {
        });
    };
    DropLocationPage.prototype.seeNotifications = function () { };
    DropLocationPage.prototype.dropOffLocation = function () {
        this.navCtrl.push("DropLocationSearchPage", null, this.navOptions);
    };
    DropLocationPage.prototype.chooseEnd = function () {
        var _this = this;
        this.app_api.startLoading();
        var that = this;
        var geocoderEnd = new google.maps.Geocoder;
        geocoderEnd.geocode({ 'placeId': this.drop_location }, function (results, status) {
            if (status === 'OK' && results[0]) {
                that.dstLattitude = results[0].geometry.location.lat();
                that.dstLongitude = results[0].geometry.location.lng();
                var baseURLp = 'https://www.oneqlik.in/zogo/nearby/dst?l=' + _this.dstLattitude + ',' + _this.dstLongitude;
                that.apiCall.getgeofenceCall(baseURLp)
                    .subscribe(function (data) {
                    that.app_api.stopLoading();
                    that.locations = data.result;
                });
            }
            else {
                that.app_api.stopLoading();
                var toasterr = _this.toast.create({
                    message: status,
                    duration: 3000,
                    position: 'top',
                    cssClass: 'toastStyle'
                });
                toasterr.present();
            }
        });
    };
    DropLocationPage.prototype.authanticationBeforeBooking = function () {
        var _this = this;
        if (!this.dropStation) {
            var toastmsg = this.toast.create({
                message: 'Please select drop station',
                duration: 3000,
                position: 'top',
                cssClass: 'tStyle'
            });
            toastmsg.present();
        }
        else {
            var useDetails = JSON.parse(localStorage.getItem('details'));
            var userId = useDetails._id;
            this.apiCall.startLoading();
            this.apiCall.paytmValidation(userId)
                .subscribe(function (res) {
                if ((res.Status == "Token Not Found") || (res.message == "Invalid Token") || (res.status == "FAILURE")) {
                    _this.apiCall.stopLoading();
                    localStorage.setItem("navigationFrom", "NotRegisteredWithPaytm");
                    var tripDetail123 = {
                        "user": _this.useridd,
                        "device": _this.deviceId,
                        "deviceName": "Testingzogo",
                        "driver": _this.useridd,
                        "createdOn": Date.now(),
                        "startsOn": _this.plannedStartTime,
                        "expiresOn": _this.plannedEndTIme,
                        "tripType": 0,
                        "poi": [],
                        "purpose": "zogo",
                        "startSite": _this.pickupgeofenceId,
                        "endSite": _this.dropLocationgeoId
                    };
                    _this.navCtrl.push("PaytmwalletloginPage", {
                        "tripParams": tripDetail123
                    });
                }
                else if ((res.mobile) && (res.id)) {
                    _this.getPaytmBalance(userId);
                    var paytmNumber = res.mobile;
                    localStorage.setItem('paytmregNum', paytmNumber);
                }
            });
        }
    };
    DropLocationPage.prototype.getPaytmBalance = function (uId) {
        var _this = this;
        var toast_validation = this.toast.create({
            duration: 3000,
            position: 'middle'
        });
        this.apiCall.paytmbalance(uId)
            .subscribe(function (res) {
            if (res.code == "ETIMEDOUT") {
                _this.apiCall.stopLoading();
                toast_validation.setMessage("Etimeout server error");
                toast_validation.present();
            }
            else {
                if (res.response.amount >= _this.blockedAmt) {
                    _this.bookRides();
                }
                else {
                    _this.apiCall.stopLoading();
                    localStorage.setItem("navigationFrom", "NotRegisteredWithPaytm");
                    _this.navCtrl.push("WalletPage", { "lowBalance": "addAmt" });
                }
            }
        });
    };
    DropLocationPage.prototype.payRideFare = function (rideID) {
        var _this = this;
        var toast_validation1 = this.toast.create({
            duration: 3000,
            position: 'middle'
        });
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        var preAuthObj = {
            "CUST_ID": userId,
            "TXN_AMOUNT": this.blockedAmt,
            "DURATIONHRS": "70",
            "app_id": "zogo",
            "ride": rideID._id
        };
        this.apiCall.preAuthantication(preAuthObj)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            var responseMsg = res;
            var StatusMsg = responseMsg ? responseMsg.STATUSMESSAGE : "";
            var SuccessRes = responseMsg ? responseMsg.STATUS : "";
            var checksumError = responseMsg ? responseMsg.ErrorMsg : '';
            if (SuccessRes == 'TXN_SUCCESS') {
                _this.navCtrl.push("BookRidesPage", null, _this.navOptions);
            }
            else if (checksumError) {
                toast_validation1.setMessage("Internal Server Error");
                toast_validation1.present();
            }
            else if (StatusMsg) {
                toast_validation1.setMessage(StatusMsg);
            }
            else {
                toast_validation1.setMessage("Internal Server Error");
                toast_validation1.present();
            }
        });
    };
    DropLocationPage.prototype.toastMessages = function (msg) {
        var toastmsgVar = this.toast.create({
            message: msg,
            duration: 5000,
            position: 'top',
            cssClass: 'tStyle'
        });
        return toastmsgVar.present();
    };
    DropLocationPage.prototype.bookRides = function () {
        var _this = this;
        var toastmsg = this.toast.create({
            message: 'Please select drop station',
            duration: 3000,
            position: 'top',
            cssClass: 'tStyle'
        });
        if (this.dropStation) {
            var tripDetail = {
                "user": this.useridd,
                "device": this.deviceId,
                "deviceName": "Testingzogo",
                "driver": this.useridd,
                "createdOn": Date.now(),
                "startsOn": this.plannedStartTime,
                "expiresOn": this.plannedEndTIme,
                "tripType": 0,
                "poi": [],
                "purpose": "zogo",
                "startSite": this.pickupgeofenceId,
                "endSite": this.dropLocationgeoId
            };
            this.app_api.savetripDetails(tripDetail)
                .subscribe(function (res) {
                var timerTime = new Date();
                var tempRes = res;
                if (tempRes) {
                    localStorage.setItem('timerTIme', JSON.stringify(timerTime));
                    localStorage.setItem("tripDetails", JSON.stringify(tempRes));
                    // localStorage.setItem("tripID_imp", tempRes._id)
                    _this.payRideFare(tempRes);
                }
                else {
                    _this.apiCall.stopLoading();
                }
            }, function (err) {
                if (err.status == 400) {
                    _this.app_api.stopLoading();
                    var abtemp = JSON.parse(err._body);
                    _this.toastMessages(abtemp.message);
                }
            });
        }
        else {
            this.app_api.stopLoading();
            toastmsg.present();
        }
    };
    DropLocationPage.prototype.selectedOption = function (ev) {
        this.dropStation = ev;
        this.dropLocationgeoId = ev._id;
        for (var i = 0; i < this.locations.length; i++) {
            if (this.locations[i]._id != ev._id)
                this.locations[i].changeColor = false;
            else
                this.locations[i].changeColor = true;
        }
        localStorage.setItem("droplocatoingeoId", JSON.stringify(ev));
    };
    DropLocationPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    DropLocationPage.prototype.getBlockedAmt = function () {
        var _this = this;
        this.app_api.getblockamt()
            .subscribe(function (res) {
            _this.blockedAmt = res['Block Amount'];
        }, function (err) {
        });
    };
    DropLocationPage.prototype.cancelride = function () {
        var _this = this;
        var reason = "plan changed";
        var cancellationTime = new Date();
        var tempDetail = localStorage.getItem('tripDetails');
        console.log("kkk", tempDetail);
        var tempRouteDetail = JSON.parse(tempDetail);
        console.log(tempRouteDetail);
        console.log("identifier=>", tempDetail);
        var cancelObj = {
            "_id": tempRouteDetail._id,
            "cancelledAt": cancellationTime,
            "status": "CANCELLED",
            "device": tempRouteDetail.device,
            "cancelledBy": this.useridd,
            "cancellationReason": reason
        };
        this.app_api.startLoading();
        this.app_api.canceltripDetails(cancelObj)
            .subscribe(function (res) {
            _this.app_api.stopLoading();
            localStorage.removeItem('tripDetails');
            localStorage.removeItem('pickupLocation');
            localStorage.removeItem("dropOffLocation");
            localStorage.removeItem('timerTIme');
            localStorage.removeItem('lockRide');
        }, function (err) {
            _this.app_api.stopLoading();
        });
    };
    DropLocationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-drop-location',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/drop-location/drop-location.html"*/'<ion-header class="transparentNav">\n\n  <ion-navbar class="navColor">\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-buttons end>\n\n      <button id="notification-button" ion-button clear (click)="seeNotifications()">\n\n        <ion-icon name="notifications" style="font-size: 20px; color: #fff">\n\n          <ion-badge id="notifications-badge" color="danger">{{cartCount}}</ion-badge>\n\n        </ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding>\n\n  <div id="map_canvas1" style="width: 100%;height: 100%;">\n\n    <ion-card>\n\n      <ion-row no-padding (click)="dropOffLocation()">\n\n        <ion-col col-2>\n\n          <ion-icon style="font-size: 46px;color: #8eda4a;margin-top: 9px;margin-left: 8px;margin-bottom: 9px;"\n\n            name="md-pin"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-10>\n\n          <p style="font-size: 1.6rem;margin-top: 12px;">Drop location</p>\n\n          <p style="font-size: 1.4rem;white-space: nowrap;">{{drop_locationShow}}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card>\n\n\n\n    <ion-card *ngFor="let location of locations ; let i =index">\n\n      <ion-row>\n\n        <ion-col col-2>\n\n          <ion-icon style="font-size: 46px;color: red;margin-top: 9px;margin-left: 8px;margin-bottom: 9px;"\n\n            name="md-pin"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-10 (click)="selectedOption(location)" [ngClass]="{\'background\':location.changeColor}">\n\n          <div style="margin: 12px 11px 5px 5px;">\n\n            <span>Station Name</span>\n\n            <span style="float:right;color:#808080;margin-left:11px;">{{location.geoname}}</span>\n\n          </div>\n\n          <div style="margin: 5px 0px 5px 5px;">\n\n            <span>Distance from destination</span>\n\n            <span style="float:right;color:#808080;">{{location.dist?(location.dist.calculated | number: \'1.2-2\'): 0 }}\n\n              kms</span>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card>\n\n\n\n  </div>\n\n</ion-content>\n\n<ion-footer style="width: 90%;\n\nheight: 100px;\n\nbackground: #ffffff;\n\nmargin-left: 5%;\n\nborder-top-left-radius: 8px;\n\nborder-top-right-radius: 8px;\n\nbox-shadow: 0px 1px 15px 3px #cfcfcf;">\n\n  <ion-row style="font-size: 14px">\n\n    <ion-col>\n\n      <p style="text-align:center;width: 100%; color:#808080">{{total_km}}</p>\n\n    </ion-col>\n\n    <ion-col>\n\n      <p style="text-align:center;width: 100%; color:#808080">{{Vehicle_count}}</p>\n\n    </ion-col>\n\n    <ion-col>\n\n      <p style="text-align:center;width: 100%; color:#808080">Navigate</p>\n\n    </ion-col>\n\n  </ion-row>\n\n  <button ion-button full style="height: 37px;background: #87c23f;font-size: 14px;width: 60%;margin-left: 20%;"\n\n    (click)="authanticationBeforeBooking()">Confirm Ride</button>\n\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/drop-location/drop-location.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], DropLocationPage);
    return DropLocationPage;
}());

//# sourceMappingURL=drop-location.js.map

/***/ })

});
//# sourceMappingURL=20.js.map