webpackJsonp([22],{

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DrivingLicensePageModule", function() { return DrivingLicensePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__driving_license__ = __webpack_require__(507);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DrivingLicensePageModule = /** @class */ (function () {
    function DrivingLicensePageModule() {
    }
    DrivingLicensePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__driving_license__["a" /* DrivingLicensePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__driving_license__["a" /* DrivingLicensePage */]),
            ],
        })
    ], DrivingLicensePageModule);
    return DrivingLicensePageModule;
}());

//# sourceMappingURL=driving-license.module.js.map

/***/ }),

/***/ 507:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DrivingLicensePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_file__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_transfer__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_path__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_api_service_api_service__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DrivingLicensePage = /** @class */ (function () {
    function DrivingLicensePage(navCtrl, navParams, transfer, transferObj, file, platform, filePath, camera, actionSheetCtrl, toastCtrl, loadingCtrl, apiCall) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.transfer = transfer;
        this.transferObj = transferObj;
        this.file = file;
        this.platform = platform;
        this.filePath = filePath;
        this.camera = camera;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.apiCall = apiCall;
        this.lastImage = null;
        this.placeString = "upload document";
    }
    DrivingLicensePage.prototype.ionViewDidLoad = function () {
        this.signupObject = this.navParams.get('signupObj');
        this.mobilenumber = this.signupObject.phoneNum;
    };
    DrivingLicensePage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    DrivingLicensePage.prototype.docChange = function (ev) {
        console.log("doc key: ", ev);
        if (ev == "Adhar Card") {
            this.docString = "adharCard";
        }
        else {
            if (ev == "Pan Card") {
                this.docString = "panCard";
            }
            else {
                if (ev == "Driving Licence") {
                    this.docString = "drivingLicence";
                }
                else {
                    if (ev == "Selfie") {
                        this.docString = "selfie";
                    }
                    else {
                        var toast = this.toastCtrl.create({
                            message: "Please choose valid doc",
                            duration: 2000,
                            position: "bottom"
                        });
                        toast.present();
                    }
                }
            }
        }
    };
    DrivingLicensePage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    DrivingLicensePage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    DrivingLicensePage.prototype.pathForImage = function (img) {
        console.log("Image=>", img);
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    DrivingLicensePage.prototype.uploadImage = function () {
        var _this = this;
        var url = "https://www.oneqlik.in/users/uploadImage";
        var targetPath = this.pathForImage(this.lastImage);
        var filename = this.lastImage;
        var options = {
            fileKey: "photo",
            fileName: filename,
            chunkedMode: false,
            mimeType: "image/jpeg",
            params: { 'fileName': filename }
        };
        this.transferObj = this.transfer.create();
        this.Imgloading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.Imgloading.present();
        this.transferObj.upload(targetPath, url, options).then(function (data) {
            _this.Imgloading.dismissAll();
            _this.dlUpdate(data.response);
        }, function (err) {
            console.log("uploadError=>", err);
            _this.lastImage = null;
            _this.Imgloading.dismissAll();
            _this.presentToast('Error while uploading file, Please try again !!!');
        });
    };
    DrivingLicensePage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    DrivingLicensePage.prototype.takePicture = function (sourceType) {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imagePath) {
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    DrivingLicensePage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    DrivingLicensePage.prototype.dlUpdate = function (dllink) {
        var _this = this;
        var dlObj = {
            image_path: dllink,
            phone: this.mobilenumber,
            img_type: this.docString
        };
        this.apiCall.startLoading();
        this.apiCall.updateDL(dlObj)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            _this.presentToast('Image succesful uploaded.');
            _this.lastImage = null;
            _this.docImge = undefined;
            _this.docString = undefined;
            _this.placeString = "upload another document";
            _this.showButton = true;
            // this.navCtrl.setRoot("LoginPage");
        }, function (err) {
            _this.apiCall.stopLoading();
            _this.presentToast('Internal server Error !!!');
        });
    };
    DrivingLicensePage.prototype.subDone = function () {
        this.showButton = false;
        this.navCtrl.setRoot("LoginPage");
    };
    DrivingLicensePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-driving-license',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/driving-license/driving-license.html"*/'<ion-content no-padding>\n\n  <ion-grid style="background: #8eda4a;">\n\n    <!-- <ion-icon name="md-arrow-back" style="font-size: 2.6em;color: white;margin: 1% 0% 0% 4%;" (click)="goBack()">\n\n    </ion-icon> -->\n\n    <ion-icon name="ios-document-outline" style="font-size: 8.6em;color: white;margin: 10% 0% 0% 43%;"></ion-icon>\n\n    <h5 style="color: white;margin-top: 1.0rem;text-align: center;font-weight: 400;">You need to Upload your</h5>\n\n    <h3 style="color: white;font-size: 2.6rem;margin-top: 0rem;text-align: center;margin-bottom: 12%;">Documents\n\n    </h3>\n\n  </ion-grid>\n\n  <!-- <ion-row style="padding: 10%;padding-top:0% ;padding-bottom: 2%;">\n\n    <ion-col col-2 style="padding-top: 10%;">\n\n      <ion-icon ios="ios-image" md="md-image" style="font-size: 3.0em;color: #131746e8;"></ion-icon>\n\n    </ion-col>\n\n    <ion-col col-10 style="padding-top: 11%" (click)="presentActionSheet()">\n\n      <p style="font-size: 1.4rem;margin: 0% 0% 0% 0%;">Select the document form gallery</p>\n\n      <p style="margin: 0%;color: #6f6b6b;">png, jpg or jpeg </p>\n\n      <p>Image <span>{{lastImage}}</span></p>\n\n    </ion-col>\n\n    <button ion-button icon-start round style="margin: 5% 0% 0% 27%;height: 4.2rem;background: #FF9800;"\n\n      (click)="uploadImage()" [disabled]="lastImage === null">\n\n      <ion-icon name="camera"></ion-icon>\n\n      Upload\n\n    </button>\n\n  </ion-row> -->\n\n  <ion-row padding-top padding-left padding-right>\n\n    <ion-col>\n\n      <ion-label style="font-size: 1.5em;color: #131746e8;">\n\n        Select Document\n\n      </ion-label>\n\n    </ion-col>\n\n  </ion-row>\n\n  <ion-row padding-bottom padding-left padding-right>\n\n    <ion-col>\n\n      <ion-select placeholder="{{placeString}}" class="selectStyle" [(ngModel)]="docImge">\n\n        <ion-option [value]="adharCard" (ionSelect)="docChange($event)">Adhar Card</ion-option>\n\n        <!-- <ion-option [value]="panCard" (ionSelect)="docChange($event)">Pan Card</ion-option> -->\n\n        <ion-option [value]="drivingLicence" (ionSelect)="docChange($event)">Driving Licence</ion-option>\n\n        <ion-option [value]="selfie" (ionSelect)="docChange($event)">Selfie</ion-option>\n\n        <!-- <ion-option *ngFor="let dealer of selectDealer" [value]="dealer.dealer_firstname" (ionSelect)="dealerOnChnage(dealer)">{{dealer.dealer_firstname}}</ion-option> -->\n\n      </ion-select>\n\n    </ion-col>\n\n  </ion-row>\n\n  <ion-row padding-left padding-right *ngIf="docImge">\n\n    <ion-col (click)="presentActionSheet()">\n\n      <p style="font-size: 1.4rem;margin: 0% 0% 0% 0%;">Select the document form gallery</p>\n\n      <p style="margin: 0%;color: #6f6b6b;">png, jpg or jpeg </p>\n\n      <p>Image <span>{{lastImage}}</span></p>\n\n    </ion-col>\n\n  </ion-row>\n\n  <button ion-button icon-start round style="margin: 5% 0% 0% 27%;height: 4.2rem;background: #FF9800;"\n\n    (click)="uploadImage()" [disabled]="lastImage === null">\n\n    <ion-icon name="camera"></ion-icon>\n\n    Upload\n\n  </button>\n\n  <!-- <ion-row style="height:23%;padding:2% 10% 0% 10%">\n\n    <img src="{{pathForImage(lastImage)}}" style="width: 100%" [hidden]="lastImage === null">\n\n  </ion-row> -->\n\n</ion-content>\n\n<ion-footer style="width: 90%;\n\n\n\nbackground: #ffffff;\n\nmargin-left: 5%;\n\nborder-top-left-radius: 8px;\n\nborder-top-right-radius: 8px;\n\nbox-shadow: 0px 1px 15px 3px #cfcfcf;" *ngIf="showButton">\n\n\n\n  <ion-row>\n\n    <ion-col>\n\n      <button ion-button full style="height: 37px;background: #87c23f;font-size: 14px;" (click)="subDone()">Upload Done</button>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/driving-license/driving-license.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_transfer__["a" /* Transfer */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_transfer__["b" /* TransferObject */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_6__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], DrivingLicensePage);
    return DrivingLicensePage;
}());

//# sourceMappingURL=driving-license.js.map

/***/ })

});
//# sourceMappingURL=22.js.map