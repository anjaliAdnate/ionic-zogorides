webpackJsonp([5],{

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TripPageModule", function() { return TripPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__trip__ = __webpack_require__(525);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TripPageModule = /** @class */ (function () {
    function TripPageModule() {
    }
    TripPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__trip__["a" /* TripPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__trip__["a" /* TripPage */]),
            ],
        })
    ], TripPageModule);
    return TripPageModule;
}());

//# sourceMappingURL=trip.module.js.map

/***/ }),

/***/ 525:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TripPage = /** @class */ (function () {
    function TripPage(navCtrl, navParams, apicalligi, alertCtrl, toast, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalligi = apicalligi;
        this.alertCtrl = alertCtrl;
        this.toast = toast;
        this.modalCtrl = modalCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    TripPage.prototype.ionViewDidLoad = function () {
        this.getTrip();
    };
    TripPage.prototype.ngOnInit = function () { };
    TripPage.prototype.getTrip = function () {
        var _this = this;
        this.TripAllData = [];
        var baseURLp = 'https://www.oneqlik.in/trackRouteMap/getTrips?user=' + this.islogin._id + "&purpose=" + 'zogo';
        this.apicalligi.trip_details(baseURLp)
            .subscribe(function (data) {
            _this.TripData = data;
            var fetchTime = _this.TripData;
            for (var i = 0; i < _this.TripData.length; i++) {
                _this.TripStart_Time = new Date(_this.TripData[i].startSiteEnterAt).toLocaleString();
                _this.TripEnd_Time = new Date(_this.TripData[i].endSiteExitAt).toLocaleString();
                var fd = new Date(_this.TripStart_Time).getTime();
                var td = new Date(_this.TripEnd_Time).getTime();
                var time_difference = td - fd;
                var total_min = time_difference / 60000;
                var hours = total_min / 60;
                var rhours = Math.floor(hours);
                var minutes = (hours - rhours) * 60;
                var rminutes = Math.round(minutes);
                _this.Durations = rhours + ':' + rminutes;
                _this.TripAllData.push({ '_id': _this.TripData[i]._id, 'Actual_startTime': _this.TripData[i].startsOn, 'Actual_endTime': _this.TripData[i].expiresOn, 'Status': _this.TripData[i].status, 'Unloading_Site': _this.TripData[i].endSite.geoname, 'Loading_Site': _this.TripData[i].startSite.geoname, 'durations': _this.Durations, 'plannedStartTime': _this.TripData[i].startsOn, 'purpose': _this.TripData[i].purpose, 'createdOn': _this.TripData[i].createdOn });
            }
        }, function (err) {
            _this.apicalligi.stopLoading();
        });
    };
    TripPage.prototype.goto = function () {
        this.navCtrl.setRoot("BookRidesPage");
    };
    TripPage.prototype.ShowTrip = function (tripdata) {
        if (tripdata.Status == "ASSIGNED") {
            var timerTime = tripdata.createdOn;
            localStorage.setItem('timerTIme', JSON.stringify(timerTime));
            localStorage.setItem("tripDetails", JSON.stringify(tripdata));
            this.navCtrl.push("BookRidesPage", {
                tripLocations: tripdata
            });
        }
        else if ((tripdata.Status == "COMPLETED") || (tripdata.Status == "CANCELLED")) {
            this.navCtrl.push("LivePage");
        }
        else if (tripdata.Status == "LOADING") {
            this.navCtrl.push("ConfirmBookingPage");
        }
        else {
            var toast = this.toast.create({
                message: 'check different Trip !!!',
                duration: 3000,
                position: 'top',
                cssClass: 'toastStyle'
            });
            toast.present();
        }
    };
    TripPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-trip',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/trip/trip.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>My Trips</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-item *ngFor="let tripdata of TripAllData">\n\n    <div (click)="ShowTrip(tripdata)" style="padding-top:8px;">\n\n      <ion-row>\n\n        <ion-col col-9>\n\n          <p style="margin-top:0%;margin-bottom: 5px;">\n\n            <ion-icon ios="ios-time" md="md-time" style="font-size: 17px;"></ion-icon>&nbsp;&nbsp;&nbsp;{{tripdata.Actual_startTime\n\n            | date:\'medium\'}}\n\n          </p>\n\n          <p style="font-size:11px;margin-bottom: 5px;">\n\n            <ion-icon name="pin" color="secondary" style="font-size: 17px;"></ion-icon>\n\n            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{tripdata.Loading_Site}}\n\n          </p>\n\n          <p style="font-size:11px;margin-bottom: 5px;">\n\n            <ion-icon name="pin" color="danger" style="font-size: 17px;"></ion-icon>\n\n            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{tripdata.Unloading_Site}}\n\n          </p>\n\n          <p style="font-size:13px;margin-bottom: 5px;margin-left: 23px;">Travel time\n\n            :&nbsp;&nbsp;{{tripdata.durations!="NaN:NaN"?tripdata.durations:"0:0"}}&nbsp;Min</p>\n\n\n\n          <p style="font-size:13px;margin-bottom: 5px;margin-left: 23px;">Trip Fare\n\n            :&nbsp;&nbsp;{{tripdata.RideAmount}}</p>\n\n        </ion-col>\n\n        <ion-col col-3 style="margin-top: 15px;">\n\n          <img src="assets/imgs/cancelled.png" *ngIf="tripdata.Status == \'CANCELLED\'" style="height:40px;">\n\n          <img src="assets/imgs/completed.jpg" *ngIf="tripdata.Status == \'COMPLETED\'" style="height:40px;">\n\n          <h6\n\n            style="color: rgb(36, 221, 129); margin-top: 2%; text-align: right;text-transform: uppercase;font-size: 11px;"\n\n            *ngIf="tripdata.Status == \'ASSIGNED\'">&nbsp;&nbsp;&nbsp;&nbsp;Booked</h6>\n\n          <h6\n\n            style="color: rgb(36, 221, 129); margin-top: 2%; text-align: right;text-transform: uppercase;font-size: 11px;"\n\n            *ngIf="tripdata.Status == \'LOADING\'">&nbsp;&nbsp;&nbsp;&nbsp;Transit</h6>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n  </ion-item>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/trip/trip.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]])
    ], TripPage);
    return TripPage;
}());

//# sourceMappingURL=trip.js.map

/***/ })

});
//# sourceMappingURL=5.js.map