webpackJsonp([16],{

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyProfilePageModule", function() { return MyProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__my_profile__ = __webpack_require__(515);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MyProfilePageModule = /** @class */ (function () {
    function MyProfilePageModule() {
    }
    MyProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__my_profile__["a" /* MyProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__my_profile__["a" /* MyProfilePage */]),
            ],
        })
    ], MyProfilePageModule);
    return MyProfilePageModule;
}());

//# sourceMappingURL=my-profile.module.js.map

/***/ }),

/***/ 515:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MyProfilePage = /** @class */ (function () {
    function MyProfilePage(navCtrl, navParams, menu, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.formBuilder = formBuilder;
        this.userprofile = formBuilder.group({
            uid: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            yourRide: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            payment: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            help: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            user_doc: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    MyProfilePage.prototype.ionViewDidEnter = function () { };
    MyProfilePage.prototype.ionViewDidLoad = function () {
        this.menu.enable(false);
    };
    MyProfilePage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    MyProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-my-profile',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/my-profile/my-profile.html"*/'<ion-content no-padding>\n\n    <ion-grid class="edt_profile" no-padding>\n\n        <ion-row style="margin-bottom: 0px">\n\n            <button ion-button icon-start (click)="goBack()"\n\n                style="background:#8eda4a;box-shadow:none;padding-left: 0px;">\n\n                <ion-icon name="md-arrow-back"></ion-icon>\n\n            </button>\n\n        </ion-row>\n\n        <ion-row style="margin:0px;">\n\n            <ion-icon class="profile_image" name="ios-contact"></ion-icon>\n\n        </ion-row>\n\n        <p style="font-size: 20px;margin: 5px 0px 12px 0px;color: white;font-weight: 400;">User Profile</p>\n\n    </ion-grid>\n\n    <ion-grid style="margin-top: 5px;">\n\n        <form [formGroup]="userprofile">\n\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n\n                <ion-col no-padding>\n\n                    <ion-input placeholder="User Id" formControlName="uid"></ion-input>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n\n                <ion-col no-padding>\n\n                    <ion-input placeholder="Your Rides" formControlName="yourRide"></ion-input>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n\n                <ion-col no-padding>\n\n                    <ion-input placeholder="Payments" formControlName="payment"></ion-input>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n\n                <ion-col no-padding>\n\n                    <ion-input placeholder="Help" type="text" formControlName="help"></ion-input>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n\n                <ion-col no-padding>\n\n                    <ion-input type="text" placeholder="Document" formControlName="user_doc"></ion-input>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n        </form>\n\n\n\n    </ion-grid>\n\n</ion-content>\n\n<ion-footer>\n\n    <ion-row no-padding>\n\n        <ion-col>\n\n            <button ion-button full outline\n\n                style="border: 0px;font-weight: 400;color: black;text-transform: none;font-size: 18px;">Settings</button>\n\n        </ion-col>\n\n        <ion-col>\n\n            <button ion-button full outline\n\n                style="border: 0px;font-weight: 400;color: black;text-transform: none;font-size: 18px;">Legal</button>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/my-profile/my-profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]])
    ], MyProfilePage);
    return MyProfilePage;
}());

//# sourceMappingURL=my-profile.js.map

/***/ })

});
//# sourceMappingURL=16.js.map