webpackJsonp([18],{

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LockIssuePageModule", function() { return LockIssuePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lock_issue__ = __webpack_require__(513);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LockIssuePageModule = /** @class */ (function () {
    function LockIssuePageModule() {
    }
    LockIssuePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__lock_issue__["a" /* LockIssuePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__lock_issue__["a" /* LockIssuePage */]),
            ],
        })
    ], LockIssuePageModule);
    return LockIssuePageModule;
}());

//# sourceMappingURL=lock-issue.module.js.map

/***/ }),

/***/ 513:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LockIssuePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LockIssuePage = /** @class */ (function () {
    function LockIssuePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = [];
        this.data = [
            {
                title: '1. I am unable to open lock?',
                details: "Please make sure the QR code you are scanning that is visible clearly. If you are still unable to scan the QR code and unlock the bike, please connect to our support team on support@zogorides.com or call us at 8178076078.",
                icon: 'ios-arrow-forward',
                showDetails: false
            },
            {
                title: '2. Bike with Private lock?',
                details: 'Our Bikes do not have any private lock. If you find any bike with private lock please raise an issue to our support team, we will immediately help you to resolve the issue- support@zogorides.com or inform us on the call 8178076078.',
                icon: 'ios-arrow-forward',
                showDetails: false
            }
        ];
    }
    LockIssuePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LockIssuePage');
    };
    LockIssuePage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-forward';
        }
        else {
            data.showDetails = true;
            data.icon = 'ios-arrow-down';
        }
    };
    LockIssuePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-lock-issue',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/lock-issue/lock-issue.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Lock Issue</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list>\n\n    <ion-item *ngFor="let d of data" (click)="toggleDetails(d)">\n\n      <ion-icon color="custCol" item-right [name]="d.icon"></ion-icon>\n\n      {{d.title}}\n\n      <div *ngIf="d.showDetails">{{d.details}}</div>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/lock-issue/lock-issue.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], LockIssuePage);
    return LockIssuePage;
}());

//# sourceMappingURL=lock-issue.js.map

/***/ })

});
//# sourceMappingURL=18.js.map