webpackJsonp([19],{

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivePageModule", function() { return LivePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__live__ = __webpack_require__(528);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ion_bottom_drawer__ = __webpack_require__(219);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var LivePageModule = /** @class */ (function () {
    function LivePageModule() {
    }
    LivePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__live__["a" /* LivePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__live__["a" /* LivePage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__node_modules_ion_bottom_drawer__["a" /* IonBottomDrawerModule */]
            ],
        })
    ], LivePageModule);
    return LivePageModule;
}());

//# sourceMappingURL=live.module.js.map

/***/ }),

/***/ 528:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_location_accuracy__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Port = /** @class */ (function () {
    function Port() {
    }
    return Port;
}());
var LivePage = /** @class */ (function () {
    function LivePage(modalCtrl, navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, platform, geolocation, menu, locationAccuracy, events) {
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.platform = platform;
        this.geolocation = geolocation;
        this.menu = menu;
        this.locationAccuracy = locationAccuracy;
        this.events = events;
        this.navOptions = {
            animation: 'ios-transition'
        };
        this.travelType = 'DRIVING';
        this.distance = '';
        this.duration = '';
        this.geoshape = {};
        this.drawerHidden = false;
        this.shouldBounce = true;
        this.disableBooking = true;
        this.dockedHeight = 150;
        this.bounceThreshold = 500;
        this.distanceTop = 56;
        this.allData = {};
        this.socketSwitch = {};
        this.socketChnl = [];
        this.showActionSheet = true;
        this.bounds = [];
        this.generalPolygon = [];
        this.geoName = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        ///////////////////////////////
        this.events.publish('user:updated', this.islogin);
        /////////////////////////////////
        this.pickup_location = JSON.parse(localStorage.getItem("pickupLocation"));
        this.drop_location = localStorage.getItem("dropOffLocation");
        // this.enableLocation();
        if (this.platform.is('ios')) {
            this.iconMarker = 'www/assets/icon/marker.png';
            this.iconUser = 'www/assets/icon/user.png';
        }
        else {
            this.iconMarker = './assets/icon/marker.png';
            this.iconUser = './assets/icon/user.png';
        }
    }
    LivePage.prototype.portChange = function (event) { };
    LivePage.prototype.ngOnInit = function () {
        this.startService = new google.maps.places.AutocompleteService();
        this.autocompleteStart = [];
        this.endService = new google.maps.places.AutocompleteService();
        this.autocompleteEnd = [];
        this.start = '';
        this.end = '';
        this.drop_location = "";
        this.pickup_location = "";
        this.locationShow = "";
        this.drop_locationShow = "";
        this.enableLocation();
    };
    LivePage.prototype.enableLocation = function () {
        var _this = this;
        var that = this;
        console.log("Inside Enable Location Function !!!");
        this.locationAccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
                _this.locationAccuracy.request(_this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
                    console.log('Request successful');
                    //  that.sethome();
                }, function (error) {
                    console.log("Inside Error block !!!");
                    console.log('Error requesting location permissions' + JSON.stringify(error));
                });
            }
        });
    };
    LivePage.prototype.ionViewDidLoad = function () {
        this.allData.map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas');
        var pl;
        this.showActionSheet = false;
        pl = localStorage.getItem('pickupLocation');
        if (pl) {
            var plDummy = JSON.parse(pl).id;
            if (plDummy != 'dummyId') {
                this.pickup_location = JSON.parse(pl).id;
                this.locationShow = JSON.parse(pl).name;
                this.pickupObj = pl;
                this.chooseStart();
            }
            else {
                this.loadMap();
            }
        }
        else {
            this.loadMap();
        }
    };
    LivePage.prototype.loadMap = function () {
        var _this = this;
        this.allData.map.one(__WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["c" /* GoogleMapsEvent */].MAP_READY).then(function (res) {
            if (res)
                console.log('map ready: ', res);
        });
        var that = this;
        this.allData.map.getMyLocation()
            .then(function (location) {
            that.fetchAdd(location.latLng);
            that.allData.map.animateCamera({
                target: location.latLng,
                zoom: 17,
                tilt: 30,
                duration: 50,
            })
                .then(function () {
                var marker = that.allData.map.addMarkerSync({
                    title: "Current Location",
                    position: location.latLng,
                    icon: that.iconUser,
                    animation: __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMapsAnimation */].BOUNCE
                });
                _this.remMarker = marker;
            });
        });
    };
    LivePage.prototype.fetchAdd = function (latlng) {
        var that = this;
        var templatlng = JSON.parse(latlng);
        var initLat = templatlng.lat;
        var initLng = templatlng.lng;
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(initLat, initLng);
        var request = {
            latLng: latlng
        };
        geocoder.geocode(request, function (data, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (data[0] != null) {
                    that.address_show = data[0].formatted_address;
                    that.locationShow = that.address_show;
                    var pickup_location = "dummyId";
                    var description = that.locationShow;
                    var pickupObj = {
                        'id': pickup_location,
                        'name': description
                    };
                    localStorage.setItem("pickupLocation", JSON.stringify(pickupObj));
                }
                else {
                    that.address_show = data[0].formatted_address;
                }
            }
            else { }
        });
        this.getgeofence(initLat, initLng);
    };
    LivePage.prototype.ionViewDidEnter = function () {
        this.menu.enable(true);
    };
    LivePage.prototype.ngOnDestroy = function () {
        for (var i = 0; i < this.socketChnl.length; i++)
            this._io.removeAllListeners(this.socketChnl[i]);
        localStorage.removeItem("LiveDevice");
    };
    LivePage.prototype.getgeofence = function (lat, lng) {
        var that = this;
        this.geoName = [];
        var c = {
            lat: lat,
            lng: lng
        };
        that.bounds.push(c);
        var baseURLp = 'https://www.oneqlik.in/zogo/nearby/src?l=' + lat + ',' + lng;
        that.apiCall.getgeofenceCall(baseURLp)
            .subscribe(function (data) {
            that.geofenceShowdata = data.result;
            that.polydatageo = [];
            for (var i = 0; i < that.geofenceShowdata.length; i++) {
                that.cordin = [];
                that.geoName.push(that.geofenceShowdata[i].geoname);
                var a = that.geofenceShowdata[i].geofence.coordinates[0];
                for (var k = 0; k < a.length; k++) {
                    var b = {
                        lat: a[k][1],
                        lng: a[k][0]
                    };
                    that.cordin.push(b);
                    that.bounds.push(b);
                }
                that.polydatageo.push(that.cordin);
            }
            // two varaible are there that.polydatageo and polydata
            var j = 0;
            function reCur(j) {
                if (j < that.polydatageo.length) {
                    var polydata = that.polydatageo[j];
                    that.allData.map.addMarker({
                        title: that.geoName[j],
                        icon: that.iconMarker,
                        styles: {
                            'text-align': 'center',
                            'font-style': 'italic',
                            'font-weight': 'bold',
                            'color': 'green'
                        },
                        position: { lat: polydata[0].lat, lng: polydata[0].lng },
                    }).then(function (marker) {
                        that.generalPolygon.push(marker);
                        that.polyRef[marker.getId()] = that.geofenceShowdata[j];
                        marker['_objectInstance'].on(__WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["c" /* GoogleMapsEvent */].MARKER_CLICK, function () {
                            that.total_distance = (Math.round((that.polyRef[marker.getId()].dist.calculated) * 10) / 10).toFixed(1) + " kms";
                            that.total_veh = that.polyRef[marker.getId()].devicesWithin.length;
                            that.assignVehicle = that.polyRef[marker.getId()].devicesWithin[0];
                            var geofenceDetails = {
                                totalDist: that.total_distance,
                                totalVeh: that.total_veh,
                                geofenceId: that.polyRef[marker.getId()]._id,
                                device: that.assignVehicle
                            };
                            that.apiCall.setGeofenceData(geofenceDetails);
                            that.showActionSheet = true;
                        });
                        reCur(j + 1);
                    });
                }
                else {
                    that.allData.map.animateCamera({
                        target: that.bounds,
                        duration: 50
                    });
                    var marker = that.allData.map.addMarkerSync({
                        title: "Current Location",
                        icon: that.iconUser,
                        animation: __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMapsAnimation */].BOUNCE,
                    });
                }
            }
            reCur(0);
        }, function (err) {
            console.log(err);
        });
    };
    LivePage.prototype.polyRef = function (polyRef) {
        throw new Error("Method not implemented.");
    };
    LivePage.prototype.search_places = function () {
        this.navCtrl.push("PickupLocationSearchPage", null, this.navOptions);
    };
    LivePage.prototype.chooseStart = function () {
        var that = this;
        this.autocompleteStart = [];
        var geocoderStart = new google.maps.Geocoder;
        geocoderStart.geocode({ 'placeId': this.pickup_location }, function (results, status) {
            if (status === 'OK' && results[0]) {
                var position = {
                    lat: results[0].geometry.location.lat,
                    lng: results[0].geometry.location.lng
                };
                var picLat = results[0].geometry.location.lat();
                var picLng = results[0].geometry.location.lng();
                that.getgeofence(picLat, picLng);
                var latLng1 = ({ lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() });
                that.allData.map.addMarker({
                    icon: that.iconUser,
                    position: latLng1,
                }).then(function (marker) {
                    that.remMarker = marker;
                });
                var startLat = results[0].geometry.location.lat();
                var startlng = results[0].geometry.location.lng();
                that.allData.map.animateCamera({
                    target: { lat: startLat, lng: startlng },
                    zoom: 13,
                    duration: 50,
                    padding: 0 // default = 20px
                });
                that.allData.map.setCameraTarget(latLng1);
            }
        });
    };
    LivePage.prototype.availableStations = function () {
        this.navCtrl.push("DropLocationPage", null, this.navOptions);
    };
    LivePage.prototype.sethome = function () {
        this.generalPolygon;
        for (var i = 0; i < this.generalPolygon.length; i++) {
            this.generalPolygon[i].remove();
        }
        if (this.remMarker) {
            this.remMarker.remove();
        }
        this.bounds = [];
        localStorage.removeItem('pickupLocation');
        this.loadMap();
    };
    LivePage.prototype.OpenPopUp = function () {
        var modal = this.modalCtrl.create('ZogomodalPage');
        modal.onDidDismiss(function () { });
        modal.present();
    };
    LivePage.prototype.navigate = function () {
        this.apiCall.navigateTO()
            .subscribe(function (res) { });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], LivePage.prototype, "mapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('directionsPanel'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], LivePage.prototype, "directionsPanel", void 0);
    LivePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-live',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/live/live.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content class="map-main" no-padding>\n\n\n\n  <div id="map_canvas">\n\n    <ion-fab bottom left style="margin-bottom: 26%;">\n\n      <button ion-fab mini (click)="OpenPopUp()" color="light">\n\n        <!-- <ion-icon name="headset" color="{{mapHideTraffic ? \'dark-grey\' : \'black\'}}"></ion-icon> -->\n\n        <img src="assets/imgs/2019-03-01.png">\n\n      </button>\n\n    </ion-fab>\n\n  </div>\n\n\n\n  <div class="location_sel">\n\n    <ion-card class="pickup_from" (click)="search_places()" style="padding:0px;">\n\n      <ion-row no-padding>\n\n        <ion-col col-2>\n\n          <ion-icon style="font-size: 46px;color: #8eda4a;margin-top: 9px;margin-left: 8px;margin-bottom: 9px;"\n\n            name="md-pin"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-10>\n\n          <p style="font-size: 1.6rem;margin-top: 12px;text-align: left">Pickup location</p>\n\n          <p style="font-size: 1.4rem;white-space: nowrap;color:#808080;text-align: left;">{{locationShow}}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card>\n\n  </div>\n\n  <ion-icon name="ios-locate-outline" class="navStyle" (click)="sethome()"></ion-icon>\n\n</ion-content>\n\n\n\n<ion-row *ngIf="showActionSheet">\n\n  <ion-footer style="width: 90%;\n\n  height: 100px;\n\n  background: #ffffff;\n\n  margin-left: 5%;\n\n  border-top-left-radius: 8px;\n\n  border-top-right-radius: 8px;\n\n  box-shadow: 0px 1px 15px 3px #cfcfcf;">\n\n    <ion-row style="font-size: 14px">\n\n      <ion-col>\n\n        <p style="text-align:center;width: 100%;">{{total_distance?total_distance:0}}</p>\n\n      </ion-col>\n\n      <ion-col>\n\n        <p style="text-align:center;width: 100%;">{{total_veh?total_veh:0}}</p>\n\n      </ion-col>\n\n      <ion-col (click)="navigate()">\n\n        <p style="text-align:center;width: 100%;">Navigate</p>\n\n      </ion-col>\n\n    </ion-row>\n\n    <button ion-button full\n\n      style="margin: 0%;width: 80%;height: 36px;background: #87c23f;font-size: 14px;margin-left: 10%;"\n\n      (click)="availableStations()">Checkout Stations</button>\n\n  </ion-footer>\n\n</ion-row>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/live/live.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], LivePage);
    return LivePage;
}());

//# sourceMappingURL=live.js.map

/***/ })

});
//# sourceMappingURL=19.js.map