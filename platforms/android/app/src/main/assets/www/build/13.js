webpackJsonp([13],{

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaytmwalletloginPageModule", function() { return PaytmwalletloginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__paytmwalletlogin__ = __webpack_require__(531);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaytmwalletloginPageModule = /** @class */ (function () {
    function PaytmwalletloginPageModule() {
    }
    PaytmwalletloginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__paytmwalletlogin__["a" /* PaytmwalletloginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__paytmwalletlogin__["a" /* PaytmwalletloginPage */]),
            ],
        })
    ], PaytmwalletloginPageModule);
    return PaytmwalletloginPageModule;
}());

//# sourceMappingURL=paytmwalletlogin.module.js.map

/***/ }),

/***/ 531:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaytmwalletloginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PaytmwalletloginPage = /** @class */ (function () {
    function PaytmwalletloginPage(navCtrl, navParams, toastCtrl, apiCall, menu, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.menu = menu;
        this.toast = toast;
        this.successresponse = false;
        this.inputform = false;
        this.navOptions = {
            animation: 'ios-transition'
        };
        if (navParams.get("chnagenumParam")) {
            this.checkPageNav = navParams.get("chnagenumParam");
        }
        var num = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
        this.paytmnumber = num.phn;
        if (navParams.get("tripParams")) {
            this.tripDetails = navParams.get("tripParams");
        }
    }
    PaytmwalletloginPage.prototype.ionViewDidEnter = function () {
        this.menu.enable(true);
        this.getBlockedAmt();
    };
    PaytmwalletloginPage.prototype.getBlockedAmt = function () {
        var _this = this;
        this.apiCall.startLoading();
        this.apiCall.getblockamt()
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            _this.blockedAmt = res['Block Amount'];
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    PaytmwalletloginPage.prototype.paytmwallet = function () {
        var _this = this;
        var useDetails = localStorage.getItem('details');
        var userId = JSON.parse(useDetails)._id;
        var paytmUserCredentials = {
            "user": userId,
            "app_id": "zogo",
            "phone": this.paytmnumber,
            "scope": "wallet",
            "responseType": "token",
        };
        this.apiCall.paytmLoginWallet(paytmUserCredentials)
            .subscribe(function (res) {
            _this.successresponse = true;
            _this.inputform = true;
            _this.paytmSignupOTPResponse = res;
            var errToast_success = _this.toast.create({
                message: 'OTP sent successfully, Please type OTP in next field !!!',
                duration: 3000,
                position: 'top',
                cssClass: "toastStyle"
            });
            errToast_success.present();
        }, function (err) {
            var errToast = _this.toast.create({
                message: 'Server error , Please try after somtime !!!',
                duration: 3000,
                position: 'top',
                cssClass: "toastStyle"
            });
            if (err) {
                errToast.present();
            }
        });
    };
    PaytmwalletloginPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    PaytmwalletloginPage.prototype.resndOTP = function () {
        var _this = this;
        var useDetails = localStorage.getItem('details');
        var userId = JSON.parse(useDetails)._id;
        var paytmUserCredentials = {
            "user": userId,
            "app_id": "zogo",
            "phone": this.paytmnumber,
            "scope": "wallet",
            "responseType": "token"
        };
        this.apiCall.paytmLoginWallet(paytmUserCredentials)
            .subscribe(function (res) {
            _this.successresponse = true;
            _this.inputform = true;
            _this.paytmSignupOTPResponse = res;
            var errToast_success = _this.toast.create({
                message: 'OTP sent successfully, Please type OTP in next field !!!',
                duration: 3000,
                position: 'top',
                cssClass: "toastStyle"
            });
            errToast_success.present();
        }, function (err) {
            var errToast = _this.toast.create({
                message: 'Server error , Please try after somtime !!!',
                duration: 3000,
                position: 'top',
                cssClass: "toastStyle"
            });
            if (err) {
                console.log("error occured !!!");
                errToast.present();
            }
        });
    };
    PaytmwalletloginPage.prototype.paytmAuthantication = function () {
        var _this = this;
        this.apiCall.startLoading();
        var otpCredentials = {
            "otp": this.paytmotp,
            "transac_id": this.paytmSignupOTPResponse.transac_id,
            "app_id": "zogo"
        };
        this.apiCall.paytmOTPvalidation(otpCredentials)
            .subscribe(function (res) {
            // this.apiCall.stopLoading();
            localStorage.setItem('paytmregNum', _this.paytmnumber);
            if (res.status == 'SUCCESS') {
                var useDetails = JSON.parse(localStorage.getItem('details'));
                var userId = useDetails._id;
                _this.getPaytmBalance(userId);
            }
            else {
                _this.apiCall.stopLoading();
                var resp = res.response;
                var resp1 = JSON.parse(resp);
                if (resp1.status == "FAILURE") {
                    var toast = _this.toastCtrl.create({
                        message: resp1.message,
                        duration: 3000,
                        position: 'top'
                    });
                    toast.present();
                }
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: 'internal server Error, Please try after sometime !!!',
                duration: 3000,
                position: 'top'
            });
            toast.present();
        });
    };
    PaytmwalletloginPage.prototype.getPaytmBalance = function (uId) {
        var _this = this;
        var toast_validation = this.toastCtrl.create({
            duration: 3000,
            position: 'middle'
        });
        // this.apiCall.startLoading();
        this.apiCall.paytmbalance(uId)
            .subscribe(function (res) {
            if (res.code == "ETIMEDOUT") {
                _this.apiCall.stopLoading();
                toast_validation.setMessage("Etimeout server error");
                toast_validation.present();
            }
            else {
                if (res.response.amount >= _this.blockedAmt) {
                    if (_this.checkPageNav != undefined) {
                        _this.apiCall.stopLoading();
                        localStorage.setItem("paytmregNum", _this.paytmnumber);
                        _this.navCtrl.push('WalletPage', null, _this.navOptions);
                    }
                    else {
                        _this.bookRides();
                    }
                }
                else {
                    _this.apiCall.stopLoading();
                    _this.navCtrl.push('WalletPage', null, _this.navOptions);
                }
            }
        });
    };
    PaytmwalletloginPage.prototype.bookRides = function () {
        var _this = this;
        // this.apiCall.startLoading();
        this.apiCall.savetripDetails(this.tripDetails)
            .subscribe(function (res) {
            var timerTime = new Date();
            var tempRes = res;
            if (tempRes) {
                _this.payRideFare(tempRes);
                localStorage.setItem('timerTIme', JSON.stringify(timerTime));
                localStorage.setItem("tripDetails", JSON.stringify(tempRes));
            }
            else {
                _this.apiCall.stopLoading();
            }
        }, function (err) {
            if (err.status == 400) {
                _this.apiCall.stopLoading();
                var abtemp = JSON.parse(err._body);
                _this.toastMessages(abtemp.message);
            }
        });
    };
    PaytmwalletloginPage.prototype.toastMessages = function (msg) {
        var toastmsgVar = this.toast.create({
            message: msg,
            duration: 5000,
            position: 'top',
            cssClass: 'tStyle'
        });
        return toastmsgVar.present();
    };
    PaytmwalletloginPage.prototype.payRideFare = function (rideID) {
        var _this = this;
        // this.apiCall.startLoading();
        var toast_validation1 = this.toast.create({
            duration: 3000,
            position: 'middle'
        });
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        var preAuthObj = {
            "CUST_ID": userId,
            "TXN_AMOUNT": this.blockedAmt,
            "DURATIONHRS": "70",
            "app_id": "zogo",
            "ride": rideID._id
        };
        this.apiCall.preAuthantication(preAuthObj)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            var responseMsg = res;
            var StatusMsg = responseMsg ? responseMsg.STATUSMESSAGE : "";
            var SuccessRes = responseMsg ? responseMsg.STATUS : "";
            var checksumError = responseMsg ? responseMsg.ErrorMsg : '';
            if (SuccessRes == 'TXN_SUCCESS') {
                _this.navCtrl.push("BookRidesPage", null, _this.navOptions);
            }
            else if (checksumError) {
                toast_validation1.setMessage("Internal Server Error");
                toast_validation1.present();
            }
            else if (StatusMsg) {
                toast_validation1.setMessage(StatusMsg);
            }
            else {
                toast_validation1.setMessage("Internal Server Error");
                toast_validation1.present();
            }
        });
    };
    PaytmwalletloginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-paytmwalletlogin',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/paytmwalletlogin/paytmwalletlogin.html"*/'<ion-header no-border>\n\n  <ion-navbar color="custCol"></ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding>\n\n  <div style="background:#8eda4a;">\n\n\n\n    <ion-card\n\n      style="margin: 0px;width: 28%;border-radius: 15px;padding-top: 46ppx;position: fixed;margin-top: 30px;margin-left: 37%;margin-bottom: 20px;">\n\n      <img src="assets/icon/zogologo.png" style="width: 60%;margin: 18%;">\n\n    </ion-card>\n\n    <h5\n\n      style="margin: 0%;color: white;font-weight: 300;padding-top: 44%;text-align: center;font-size: 1.8rem;padding-bottom: 15px;">\n\n      Payment Method</h5>\n\n  </div>\n\n  <div style="padding: 0% 10% 0% 10%;">\n\n\n\n    <h4 style="font-size: 1.8rem;letter-spacing: 2px;color: #777575;">Paytm</h4>\n\n    <ion-item style="padding-left: 0px;">\n\n      <ion-input [disabled]=\'inputform\' [(ngModel)]="paytmnumber" type="number" placeholder="Mobile Number*">\n\n      </ion-input>\n\n    </ion-item>\n\n    <p style="color:grey">Linking wallet is one time process. Next time, checkout will be a breeze!</p>\n\n  </div>\n\n  <button [disabled]=\'inputform\' ion-button full\n\n    style="height: 35px;width: 45%;margin-left: 30%;background: rgb(135, 194, 63);font-size: 15px;color: white;margin-top: 15px;color: rgb(255, 255, 255);"\n\n    (click)="paytmwallet()">Continue</button>\n\n  <ion-row style="padding: 2% 10% 0% 10%;" *ngIf="successresponse">\n\n    <h4 style="font-size: 1.8rem;letter-spacing: 2px;color: #777575;">Enter verification code</h4>\n\n    <p style="margin-top: 0px;color: grey;margin-bottom: 0;">Paytm has send verification code on {{paytmnumber}} via sms\n\n      Please enter it below, and you are done!</p>\n\n    <ion-item style="padding: 0px;">\n\n      <ion-input [(ngModel)]="paytmotp" type="number" placeholder="Enter OTP"></ion-input>\n\n    </ion-item>\n\n\n\n  </ion-row>\n\n</ion-content>\n\n<ion-footer style="width: 90%;\n\nheight: 10%;\n\nbackground: #ffffff;\n\nmargin-left: 5%;\n\nborder-top-left-radius: 8px;\n\nborder-top-right-radius: 8px;\n\nbox-shadow: 0px 1px 15px 3px #cfcfcf;" *ngIf="successresponse">\n\n  <ion-row style="margin-top: 4px;width: 90%;margin-left: 5%;">\n\n    <ion-col>\n\n      <button ion-button full style="margin: 0%;height: 43px;background: #87c23f;font-size: 14px;border-radius: 8px"\n\n        (click)="resndOTP()">Resend</button>\n\n    </ion-col>\n\n    <ion-col>\n\n      <button ion-button full style="margin: 0%;height: 43px;background: #87c23f;font-size: 14px;border-radius: 8px"\n\n        (click)="paytmAuthantication()">Proceed</button>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/paytmwalletlogin/paytmwalletlogin.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], PaytmwalletloginPage);
    return PaytmwalletloginPage;
}());

//# sourceMappingURL=paytmwalletlogin.js.map

/***/ })

});
//# sourceMappingURL=13.js.map