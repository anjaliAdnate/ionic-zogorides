webpackJsonp([17],{

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(515);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 515:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signupOtp__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { SignupPage } from '../signup/signup';


var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, formBuilder, alertCtrl, apiservice, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.apiservice = apiservice;
        this.toastCtrl = toastCtrl;
        this.showPassword = false;
        this.type = "password";
        this.show = false;
        this.loginForm = formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.toggleShowPassword = function () {
        this.showPassword = !this.showPassword;
    };
    LoginPage.prototype.userlogin = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.loginForm.valid) {
            var UserName = isNaN(this.loginForm.value.username);
            if (UserName == false) {
                this.data = {
                    "psd": this.loginForm.value.password,
                    "ph_num": this.loginForm.value.username
                };
            }
            else {
                this.data = {
                    "psd": this.loginForm.value.password,
                    "emailid": this.loginForm.value.username
                };
            }
            this.apiservice.startLoading();
            this.apiservice.loginApi(this.data)
                .subscribe(function (response) {
                _this.logindata = response;
                _this.logindata = JSON.stringify(response);
                var logindetails = JSON.parse(_this.logindata);
                _this.userDetails = window.atob(logindetails.token.split('.')[1]);
                _this.details = JSON.parse(_this.userDetails);
                localStorage.setItem("loginflag", "loginflag");
                localStorage.setItem('details', JSON.stringify(_this.details));
                localStorage.setItem('condition_chk', _this.details.isDealer);
                _this.apiservice.stopLoading();
                _this.checkPageStatus(_this.details._id);
            }, function (error) {
                var body = error._body;
                if (body) {
                    var msg = JSON.parse(body);
                    if (msg.message == "User account is InActive") {
                        // show alert
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Account Inactive',
                            message: 'DL is pending for approval and within 12 Hours at max it will be approved.',
                            cssClass: 'alertStyle',
                            buttons: ['OK']
                        });
                        alert_1.present();
                        _this.apiservice.stopLoading();
                    }
                    else {
                        _this.toastMsg(msg.message);
                        _this.responseMessage = "Something Wrong";
                        _this.apiservice.stopLoading();
                    }
                }
            });
        }
    };
    LoginPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2500
        });
        toast.present();
    };
    LoginPage.prototype.gotosignuppage = function () {
        this.navCtrl.push("SignupPage");
    };
    LoginPage.prototype.toggleShow = function () {
        this.show = !this.show;
        if (this.show) {
            this.type = "text";
        }
        else {
            this.type = "password";
        }
    };
    LoginPage.prototype.resendOtp = function (pnum) {
        var _this = this;
        console.log("resend");
        console.log(pnum);
        var toast = this.toastCtrl.create({
            message: 'OTP Sent !!!',
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var alert = this.alertCtrl.create({
            title: 'Verify Account',
            message: 'Please Verify User Detail for use App',
            cssClass: 'alertStyle',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel'
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__signup_signupOtp__["a" /* SignupOtp */]);
                        var phonenumber = { ph_num: pnum };
                        _this.apiservice.resendOtp(phonenumber)
                            .subscribe(function (res) {
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    LoginPage.prototype.checkPageStatus = function (uID) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: "Welcome! You're logged In successfully.",
            duration: 3000,
            position: 'bottom'
        });
        this.responseMessage = "LoggedIn  successfully";
        this.apiservice.checkRideStatus(uID)
            .subscribe(function (res) {
            var currentStatus = res.status;
            if (currentStatus == "COMPLETED") {
                if (res.payment_status == false) {
                    console.log(res.payment_status);
                    toast.onDidDismiss(function () {
                        _this.navCtrl.setRoot("RideTotalAmountPage");
                    });
                    toast.present();
                }
                else if (res.payment_status == true) {
                    toast.onDidDismiss(function () {
                        if (res.feedbackStatus == true) {
                            _this.navCtrl.setRoot("LivePage");
                        }
                        else {
                            _this.navCtrl.setRoot("FeedbackPage");
                        }
                    });
                    toast.present();
                }
            }
            else if (currentStatus == "ASSIGNED") {
                toast.onDidDismiss(function () {
                    _this.navCtrl.setRoot("BookRidesPage");
                });
                toast.present();
            }
            else if (currentStatus == "LOADING") {
                toast.onDidDismiss(function () {
                    _this.navCtrl.setRoot("ConfirmBookingPage");
                });
                toast.present();
            }
            else {
                toast.onDidDismiss(function () {
                    _this.navCtrl.setRoot("LivePage");
                });
                toast.present();
            }
        }, function (err) {
            toast.setMessage("Internal Server Error, Please try again !!");
            toast.present();
        });
    };
    LoginPage.prototype.toastMsg = function (msg) {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });
        if (msg == "Mobile Phone Not Verified") {
            toast.onDidDismiss(function () {
                localStorage.setItem('mobnum', _this.loginForm.value.username);
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__signup_signupOtp__["a" /* SignupOtp */]);
            });
            return toast.present();
        }
        if (msg == "User is not registered") {
            toast.onDidDismiss(function () {
                _this.navCtrl.push("SignupPage");
            });
            return toast.present();
        }
        return toast.present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/login/login.html"*/'<ion-content no-padding>\n\n    <div width="100%" height="100%">\n\n    <div style="background: #8eda4a;padding-top: 15%;padding-bottom: 10%;" height="50%">\n\n        <ion-card style="margin: 0px;width: 28%;border-radius: 15px; padding-top: 0%;margin-left: 37%;">\n\n            <img src="assets/icon/zogologo.png" style="width: 60%;margin: 18%;">\n\n        </ion-card>\n\n        <h5 style="color: white;font-weight: 500;text-align: center;font-size: 3.6rem;padding-top: 0%;padding-bottom: 7%;">SIGN IN</h5>\n\n    </div>\n\n    <!-- <div> -->\n\n        <div class="row">\n\n            <ion-card style="margin: -70px 0px 0px 12px;border-radius: 20px;" class="col-sm-12 col-12 col-md-12" >\n\n                <form [formGroup]="loginForm" style="margin-top: 10%;margin-bottom: 10%">\n\n                    <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 8px 30px;">\n\n                        <ion-col col-10 no-padding>\n\n                            <ion-input placeholder="Phone Number" type = "number" minlength="10" maxlength="13"  formControlName="username"></ion-input>\n\n                        </ion-col>\n\n                        <ion-col col-2 no-padding style="text-align: right;margin-top: 1%;color: gray;">\n\n                            <ion-icon name="mail" style="font-size:3.4rem"></ion-icon>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                    <p *ngIf="!loginForm.controls.username.valid && (loginForm.controls.username.dirty || submitAttempt)"\n\n                        style="text-align:center;color:red;">Please type valid phone number</p>\n\n                    <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n\n                        <ion-col col-10 no-padding>\n\n                            <ion-input placeholder="Password*" type="{{type}}" formControlName="password"></ion-input>\n\n                        </ion-col>\n\n                        <ion-col col-2 no-padding style="text-align: right;margin-top: 1%;color: gray;"\n\n                            (click)="toggleShow()">\n\n                            <ion-icon name="eye" style="font-size:3.4rem"></ion-icon>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                    <p *ngIf="!loginForm.controls.password.valid && (loginForm.controls.password.dirty || submitAttempt)"\n\n                        style="text-align:center;color:red;">password required!</p>\n\n                </form>\n\n            </ion-card> \n\n            <ion-row class="col-sm-12 col-12 col-md-12 " style="margin:auto">\n\n                <button ion-button round\n\n                    style="width: 18.2rem;color:white;background: #8eda4a;margin: -24px 0px 0px -12px;font-weight:400;text-transform: none;"\n\n                    (click)="userlogin()">Sign In</button>\n\n            </ion-row>         \n\n        </div>\n\n        <div style="text-align:center">\n\n                <p (tap)="forgotPassFunc()" style="text-align:center;color: grey;">Forgot Password ?</p>\n\n                <div (click)="gotosignuppage()" >\n\n                    <p style="color:grey;">Don\'t have an account ?<span style="color: black;margin-left: 5px;">Sign\n\n                            Up</span></p>\n\n                </div>      \n\n        </div>\n\n    <!-- </div> -->\n\n</div>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=17.js.map