webpackJsonp([9],{

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RideBillingPageModule", function() { return RideBillingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ride_billing__ = __webpack_require__(521);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RideBillingPageModule = /** @class */ (function () {
    function RideBillingPageModule() {
    }
    RideBillingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__ride_billing__["a" /* RideBillingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__ride_billing__["a" /* RideBillingPage */]),
            ],
        })
    ], RideBillingPageModule);
    return RideBillingPageModule;
}());

//# sourceMappingURL=ride-billing.module.js.map

/***/ }),

/***/ 521:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RideBillingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RideBillingPage = /** @class */ (function () {
    function RideBillingPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = [
            {
                title: '1. Unable to end ride?',
                details: 'Please restart the app and try to end the ride. Generally, end ride process may take 10 seconds to 30 seconds depending on your operator mobile network. Still Unable to End the ride Raise the issue to our support team support@zogorides.com',
                icon: 'ios-arrow-forward',
                showDetails: false
            },
            {
                title: '2. Pause the ride?',
                details: 'Please restart the app and try to end the ride. Generally, end ride process may take 10 seconds to 30 seconds depending on your operator mobile network. Still Unable to End the ride Raise the issue to our support team support@zogorides.com',
                icon: 'ios-arrow-forward',
                showDetails: false
            },
            {
                title: '3. I was overcharged for my ride?',
                details: 'Please write to us with your ride details to our support team, we will resolve the issue within 24 hours. support@zogorides.com',
                icon: 'ios-arrow-forward',
                showDetails: false
            }
        ];
    }
    RideBillingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RideBillingPage');
    };
    RideBillingPage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-forward';
        }
        else {
            data.showDetails = true;
            data.icon = 'ios-arrow-down';
        }
    };
    RideBillingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ride-billing',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/ride-billing/ride-billing.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Ride Billing</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list>\n\n    <ion-item *ngFor="let d of data" (click)="toggleDetails(d)">\n\n      <ion-icon color="custCol" item-right [name]="d.icon"></ion-icon>\n\n      {{d.title}}\n\n      <div *ngIf="d.showDetails">{{d.details}}</div>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/ride-billing/ride-billing.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], RideBillingPage);
    return RideBillingPage;
}());

//# sourceMappingURL=ride-billing.js.map

/***/ })

});
//# sourceMappingURL=9.js.map