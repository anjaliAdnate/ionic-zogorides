webpackJsonp([14],{

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentSecurePageModule", function() { return PaymentSecurePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_secure__ = __webpack_require__(518);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaymentSecurePageModule = /** @class */ (function () {
    function PaymentSecurePageModule() {
    }
    PaymentSecurePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__payment_secure__["a" /* PaymentSecurePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__payment_secure__["a" /* PaymentSecurePage */]),
            ],
        })
    ], PaymentSecurePageModule);
    return PaymentSecurePageModule;
}());

//# sourceMappingURL=payment-secure.module.js.map

/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentSecurePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_network_interface__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PaymentSecurePage = /** @class */ (function () {
    function PaymentSecurePage(navCtrl, navParams, apiCall, toastCtrl, networkInterface) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.networkInterface = networkInterface;
        this.amounttobepaid = navParams.get("amtPayble");
        this.imp_id = navParams.get("imp_id");
        this.getNetworkDetail();
    }
    PaymentSecurePage.prototype.ionViewDidLoad = function () {
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        this.getPaytmBalance(userId);
    };
    PaymentSecurePage.prototype.getPaytmBalance = function (uId) {
        var _this = this;
        var toast_validation = this.toastCtrl.create({
            duration: 3000,
            position: 'middle'
        });
        this.apiCall.startLoading();
        this.apiCall.paytmbalance(uId)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            console.log("response");
            if (res.code == "ETIMEDOUT") {
                toast_validation.setMessage("Etimeout server error");
                toast_validation.present();
            }
            else {
                _this.walletBal = res.response.amount;
            }
        });
    };
    PaymentSecurePage.prototype.proceedSecurely = function () {
        var _this = this;
        this.paytmregNum = localStorage.getItem('paytmregNum');
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        var toastCtrl = this.toastCtrl.create({
            message: "Trip amount paid, Thanks for using zogo ride !!",
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var toastCtrlfailure = this.toastCtrl.create({
            message: "Payment failed , Try again",
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var toastCtrlServerError = this.toastCtrl.create({
            message: "Internal Server Error, Try Again",
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var updatePaymentStatus = this.toastCtrl.create({
            message: "Unable to update Payment Status ",
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var app_ip_address;
        if (this.wifiIpAdd) {
            app_ip_address = this.wifiIpAdd;
            console.log("wifiaddress=>", app_ip_address);
        }
        else if (this.carrerIpAdd) {
            app_ip_address = this.carrerIpAdd;
            console.log("carrerIpaddress", app_ip_address);
        }
        else {
            var Networkmsg = "Plese check your Internet connection";
            console.log(Networkmsg);
        }
        var paymentWithdraw = {
            CUST_ID: userId,
            app_ip: app_ip_address,
            deviceId: this.paytmregNum,
            "app_id": "zogo",
            "TXN_AMOUNT": this.amounttobepaid
        };
        this.apiCall.startLoading();
        this.apiCall.proceedSecurely(paymentWithdraw)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            if (res.Status == "TXN_SUCCESS") {
                _this.apiCall.updateTripStatus(userId, _this.imp_id, "payment", _this.amounttobepaid).subscribe(function (res) {
                    if (res.payment_status == true) {
                        toastCtrl.present();
                        toastCtrl.onDidDismiss(function () {
                            localStorage.removeItem('flag');
                            localStorage.removeItem('tripFare');
                            localStorage.removeItem('totalrideTime');
                            localStorage.removeItem('rideStatus');
                            localStorage.removeItem('pickupLocation');
                            localStorage.removeItem('paytmBalance');
                            localStorage.removeItem('droplocatoingeoId');
                            localStorage.removeItem('active_Device');
                            localStorage.removeItem("fareTime");
                            localStorage.removeItem("tripStartTime");
                            localStorage.removeItem('ignitionLock');
                            // this.navCtrl.setRoot('LivePage');
                            _this.navCtrl.push('FeedbackPage', {
                                "imp_id": _this.imp_id
                            });
                        });
                    }
                    else {
                        updatePaymentStatus.present();
                    }
                });
            }
            else if (res.STATUS == "TXN_FAILURE") {
                toastCtrlfailure.present();
            }
            else {
                toastCtrlServerError.present();
            }
        }, function (err) {
            console.log("error: ", err);
        });
    };
    PaymentSecurePage.prototype.getNetworkDetail = function () {
        var outerThis = this;
        this.networkInterface.getWiFiIPAddress()
            .then(function (address) {
            outerThis.wifiIpAdd = address.ip;
            console.log("WifiAddress", outerThis.wifiIpAdd);
            console.info("IP: " + address.ip + ", Subnet: " + address.subnet);
        })
            .catch(function (error) { return console.error("Unable to get IP: " + error); });
        this.networkInterface.getCarrierIPAddress()
            .then(function (address) {
            outerThis.carrerIpAdd = address.ip;
            console.log("careerAddress", outerThis.carrerIpAdd);
            console.info("IP: " + address.ip + ", Subnet: " + address.subnet);
        })
            .catch(function (error) { return console.error("Unable to get IP: " + error); });
    };
    PaymentSecurePage.prototype.toasterror = function (errstatus) {
        console.log(errstatus);
        var toastCtrl1 = this.toastCtrl.create({
            duration: 3000,
            position: 'bottom',
            cssClass: 'toastStyle'
        });
        toastCtrl1['message'] = errstatus;
        return toastCtrl1.present();
    };
    PaymentSecurePage.prototype.releaseAMT = function () {
        var _this = this;
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        this.apiCall.startLoading();
        var releaseObj = {
            CUST_ID: userId,
            app_id: 'zogo'
        };
        this.apiCall.releaseAmount(releaseObj)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            if (res.STATUS == "TXN_SUCCESS") {
                _this.proceedSecurely();
            }
            else {
                _this.toasterror("try again");
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            _this.toasterror("try again");
        });
    };
    PaymentSecurePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-payment-secure',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/payment-secure/payment-secure.html"*/'<ion-header no-border>\n\n  <ion-navbar color="custCol"></ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding>\n\n  <div width="100%" height="100%">\n\n      <div style="background: #8eda4a;" height="50%">\n\n          <ion-card style="margin: 0px;width: 28%;border-radius: 15px; padding-top: 0%;margin-left: 37%;">\n\n              <img src="assets/icon/zogologo.png" style="width: 60%;margin: 18%;">\n\n          </ion-card>\n\n          <h5 style="color: white;font-weight: 500;text-align: center;font-size: 2.0rem;padding-top: 0%;padding-bottom: 7%;">PAYMENT</h5>\n\n      </div>\n\n  <ion-grid no-padding style="text-align: center;">\n\n    <p style="color: #8eda4a;font-size: 15px;text-align: right;margin-right: 5%;">Amount to be paid :\n\n      <i class="fa fa-rupee"></i> {{amounttobepaid}}</p>\n\n    <p style="color: #6a6c69;margin: 8% 0% 0% 20%;font-size: 20px;text-align: left;">Pay via</p>\n\n    <img src="assets/icon/Paytm_logo.png" style="width: 60%;margin: 2% 0% 0% 17%;">\n\n    <p style="color: #8f8f8f;margin: 0%;margin-top: 5%;font-size: 14px;">Balance Available in Wallet\n\n      <i class="fa fa-rupee"></i> {{walletBal}}</p>\n\n  </ion-grid>\n\n</div>\n\n</ion-content>\n\n<ion-footer style="width: 90%;\n\nheight: 100px;\n\nbackground: #ffffff;\n\nmargin-left: 5%;\n\nborder-top-left-radius: 8px;\n\nborder-top-right-radius: 8px;\n\nbox-shadow: 0px 1px 15px 3px #cfcfcf;">\n\n  <ion-row style="margin-top: 22px;width: 90%;margin-left: 5%;">\n\n    <button ion-button full\n\n      style="margin: 0%;height: 50px;background: #87c23f;font-size: 18px;border-radius: 6px;text-transform: none;"\n\n      (click)="releaseAMT()">Proceed Securely</button>\n\n  </ion-row>\n\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/payment-secure/payment-secure.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"], __WEBPACK_IMPORTED_MODULE_3__ionic_native_network_interface__["a" /* NetworkInterface */]])
    ], PaymentSecurePage);
    return PaymentSecurePage;
}());

//# sourceMappingURL=payment-secure.js.map

/***/ })

});
//# sourceMappingURL=14.js.map