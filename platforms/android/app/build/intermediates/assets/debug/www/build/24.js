webpackJsonp([24],{

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BikeIssuePageModule", function() { return BikeIssuePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__bike_issue__ = __webpack_require__(507);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BikeIssuePageModule = /** @class */ (function () {
    function BikeIssuePageModule() {
    }
    BikeIssuePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__bike_issue__["a" /* BikeIssuePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__bike_issue__["a" /* BikeIssuePage */]),
            ],
        })
    ], BikeIssuePageModule);
    return BikeIssuePageModule;
}());

//# sourceMappingURL=bike-issue.module.js.map

/***/ }),

/***/ 507:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BikeIssuePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(208);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BikeIssuePage = /** @class */ (function () {
    function BikeIssuePage(navCtrl, navParams, barcodeScanner, formBuilder, geolocation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcodeScanner = barcodeScanner;
        this.formBuilder = formBuilder;
        this.geolocation = geolocation;
        this.BikeIssueForm = formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            bikenumber: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            note: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            location: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required]
        });
    }
    BikeIssuePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BikeIssuePage');
    };
    BikeIssuePage.prototype.ScanQrcode = function () {
        var _this = this;
        this.barcodeScanner.scan().then(function (barcodeData) {
            _this.scannedCode = barcodeData.text;
        }, function (err) {
            console.log('barcodeScanner Error: ', err);
        });
    };
    BikeIssuePage.prototype.BikeLocation = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            var that = _this;
            var initLat = resp.coords.latitude;
            var initLng = resp.coords.longitude;
            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(initLat, initLng);
            var request = {
                latLng: latlng
            };
            geocoder.geocode(request, function (data, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (data[0] != null) {
                        that.address_show = data[0].formatted_address;
                        that.locationShow = that.address_show;
                    }
                    else {
                        that.address_show = data[0].formatted_address;
                    }
                }
                else {
                }
            });
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    };
    BikeIssuePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-bike-issue',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/bike-issue/bike-issue.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Bike Issue</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <form class="form" [formGroup]="BikeIssueForm">\n\n    <p type="Email ID">\n\n      <input formControlName="name" type="text" placeholder="Write your Email here.." />\n\n    </p>\n\n    <span class="span"\n\n      *ngIf="!BikeIssueForm.controls.name.valid && (BikeIssueForm.controls.name.dirty || submitAttempt)">Email is\n\n      required and should be in valid format!</span>\n\n    <p type="Bike Number">\n\n      <input formControlName="bikenumber" type="text" placeholder="Write your Bike Number here...."\n\n        [(ngModel)]="scannedCode" />\n\n      <ion-icon name="qr-scanner" style="position: fixed;\n\n      right:35px;\n\n      top:38%;\n\n      z-index: 1000;\n\n      font-size: 23px;" (click)="ScanQrcode()"></ion-icon>\n\n    </p>\n\n    <span class="span"\n\n      *ngIf="!BikeIssueForm.controls.bikenumber.valid && (BikeIssueForm.controls.bikenumber.dirty || submitAttempt)">Bike\n\n      Number is required and should be in valid format!</span>\n\n    <p type="Bike Location">\n\n      <textarea rows="2" cols="50" formControlName="location" placeholder="please write your Location.."\n\n        [(ngModel)]="locationShow"></textarea>\n\n      <ion-icon name="locate" style="position: fixed;\n\n      right:35px;\n\n      top:53%;\n\n      z-index: 1000;\n\n      font-size: 23px;" (click)="BikeLocation()"></ion-icon>\n\n    </p>\n\n    <span class="span"\n\n      *ngIf="!BikeIssueForm.controls.location.valid && (BikeIssueForm.controls.location.dirty || submitAttempt)">please\n\n      write your Location!</span>\n\n\n\n    <p type="Describe">\n\n      <textarea rows="2" cols="50" formControlName="note" placeholder="What would you like to tell us.."></textarea>\n\n    </p>\n\n    <span class="span"\n\n      *ngIf="!BikeIssueForm.controls.note.valid && (BikeIssueForm.controls.note.dirty || submitAttempt)">please write\n\n      your Location!</span>\n\n    <button (tap)="contactUs()" style="text-align:center;">SUBMIT</button>\n\n  </form>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/bike-issue/bike-issue.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */]])
    ], BikeIssuePage);
    return BikeIssuePage;
}());

//# sourceMappingURL=bike-issue.js.map

/***/ })

});
//# sourceMappingURL=24.js.map