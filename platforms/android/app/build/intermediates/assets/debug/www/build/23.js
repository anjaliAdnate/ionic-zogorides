webpackJsonp([23],{

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookRidesModule", function() { return BookRidesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__book_ride__ = __webpack_require__(528);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BookRidesModule = /** @class */ (function () {
    function BookRidesModule() {
    }
    BookRidesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__book_ride__["a" /* BookRidesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__book_ride__["a" /* BookRidesPage */]),
            ],
        })
    ], BookRidesModule);
    return BookRidesModule;
}());

//# sourceMappingURL=book-ride.module.js.map

/***/ }),

/***/ 528:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BookRidesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__ = __webpack_require__(210);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BookRidesPage = /** @class */ (function () {
    function BookRidesPage(navCtrl, apiCall, loadingCtrl, socialSharing, barcodeScanner, navParams, app_api, toastCtrl, alertCtrl, menu) {
        this.navCtrl = navCtrl;
        this.apiCall = apiCall;
        this.loadingCtrl = loadingCtrl;
        this.socialSharing = socialSharing;
        this.barcodeScanner = barcodeScanner;
        this.navParams = navParams;
        this.app_api = app_api;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.menu = menu;
        this.navOptions = {
            animation: 'ios-transition'
        };
        this.tripaddresses = this.navParams.get('tripLocations');
        var uID = localStorage.getItem('details');
        var drpgeoID = JSON.parse(localStorage.getItem('droplocatoingeoId'));
        this.dropLocationgeoId = drpgeoID ? drpgeoID._id : '';
        var getData = this.app_api.getGeofence();
        this.pickupgeofenceId = getData['geofenceId'];
        this.useridd = JSON.parse(uID)._id;
        var a = new Date();
        this.plannedEndTIme = new Date(a);
    }
    BookRidesPage.prototype.ionViewDidLoad = function () {
        var initialTimer = localStorage.getItem('timerTIme');
        if (initialTimer) {
            var parseTime = JSON.parse(initialTimer);
            var startT = new Date(parseTime);
            var startS = startT.getTime();
            var currTime = new Date();
            var currSeconds = currTime.getTime();
            var timeDiff = currSeconds - startS;
            this.remainingSeconds = 600 - (timeDiff / 1000);
            this.remainingTime = this.remainingSeconds;
            localStorage.setItem("lockRide", JSON.stringify("rideLocked"));
        }
    };
    BookRidesPage.prototype.ionViewDidEnter = function () {
        this.menu.enable(true);
    };
    BookRidesPage.prototype.ngOnInit = function () {
        this.pickup_location = localStorage.getItem("pickupLocation");
        this.drop_location = localStorage.getItem("dropOffLocation");
        var pl;
        pl = localStorage.getItem('pickupLocation');
        if (pl) {
            this.pickup_location = JSON.parse(pl).name;
        }
        var dl;
        dl = localStorage.getItem('dropOffLocation');
        if (dl) {
            this.drop_location = JSON.parse(dl).name;
        }
        this.initTimer();
        this.startTimer();
    };
    BookRidesPage.prototype.initTimer = function () {
        if (!this.timeInSeconds) {
            this.timeInSeconds = 600;
        }
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    };
    BookRidesPage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    BookRidesPage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    BookRidesPage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    BookRidesPage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
            }
        }, 1000);
    };
    BookRidesPage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var minutesString = '';
        var secondsString = '';
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return minutesString + ':' + secondsString;
    };
    // ===================Cancel confirmaion Alert =====================
    BookRidesPage.prototype.cancelBookingAlert = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Cancel Booking',
            message: 'Are you sure,you want to cancel this ride ?',
            cssClass: 'alertStyle',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel'
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.releaseAMT();
                    }
                }
            ]
        });
        alert.present();
    };
    BookRidesPage.prototype.ScanQrcode = function () {
        var _this = this;
        var that = this;
        this.barcodeScanner.scan({ showTorchButton: true }).then(function (barcodeData) {
            that.IMEI = barcodeData.text;
            that.actualStartTime = new Date();
            that.startTrip();
        }, function (err) {
            _this.app_api.stopLoading();
            console.log('Error: ', err);
        });
    };
    BookRidesPage.prototype.tipStarted = function () {
        var that = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        var toast_1 = this.toastCtrl.create({
            message: 'Device in not inside station, Please goto nearest station',
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var that = this;
        var tempDetail = localStorage.getItem('tripDetails');
        var tempRouteDetail = JSON.parse(tempDetail);
        var startObj = {
            _id: tempRouteDetail._id,
            status: "LOADING",
            AcctualStartTime: new Date(),
            imei: this.IMEI
        };
        loading.present();
        this.app_api.updatetripDetails(startObj)
            .subscribe(function (res) {
            var device_Info = JSON.parse(res['_body']).device;
            var ID = JSON.parse(res['_body'])._id;
            var geofenceCheck = JSON.parse(res['_body']).status;
            if (geofenceCheck == "you are not within gofence") {
                loading.dismiss();
                toast_1.present();
            }
            else {
                var status = setInterval(function () {
                    var _this = this;
                    that.app_api.checkTripStatus(ID)
                        .subscribe(function (res) {
                        if (res.status == "SUCCESS") {
                            loading.dismiss();
                            clearInterval(status);
                            localStorage.removeItem('lockRide');
                            localStorage.setItem("active_Device", JSON.stringify(device_Info));
                            localStorage.setItem("tripstarted", "SUCCESS");
                            that.navCtrl.push("ConfirmBookingPage", { 'imei': that.IMEI }, _this.navOptions);
                        }
                    }, function (err) {
                        loading.dismiss();
                    });
                }, 5000);
            }
        }, function (err) {
            loading.dismiss();
            localStorage.removeItem('timerTIme');
        });
    };
    BookRidesPage.prototype.toastfunc = function (msg) {
        this.toastVar = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        this.toastVar.present();
        return this.toastVar;
    };
    BookRidesPage.prototype.startTrip = function () {
        var _this = this;
        var devObj = {
            user: this.useridd,
            imei: this.IMEI,
            command: "ON"
        };
        this.app_api.startLoading();
        this.app_api.addcommandQueue(devObj).subscribe(function (response) {
            if (response.message == "Device is not within geofence") {
                _this.app_api.stopLoading();
                _this.toastfunc(response.message);
            }
            else if (response.message == "Device Info updated and trip started!!!") {
                // this.app_api.stopLoading();
                _this.checkBikeAvailability(response._id);
            }
        }, function (err) {
            _this.app_api.stopLoading();
            _this.toastfunc("no device found");
        });
    };
    BookRidesPage.prototype.checkBikeAvailability = function (cmdqId) {
        var that = this;
        // this.app_api.startLoading();
        var status_1 = setInterval(function () {
            that.app_api.checkTripStatus(cmdqId)
                .subscribe(function (res) {
                if (res.status == "SUCCESS") {
                    // that.app_api.stopLoading();
                    clearInterval(status_1);
                    that.updateRide();
                }
            }, function (err) {
                that.app_api.stopLoading();
                that.toastfunc("Internal Server Error , Please Try after sometime !!!");
                clearInterval(status_1);
            });
        }, 5000);
    };
    BookRidesPage.prototype.updateRide = function () {
        var _this = this;
        var tempDetail = localStorage.getItem('tripDetails');
        var tempRouteDetail = JSON.parse(tempDetail);
        // this.app_api.startLoading();
        var startObj = {
            _id: tempRouteDetail._id,
            status: "LOADING",
            AcctualStartTime: new Date(),
            imei: this.IMEI,
            user: this.useridd
        };
        this.app_api.updatetripDetails(startObj)
            .subscribe(function (res) {
            _this.app_api.stopLoading();
            var device_Info = JSON.parse(res['_body']).device;
            var resMessage = JSON.parse(res['_body']).message;
            if (resMessage == "Device Info updated and trip started!!!") {
                localStorage.removeItem('lockRide');
                localStorage.setItem("active_Device", JSON.stringify(device_Info));
                localStorage.setItem("tripstarted", "SUCCESS");
                localStorage.setItem("tripStartTime", new Date().toISOString());
                _this.navCtrl.push("ConfirmBookingPage", { 'imei': _this.IMEI }, _this.navOptions);
            }
            else {
                _this.toastfunc(resMessage);
            }
        }, function (err) {
            _this.app_api.stopLoading();
            _this.toastfunc("Please Try after sometime !!!");
        });
    };
    BookRidesPage.prototype.releaseAMT = function () {
        var _this = this;
        this.app_api.startLoading();
        var releaseObj = {
            CUST_ID: this.useridd,
            app_id: 'zogo'
        };
        this.app_api.releaseAmount(releaseObj)
            .subscribe(function (res) {
            if (res.STATUS == "TXN_SUCCESS") {
                _this.app_api.stopLoading();
                _this.canceltripRide();
            }
        }, function (err) {
            _this.app_api.stopLoading();
        });
    };
    BookRidesPage.prototype.canceltripRide = function () {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: 'Trip cancelled',
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        var reason = "plan changed";
        var cancellationTime = new Date();
        var tempDetail = localStorage.getItem('tripDetails');
        var tempRouteDetail = JSON.parse(tempDetail);
        var cancelObj = {
            "_id": tempRouteDetail._id,
            "cancelledAt": cancellationTime,
            "status": "CANCELLED",
            "device": tempRouteDetail.device,
            "cancelledBy": this.useridd,
            "cancellationReason": reason
        };
        this.app_api.startLoading();
        this.app_api.canceltripDetails(cancelObj)
            .subscribe(function (res) {
            _this.app_api.stopLoading();
            toast.present();
            localStorage.removeItem('tripDetails');
            localStorage.removeItem('pickupLocation');
            localStorage.removeItem("dropOffLocation");
            localStorage.removeItem('timerTIme');
            localStorage.removeItem('lockRide');
            _this.navCtrl.setRoot("LivePage");
        }, function (err) {
            _this.app_api.stopLoading();
            _this.navCtrl.setRoot("TripPage");
        });
    };
    BookRidesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-book-ride',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/bookRide/book-ride.html"*/'<ion-content no-padding>\n\n    <div width="100%" height="100%">\n\n            <div style="background: #8eda4a;padding-top: 15%;padding-bottom: 10%;" height="50%">\n\n                    <ion-card style="margin: 0px;width: 28%;border-radius: 15px; padding-top: 0%;margin-left: 37%;">\n\n                        <img src="assets/icon/zogologo.png" style="width: 60%;margin: 18%;">\n\n                    </ion-card>\n\n                    <h5 style="color: white;font-weight: 500;text-align: center;font-size: 2.4rem;padding-top: 0%;padding-bottom: 7%;">RIDE BOOKED</h5>\n\n                </div>\n\n       <div class="row">         \n\n    <ion-card ion-card style="margin: -60px 0px 0px 12px;border-radius: 10px;padding:15px;" class="col-sm-12 col-12 col-md-12">\n\n        <h1\n\n            style="text-align: center;font-size: 62px;color: #fb7d0c;font-family: fantasy;letter-spacing: 5px;">\n\n            {{displayTime}}</h1>\n\n    </ion-card>\n\n</div>\n\n    <ion-grid no-padding style="padding-top: 26px;">\n\n        <ion-row style="width: 90%;\n\n        background: #ffffff;\n\n        margin-left: 5%;\n\n        border-radius:8px;\n\n        box-shadow: 0px 1px 15px 3px #cfcfcf;">\n\n            <ion-col col-2>\n\n                <ion-icon style="font-size: 46px;color: #8eda4a;margin-top: 9px;margin-left: 8px;margin-bottom: 9px;"\n\n                    name="md-pin"></ion-icon>\n\n            </ion-col>\n\n            <ion-col col-10 style="margin-top: 5px;">\n\n                <p style="font-size: 18px;margin-top: 8px;margin-bottom: 0;">Pickup Location</p>\n\n                <p style="margin: 0;margin-top: 3px;color: grey;">{{pickup_location}}</p>\n\n            </ion-col>\n\n        </ion-row>\n\n        <ion-row style="width: 90%;\n\n        background: #ffffff;\n\n        margin-left: 5%;\n\n        margin-top: 4%;\n\n        border-radius:8px;\n\n        box-shadow: 0px 1px 15px 3px #cfcfcf;">\n\n            <ion-col col-2>\n\n                <ion-icon style="font-size: 46px;color: #8eda4a;margin-top: 9px;margin-left: 8px;margin-bottom: 9px;"\n\n                    name="md-pin"></ion-icon>\n\n            </ion-col>\n\n            <ion-col col-10 col-10 style="margin-top: 5px;">\n\n                <p style="font-size: 18px;margin-top: 8px;margin-bottom: 0;">Drop Location</p>\n\n                <p style="margin: 0;margin-top: 3px;color: grey;">{{drop_location}}</p>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n</div>\n\n</ion-content>\n\n<ion-footer style="width: 90%;\n\nheight: 100px;\n\nbackground: #ffffff;\n\nmargin-left: 5%;\n\nborder-top-left-radius: 8px;\n\nborder-top-right-radius: 8px;\n\nbox-shadow: 0px 1px 15px 3px #cfcfcf;">\n\n    <ion-row style="margin-top: 22px;width: 90%;margin-left: 5%;">\n\n        <ion-col>\n\n            <button ion-button full\n\n                style="margin: 0%;height:50px;background:#87c23f;font-size: 14px;border-radius: 8px;"\n\n                (click)="cancelBookingAlert()">Cancel Ride</button>\n\n        </ion-col>\n\n        <ion-col>\n\n            <button ion-button full\n\n                style="margin: 0%;height:50px;background:#87c23f;font-size: 14px;border-radius: 8px;"\n\n                (click)="ScanQrcode()">\n\n                <ion-icon name="qr-scanner"></ion-icon>&nbsp;&nbsp;Unlock Bike\n\n            </button>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/bookRide/book-ride.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"], __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__["a" /* BarcodeScanner */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"]])
    ], BookRidesPage);
    return BookRidesPage;
}());

// savtrip is the first method call when coming from drop location page 
//scanqr method calls after scaning QR trip started is attached with scanQR method (Abinash )
//# sourceMappingURL=book-ride.js.map

/***/ })

});
//# sourceMappingURL=23.js.map