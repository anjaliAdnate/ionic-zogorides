import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PickupLocationSearchPage } from './pickup-location-search';

@NgModule({
  declarations: [
    PickupLocationSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(PickupLocationSearchPage),
  ],
})
export class PickupLocationSearchPageModule {}
