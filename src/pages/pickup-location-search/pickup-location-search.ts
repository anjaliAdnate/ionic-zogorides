import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, MenuController } from 'ionic-angular';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-pickup-location-search',
  templateUrl: 'pickup-location-search.html',
})
export class PickupLocationSearchPage {
  SearchLocation: any;
  autocompleteItems: any;
  autocomplete: any;
  acService: any;
  placesService: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public menu: MenuController
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PickupLocationSearchPage');
  }

  ionViewDidEnter() {
    this.menu.enable(false);
  }

  ngOnInit() {
    this.acService = new google.maps.places.AutocompleteService();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  updateSearch() {
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let self = this;
    let config = {
      input: this.autocomplete.query,
      componentRestrictions: {}
    }

    this.acService.getPlacePredictions(config, function (predictions, status) {
      self.autocompleteItems = [];
      predictions.forEach(function (prediction) {
        self.autocompleteItems.push(prediction);
      });
    });
  }

  chooseItem(item) {
    let pickup_location = item.place_id;
    let description = item.description;
    let pickupObj = {
      'id': pickup_location,
      'name': description
    }
    localStorage.setItem("pickupLocation", JSON.stringify(pickupObj))
    this.navCtrl.setRoot("LivePage")
  }

  searchInput(ev) { }

  onCancel($event) {
    this.viewCtrl.dismiss()
  }
}
