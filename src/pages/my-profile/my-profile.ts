import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-my-profile',
  templateUrl: 'my-profile.html',
})
export class MyProfilePage {
  userprofile: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, public menu: MenuController, public formBuilder: FormBuilder, ) {

    this.userprofile = formBuilder.group({
      uid: ["", Validators.required],
      yourRide: ["", Validators.required],
      payment: ["", Validators.required],
      help: ["", Validators.required],
      user_doc: ["", Validators.required]
    })
  }

  ionViewDidEnter() { }

  ionViewDidLoad() {
    this.menu.enable(false);
  }

  goBack() {
    this.navCtrl.pop();
  }
}
