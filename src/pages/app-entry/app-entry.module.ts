import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppEntryPage } from './app-entry';

@NgModule({
  declarations: [
    AppEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(AppEntryPage),
  ],
})
export class AppEntryPageModule {}
