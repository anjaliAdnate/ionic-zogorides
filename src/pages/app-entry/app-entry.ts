import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-app-entry',
  templateUrl: 'app-entry.html',
})
export class AppEntryPage {

  constructor(public navCtrl: NavController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppEntryPage');
  }

  signUpPage() {
    this.navCtrl.push('SignupPage')
  }

  loginPage() {
    this.navCtrl.push('LoginPage')
  }
}
