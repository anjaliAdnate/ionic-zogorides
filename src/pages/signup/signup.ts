import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Platform, Keyboard } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SignupOtp } from './signupOtp';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  signupForm: FormGroup;
  isdealer: boolean;
  submitAttempt: boolean;
  usersignupdetails: any;
  signupUseradd: any;
  responseMessage: string;
  signupDetails: any;
  type1 = "password";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public apiService: ApiServiceProvider,
    private toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public platform: Platform,
    public keyboard: Keyboard,

  ) {
    this.signupForm = formBuilder.group({
      mob_num: ["",Validators.compose([Validators.minLength(10),Validators.maxLength(13),  Validators.required])],
      email_add: ["", Validators.email],
      Name: ["", Validators.required],
      pass: ["", Validators.required],
      cnfrm_passwrd: ["", Validators.required],
      dlno: ["", Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  doLogin() {
    this.navCtrl.setRoot("LoginPage");
  }

  IsDealer(check) {
    this.isdealer = check;
  }

  getotp() {
    this.isdealer = false;
    this.submitAttempt = true;
    if (this.signupForm.valid) {
      this.usersignupdetails = this.signupForm.value;
      localStorage.setItem('usersignupdetails', this.usersignupdetails);
      this.signupDetails = localStorage.getItem("usersignupdetails");
      if (this.signupForm.value.cnfrm_passwrd && this.signupForm.value.email_add && this.signupForm.value.mob_num && this.signupForm.value.Name && this.signupForm.value.pass) {
        if (this.signupForm.value.pass == this.signupForm.value.cnfrm_passwrd) {

          var usersignupdata = {
            "first_name": this.signupForm.value.Name,
            "last_name": '',
            "email": this.signupForm.value.email_add,
            "password": this.signupForm.value.pass,
            "confirmpass": this.signupForm.value.cnfrm_passwrd,
            "org": '5c179c37cca2711e341a2031',
            "phone": String(this.signupForm.value.mob_num),
            "purpose": 'zogo',
            "dl_number": this.signupForm.value.dlno,
            "status": false
          }

          this.apiService.startLoading();
          this.apiService.signupApi(usersignupdata)
            .subscribe(response => {
              var phone = usersignupdata.phone;
              localStorage.setItem("mobnum", phone)
              this.apiService.stopLoading();
              this.signupUseradd = response;
              let toast = this.toastCtrl.create({
                message: response.message,
                duration: 3000,
                position: 'top'
              });

              toast.onDidDismiss(() => {
                if (response.message === 'Email ID or Mobile Number already exists') {
                  this.navCtrl.push("LoginPage");
                } else if (response.message === "OTP sent successfully") {
                  this.navCtrl.push(SignupOtp);
                }
              });
              toast.present();
            },
            err => {
              this.apiService.stopLoading();
              let toast = this.toastCtrl.create({
                message: "Something went wrong. Please check your net connection..",
                duration: 2500,
                position: "top"
              })
              toast.present();
            })
        } else {
          var alertPopup = this.alertCtrl.create({
            title: 'Warning!',
            message: "Password and Confirm Password Not Matched",
            buttons: ['OK']
          });
          alertPopup.present();
        }
      }
    }
  }

  gotoOtp() {
    this.navCtrl.push(SignupOtp);
  }

  gotoLogin() {
    this.navCtrl.push("LoginPage");
  }

  type = "password";
  show = false;
  show1 = false;
  toggleShow(ev) {
    if (ev == 0) {
      this.show = !this.show;
      if (this.show) {
        this.type = "text";
      }
      else {
        this.type = "password";
      }
    } else {
      this.show1 = !this.show1;
      if (this.show1) {
        this.type1 = "text";
      }
      else {
        this.type1 = "password";
      }
    }
  }

  upload() {
    this.navCtrl.push("DrivingLicensePage");
  }

  goBack() {
    this.navCtrl.pop();
  }


  // =================================Facebook SignUp =====================================================

  FacebookSignup() {
    //   this.fb.login(['public_profile', 'user_friends', 'email'])
    //     .then((res: FacebookLoginResponse) => console.log('Logged into Facebook!', res))
    //     .catch(e => console.log('Error logging into Facebook', e))
    //   // this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);
    // }

  }

  // =================================Facebook SignUp =====================================================

}
