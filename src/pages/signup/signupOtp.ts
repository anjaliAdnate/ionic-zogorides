import { Component } from "@angular/core";
import { NavController, NavParams, ToastController, AlertController, ViewController, Platform } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import { AndroidPermissions } from '@ionic-native/android-permissions';

// declare var SMS :any;
// declare var cordova: any;

@Component({
    selector: 'page-signup',
    templateUrl: './signupOtp.html'
})
export class SignupOtp {
    signupForm: FormGroup;
    signupverify: any;
    mobileNum: string;
    fetchOtp: any;
    Otp: any;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public androidPermissions: AndroidPermissions,
        public formBuilder: FormBuilder,
        public apiService: ApiServiceProvider,
        public platform: Platform,
        private toastCtrl: ToastController,
        public alerCtrl: AlertController,
        public viewCtrl: ViewController) {
        this.mobileNum = localStorage.getItem('mobnum');
        this.signupForm = formBuilder.group({
            otp1: ["", Validators.required],
            otp2: ["", Validators.required],
            otp3: ["", Validators.required],
            otp4: ["", Validators.required]

        })    
    }

    // ionViewWillEnter() {
    //     this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(   
    //       success => console.log('Permission granted'),
    //       err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
    //     );  
    //     this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);     
    //     this.autoOtpReader();
    //   }


    // autoOtpReader() {
    //     this.platform.ready().then((readySource) => {
         
    //         if (SMS) SMS.startWatch(() => {
    //             console.log('watching started');
    //         }, Error => {
    //             console.log('failed to start watching');
    //         });
    //         document.addEventListener('onSMSArrive', (e: any) => {
    //             var sms = e.data;
    //             console.log(sms);
    //             this.fetchOtp = e.data.body.match(/[0-9]+/g);
    //             console.log(this.fetchOtp);
    //             this.Otp = this.fetchOtp[0];
    //             console.log(this.Otp);
    //         })
    //     })
    // }

    getCodeBoxElement(index) {
        let inputValue = (document.getElementById('codeBox' + index) as HTMLInputElement);
        return inputValue;
    }

    onKeyUpEvent(index, event) {
        const eventCode = event.which || event.keyCode;
        if (this.getCodeBoxElement(index).value.length === 1) {
            if (index !== 4) {
                this.getCodeBoxElement(index + 1).focus();
            } else {
                this.getCodeBoxElement(index).blur();
            }
        }
        if (eventCode === 8 && index !== 1) {
            this.getCodeBoxElement(index - 1).focus();
        }
    }
    onFocusEvent(index) {
        for (var item = 1; item < index; item++) {
            const currentElement = this.getCodeBoxElement(item);
            if (!currentElement.value) {
                currentElement.focus();
                break;
            }
        }
    }
    signupUser() {
        var otp = this.signupForm.value.otp1.toString() + this.signupForm.value.otp2.toString() + this.signupForm.value.otp3.toString() + this.signupForm.value.otp4.toString();
        var usersignup = {
            "phone": this.mobileNum,
            "otp": otp
        }

        this.apiService.startLoading();
        this.apiService.siginupverifyCall(usersignup)
            .subscribe(data => {
                this.apiService.stopLoading();
                this.signupverify = data;
                this.signupverify.phoneNum = this.mobileNum;
                let toast = this.toastCtrl.create({
                    message: 'Mobile Verified',
                    position: 'bottom',
                    duration: 2000
                });

                toast.onDidDismiss(() => {
                    console.log('Dismissed toast');
                    this.navCtrl.push("DrivingLicensePage", { signupObj: this.signupverify });
                });

                toast.present();
            },
                err => {
                    this.apiService.stopLoading();
                    var body = err._body;
                    var msg = JSON.parse(body);
                    let toastErr = this.toastCtrl.create({
                        message: msg.message,
                        position: 'top',
                        duration: 2000
                    });
                    toastErr.present();
                })
    }

    resendOtp() {
        var phoneNumber = {
            ph_num : this.mobileNum
        } 
        this.apiService.resendOtp(phoneNumber)
            .subscribe(res=>{
                if(res) {
                    let toast = this.toastCtrl.create({
                        message: "OTP sent successfully",
                        duration: 2000,
                        position: "top"
                    });
                    toast.present();
                }
            },err=>{
                console.log(err);
            })
    }

    goBack() {
        this.navCtrl.pop();
    }

}