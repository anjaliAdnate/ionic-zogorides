import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupPage } from './signup';

@NgModule({
  declarations: [
    SignupPage,
    // SignupOtp
  ],
  imports: [
    IonicPageModule.forChild(SignupPage),
  ],
  entryComponents: [
    // SignupOtp
  ]
})
export class SignupPageModule {}
