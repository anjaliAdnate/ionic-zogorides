import { Component, NgZone } from "@angular/core";
import { NavParams, ViewController, Platform, ToastController, AlertController } from "ionic-angular";
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { ApiServiceProvider } from "../../providers/api-service/api-service";

@Component({
    selector: 'page-modal-offline',
    templateUrl: './offline-modal.html'
})

export class OfflineModalPage {
    devices: any[] = [];
    statusMessage: string;
    peripheral: any;
    btnText: string;

    constructor(
        public plt: Platform,
        public navparam: NavParams,
        public viewCtrl: ViewController,
        private bluetoothSerial: BluetoothSerial,
        private ngZone: NgZone,
        private toastCtrl: ToastController,
        public alertCtrl: AlertController,
        public apiCall: ApiServiceProvider,
    ) {

        console.log("key: ", navparam.get("key"));
        if (navparam.get("key") == "0") {
            this.btnText = "Turn off vehicle";
        } else {
            if (navparam.get("key") == "1") {
                this.btnText = "Turn on vehicle";
            }
        }


        this.plt.ready().then((readySource) => {
            // this.bluetoothSerial.setDiscoverable(1000);
            this.bluetoothSerial.setName("Zogorides");
            console.log('Platform ready from', readySource);
        });
    }

    ionViewDidEnter() {
        console.log('ionViewDidEnter');
        this.scan();
    }

    scan() {
        this.setStatus('Scanning for Bluetooth Devices ...');
        this.devices = [];  // clear list
        this.bluetoothSerial.discoverUnpaired().then(
            (device) => {
                debugger
                this.onDeviceDiscovered(device)
            },
            error => this.scanError(error)
        );

        setTimeout(this.setStatus.bind(this), 10000, 'Scan complete');
    }

    onDeviceDiscovered(device) {
        console.log('Discovered ' + JSON.stringify(device, null, 2));
        this.ngZone.run(() => {
            // this.devices.push(device);
            this.devices = device;
        });
    }

    // If location permission is denied, you'll end up here
    scanError(error) {
        this.setStatus('Error ' + error);
        let toast = this.toastCtrl.create({
            message: 'Error scanning for Bluetooth low energy devices',
            position: 'middle',
            duration: 5000
        });
        toast.present();
    }

    setStatus(message) {
        console.log(message);
        this.ngZone.run(() => {
            this.statusMessage = message;
        });
    }

    deviceSelected(device) {
        this.alertCtrl.create({
            message: "Connect this device?",
            buttons: [{
                text: 'Accept',
                handler: () => {
                    this.connectDevice(device)
                }
            },
            {
                text: 'Cancle',
            }
            ]
        }).present();
    }

    connectDevice(device) {
        this.apiCall.startLoading123('Connecting to ' + device.name || device.id);
        this.bluetoothSerial.connect(device.id)
            .subscribe((peripheral) => {
                this.apiCall.stopLoading();
                console.log("connected: ", peripheral);
                this.peripheral = peripheral;
                // this.apiCall.startLoading();
                this.bluetoothSerial.subscribe('\n')
                    .subscribe((data) => {
                        // this.apiCall.stopLoading();
                        console.log("subscribed data: ", data)
                        if (data) {
                            this.alertCtrl.create({
                                message: 'Thank you. Vehicled successfully turned off.',
                                buttons: [{
                                    text: 'proceed',
                                    handler: () => {
                                        this.viewCtrl.dismiss();
                                    }
                                }]
                            }).present();
                        }

                        this.bluetoothSerial.available()
                            .then((number: any) => {
                                this.bluetoothSerial.read()
                                    .then((data: any) => {
                                        console.log("read data: ", data);
                                    });
                            });
                    },
                        (err) => {
                            // this.apiCall.stopLoading();
                            console.log("subscriber err: ", err)
                        });
            },
                (err) => {
                    this.apiCall.stopLoading();
                    console.log("error found: ", err)
                    this.toastCtrl.create({
                        message: err,
                        duration: 2000,
                        position: 'middle'
                    }).present();
                })
    }

    setONOFF(str) {
        var ignstr;
        if (str == 'Turn off vehicle') {
            ignstr = '++S*R:IMOFF#;';
        } else {
            ignstr = '++S*R:IMON#;';
        }
        this.apiCall.startLoading();
        this.bluetoothSerial.write(ignstr)
            .then((resp) => {
                this.apiCall.stopLoading();
                console.log("off command: " + resp)
                // this.bluetoothSerial.available()
                //     .then((number: any) => {
                //         this.bluetoothSerial.read()
                //             .then((data: any) => {
                //                 console.log("read data: ", data);
                //             });
                //     });
            },
                err => {
                    this.apiCall.stopLoading();
                    console.log("error found in setOFF: ", err)
                    this.toastCtrl.create({
                        message: err,
                        duration: 2000,
                        position: 'bottom'
                    }).present();
                });
    }
}