import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, Platform, MenuController, ModalController, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { GoogleMaps, Marker, GoogleMapsEvent, MyLocation, GoogleMapsAnimation } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
declare var google: any;

class Port {
  public id: number;
  public name: string;
}

@IonicPage()
@Component({
  selector: 'page-live',
  templateUrl: 'live.html',
})

export class LivePage implements OnInit, OnDestroy {

  navOptions = {
    animation: 'ios-transition'
  };

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('directionsPanel') directionsPanel: ElementRef;

  map: any;
  latLng: any;
  markers: any;

  total_distance: any;
  total_veh: any;
  mapOptions: any;

  startService: any;
  autocompleteStart: any;

  endService: any;
  autocompleteEnd: any;

  start: any;
  end: any;
  travelType: any = 'DRIVING';

  distance: any = '';
  duration: any = '';
  shouldShowCancel: any;
  items: any;
  gefence: any;
  gefencename: any;
  geofenceShowdata: any;
  geoshape: any = {};
  GeoFencCoords: any;
  cartCount: any;
  drawerHidden = false;
  shouldBounce = true;
  disableBooking = true;
  dockedHeight = 150;
  bounceThreshold = 500;
  distanceTop = 56;

  ports: Port[];
  port: Port;
  allData: any = {};
  socketSwitch: any = {};
  socketChnl: any = [];
  _io: any;
  portstemp: any;
  selectedVehicle: any;
  showBtn: boolean;
  SelectVehicle: string;
  titleText: any;
  showActionSheet: boolean = true;
  vehicle_speed: any;
  todays_odo: any;
  fuel: any;
  address: void;
  drop_location: string;
  pickup_location: string;

  wayPoints: any;
  path: any[];
  islogin: any;
  cordin: any[];
  polydatageo: any[];
  geocoder: any;
  locationShow: any;
  drop_locationShow: any;
  pickupObj: any;
  dropOffObj: any;
  address_show: any;
  bounds: any = [];
  generalPolygon = [];
  remMarker: Marker;
  assignVehicle: any;
  geoName: any = [];
  iconMarker: string;
  iconUser: string;

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    public elementRef: ElementRef,
    public platform: Platform,
    public geolocation: Geolocation,
    public menu: MenuController,
    public locationAccuracy: LocationAccuracy,
    public events: Events
  ) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    ///////////////////////////////
    this.events.publish('user:updated', this.islogin);
    /////////////////////////////////
    this.pickup_location = JSON.parse(localStorage.getItem("pickupLocation"));
    this.drop_location = localStorage.getItem("dropOffLocation");
    // this.enableLocation();

    
    if (this.platform.is('ios')) {
      this.iconMarker = 'www/assets/icon/marker.png';
      this.iconUser = 'www/assets/icon/user.png';
    } else {
      this.iconMarker = './assets/icon/marker.png';
      this.iconUser = './assets/icon/user.png';
    }
  }

  portChange(event: {
    component: SelectSearchableComponent,
    value: any
  }) { }

  ngOnInit() {
    this.startService = new google.maps.places.AutocompleteService();
    this.autocompleteStart = [];
    this.endService = new google.maps.places.AutocompleteService();
    this.autocompleteEnd = [];
    this.start = '';
    this.end = '';

    this.drop_location = "";
    this.pickup_location = "";
    this.locationShow = "";
    this.drop_locationShow = "";
    this.enableLocation();
  }

  enableLocation() {
    var that = this;
    console.log("Inside Enable Location Function !!!");
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => {
            console.log('Request successful');
            //  that.sethome();
          },
          error => {
            console.log("Inside Error block !!!")
            console.log('Error requesting location permissions' + JSON.stringify(error))
          }


        );
      }
    });
  }

  ionViewDidLoad() {
    this.allData.map = GoogleMaps.create('map_canvas');
    var pl: any;
    this.showActionSheet = false;
    pl = localStorage.getItem('pickupLocation');
    if (pl) {
      var plDummy = JSON.parse(pl).id;
      if (plDummy != 'dummyId') {
        this.pickup_location = JSON.parse(pl).id;
        this.locationShow = JSON.parse(pl).name;
        this.pickupObj = pl;
        this.chooseStart();
      } else {
        this.loadMap();
      }
    } else {
      this.loadMap();
    }
  }

  loadMap() {

    this.allData.map.one(GoogleMapsEvent.MAP_READY).then((res) => {
      if (res) console.log('map ready: ', res);
    });
    var that = this;
    this.allData.map.getMyLocation()
      .then((location: MyLocation) => {
        that.fetchAdd(location.latLng);
        that.allData.map.animateCamera({
          target: location.latLng,
          zoom: 17,
          tilt: 30,
          duration: 50,
        })
          .then(() => {
            let marker: Marker = that.allData.map.addMarkerSync({
              title: "Current Location",
              position: location.latLng,
              icon: that.iconUser,
              animation: GoogleMapsAnimation.BOUNCE
            });
            this.remMarker = marker;
          })
      })
  }

  fetchAdd(latlng) {
    var that = this;
    var templatlng = JSON.parse(latlng);
    var initLat = templatlng.lat;
    var initLng = templatlng.lng;
    var geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(initLat, initLng);
    var request = {
      latLng: latlng
    };
    geocoder.geocode(request, function (data, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (data[0] != null) {
          that.address_show = data[0].formatted_address;
          that.locationShow = that.address_show;
          let pickup_location = "dummyId";
          let description = that.locationShow;
          let pickupObj = {
            'id': pickup_location,
            'name': description
          }
          localStorage.setItem("pickupLocation", JSON.stringify(pickupObj));
        } else {
          that.address_show = data[0].formatted_address;
        }
      }
      else { }
    })
    this.getgeofence(initLat, initLng);
  }

  ionViewDidEnter() {
    this.menu.enable(true);
  }

  ngOnDestroy() {
    for (var i = 0; i < this.socketChnl.length; i++)
      this._io.removeAllListeners(this.socketChnl[i]);
    localStorage.removeItem("LiveDevice")
  }

  getgeofence(lat, lng) {
    let that = this;
    this.geoName = [];
    var c = {
      lat: lat,
      lng: lng
    }
    that.bounds.push(c);
    var baseURLp = 'https://www.oneqlik.in/zogo/nearby/src?l=' + lat + ',' + lng;
    that.apiCall.getgeofenceCall(baseURLp)
      .subscribe(data => {
        that.geofenceShowdata = data.result;
        that.polydatageo = [];
        for (var i = 0; i < that.geofenceShowdata.length; i++) {
          that.cordin = [];
          that.geoName.push(that.geofenceShowdata[i].geoname);
          var a = that.geofenceShowdata[i].geofence.coordinates[0];
          for (var k = 0; k < a.length; k++) {
            var b = {
              lat: a[k][1],
              lng: a[k][0]
            }
            that.cordin.push(b);
            that.bounds.push(b);
          }
          that.polydatageo.push(that.cordin);
        }

        // two varaible are there that.polydatageo and polydata
        var j = 0;
        function reCur(j) {

          if (j < that.polydatageo.length) {
            var polydata = that.polydatageo[j];
            that.allData.map.addMarker({
              title: that.geoName[j],
              icon: that.iconMarker,
              styles: {
                'text-align': 'center',
                'font-style': 'italic',
                'font-weight': 'bold',
                'color': 'green'
              },
              position: { lat: polydata[0].lat, lng: polydata[0].lng },
            }).then((marker: Marker) => {
              that.generalPolygon.push(marker);
              that.polyRef[marker.getId()] = that.geofenceShowdata[j]
              marker['_objectInstance'].on(GoogleMapsEvent.MARKER_CLICK,
                () => {
                  that.total_distance = (Math.round((that.polyRef[marker.getId()].dist.calculated) * 10) / 10).toFixed(1) + " kms";
                  that.total_veh = that.polyRef[marker.getId()].devicesWithin.length;
                  that.assignVehicle = that.polyRef[marker.getId()].devicesWithin[0];
                  var geofenceDetails = {
                    totalDist: that.total_distance,
                    totalVeh: that.total_veh,
                    geofenceId: that.polyRef[marker.getId()]._id,
                    device: that.assignVehicle
                  }
                  that.apiCall.setGeofenceData(geofenceDetails);
                  that.showActionSheet = true;
                })
              reCur(j + 1);
            });
          } else {
            that.allData.map.animateCamera({
              target: that.bounds,
              duration: 50
            })
            
            let marker: Marker = that.allData.map.addMarkerSync({
              title: "Current Location",
              icon: that.iconUser,
              animation: GoogleMapsAnimation.BOUNCE,
            });
          }
        }
        reCur(0)
      },
        err => {
          console.log(err)
        });
  }
  polyRef(polyRef: any): any {
    throw new Error("Method not implemented.");
  }

  search_places() {
    this.navCtrl.push("PickupLocationSearchPage", null, this.navOptions);
  }

  chooseStart() {
    let that = this;
    this.autocompleteStart = [];
    var geocoderStart = new google.maps.Geocoder;

    geocoderStart.geocode({ 'placeId': this.pickup_location }, (results, status) => {
      if (status === 'OK' && results[0]) {
        let position = {
          lat: results[0].geometry.location.lat,
          lng: results[0].geometry.location.lng
        };

        let picLat = results[0].geometry.location.lat();
        let picLng = results[0].geometry.location.lng();
        that.getgeofence(picLat, picLng);

        var latLng1 = ({ lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() })

        that.allData.map.addMarker({
          icon: that.iconUser,
          position: latLng1,
        }).then((marker: Marker) => {
          that.remMarker = marker;
        });

        var startLat = results[0].geometry.location.lat();
        var startlng = results[0].geometry.location.lng();
        that.allData.map.animateCamera({
          target: { lat: startLat, lng: startlng },
          zoom: 13,
          duration: 50,
          padding: 0 // default = 20px
        });
        that.allData.map.setCameraTarget(latLng1);
      }
    })
  }

  availableStations() {
    this.navCtrl.push("DropLocationPage", null, this.navOptions);
  }

  sethome() {
    this.generalPolygon;
    for (var i = 0; i < this.generalPolygon.length; i++) {
      this.generalPolygon[i].remove();
    }
    if (this.remMarker) {
      this.remMarker.remove();
    }
    this.bounds = [];
    localStorage.removeItem('pickupLocation');
    this.loadMap();
  }

  OpenPopUp() {
    let modal = this.modalCtrl.create('ZogomodalPage');
    modal.onDidDismiss(() => { })
    modal.present();
  }

  navigate() {
    this.apiCall.navigateTO()
      .subscribe(res => { })
  }

}




