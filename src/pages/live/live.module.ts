import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LivePage } from './live';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { IonBottomDrawerModule } from '../../../node_modules/ion-bottom-drawer';
@NgModule({
  declarations: [
    LivePage,
  ],
  imports: [
    IonicPageModule.forChild(LivePage),
    SelectSearchableModule,
    IonBottomDrawerModule
  ],
})
export class LivePageModule {}
