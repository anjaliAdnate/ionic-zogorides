import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookRidesPage } from './book-ride';

@NgModule({
  declarations: [
    BookRidesPage,
  ],
  imports: [
    IonicPageModule.forChild(BookRidesPage),
  ],
})
export class BookRidesModule {}
