import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, MenuController, LoadingController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-book-ride',
  templateUrl: 'book-ride.html',
})
export class BookRidesPage implements OnInit {
  navOptions = {
    animation: 'ios-transition'
  };
  timeInSeconds: any;
  time: any;
  runTimer: boolean;
  hasStarted: boolean;
  hasFinished: boolean;
  remainingTime: any;
  displayTime: any;
  pickup_location: string;
  drop_location: string;
  tripaddresses: any;

  plannedEndTIme: Date;
  useridd: any;
  dropLocationgeoId: any;
  pickupgeofenceId: void;
  remainingSeconds: number;
  actualStartTime: Date;
  liveDataShare: any;
  resToken: any;
  deviceId: any;

  constructor(public navCtrl: NavController, public apiCall: ApiServiceProvider, public loadingCtrl: LoadingController, private socialSharing: SocialSharing, public barcodeScanner: BarcodeScanner, public navParams: NavParams, public app_api: ApiServiceProvider, private toastCtrl: ToastController, public alertCtrl: AlertController, private menu: MenuController) {

    this.tripaddresses = this.navParams.get('tripLocations');
    var uID = localStorage.getItem('details');
    var drpgeoID = JSON.parse(localStorage.getItem('droplocatoingeoId'));
    this.dropLocationgeoId = drpgeoID?drpgeoID._id:'';
    var getData = this.app_api.getGeofence();
    this.pickupgeofenceId = getData['geofenceId'];
    this.useridd = JSON.parse(uID)._id;
    var a = new Date();
    this.plannedEndTIme = new Date(a);
  }

  changeTimer: any;

  ionViewDidLoad() {
    var initialTimer = localStorage.getItem('timerTIme');
    if (initialTimer) {
      var parseTime = JSON.parse(initialTimer);
      var startT = new Date(parseTime);
      var startS = startT.getTime();
      var currTime = new Date();
      var currSeconds = currTime.getTime();
      var timeDiff = currSeconds - startS;
      this.remainingSeconds = 600 - (timeDiff / 1000);
      this.remainingTime = this.remainingSeconds;
      localStorage.setItem("lockRide", JSON.stringify("rideLocked"));
    }
  }

  ionViewDidEnter() {
    this.menu.enable(true);
  }

  ngOnInit() {
    this.pickup_location = localStorage.getItem("pickupLocation");
    this.drop_location = localStorage.getItem("dropOffLocation");
    var pl: any;
    pl = localStorage.getItem('pickupLocation');
    if (pl) {
      this.pickup_location = JSON.parse(pl).name;
    }
    var dl: any;
    dl = localStorage.getItem('dropOffLocation');
    if (dl) {
      this.drop_location = JSON.parse(dl).name;
    }
    this.initTimer();
    this.startTimer();
  }

  initTimer() {
    if (!this.timeInSeconds) {
      this.timeInSeconds = 600;
    }
    this.time = this.timeInSeconds;
    this.runTimer = false;
    this.hasStarted = false;
    this.hasFinished = false;
    this.remainingTime = this.timeInSeconds;
    this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
  }

  startTimer() {
    this.runTimer = true;
    this.hasStarted = true;
    this.timerTick();
  }

  pauseTimer() {
    this.runTimer = false;
  }

  resumeTimer() {
    this.startTimer();
  }

  timerTick() {
    setTimeout(() => {
      if (!this.runTimer) { return; }
      this.remainingTime--;
      this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);

      if (this.remainingTime > 0) {
        this.timerTick();
      }
      else {
        this.hasFinished = true;
      }
    }, 1000);
  }

  getSecondsAsDigitalClock(inputSeconds: number) {
    var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);
    var minutesString = '';
    var secondsString = '';
    minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
    secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
    return minutesString + ':' + secondsString;
  }
  // ===================Cancel confirmaion Alert =====================

  cancelBookingAlert() {
    let alert = this.alertCtrl.create({
      title: 'Cancel Booking',
      message: 'Are you sure,you want to cancel this ride ?',
      cssClass: 'alertStyle',
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.releaseAMT();
          }
        }
      ]
    });
    alert.present();
  }

  IMEI: any;

  ScanQrcode() {
    var that = this;
    this.barcodeScanner.scan({showTorchButton : true}).then(barcodeData => {
      that.IMEI = barcodeData.text;
      that.actualStartTime = new Date();
      that.startTrip();
    }, (err) => {
      this.app_api.stopLoading();
      console.log('Error: ', err);
    });
  }

  tipStarted() {
    var that = this;
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    let toast_1 = this.toastCtrl.create({
      message: 'Device in not inside station, Please goto nearest station',
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    });
    var that = this;
    var tempDetail = localStorage.getItem('tripDetails');
    var tempRouteDetail = JSON.parse(tempDetail);

    var startObj = {
      _id: tempRouteDetail._id,
      status: "LOADING",
      AcctualStartTime: new Date(),
      imei: this.IMEI
    }

    loading.present();
    this.app_api.updatetripDetails(startObj)
      .subscribe(res => {
        var device_Info = JSON.parse(res['_body']).device;
        var ID = JSON.parse(res['_body'])._id;
        var geofenceCheck = JSON.parse(res['_body']).status;
        if (geofenceCheck == "you are not within gofence") {
          loading.dismiss();
          toast_1.present();
        } else {
          var status = setInterval(function () {
            that.app_api.checkTripStatus(ID)
              .subscribe(res => {
                if (res.status == "SUCCESS") {
                  loading.dismiss();
                  clearInterval(status);
                  localStorage.removeItem('lockRide');
                  localStorage.setItem("active_Device", JSON.stringify(device_Info));
                  localStorage.setItem("tripstarted", "SUCCESS")
                  that.navCtrl.push("ConfirmBookingPage", { 'imei': that.IMEI }, this.navOptions);
                }
              }, err => {
                loading.dismiss();
              })
          }, 5000);
        }
      }, err => {
        loading.dismiss();
        localStorage.removeItem('timerTIme');
      })
  }

  toastVar: any;
  toastfunc(msg) {
    this.toastVar = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    });
    this.toastVar.present();
    return this.toastVar;
  }

  startTrip() {
    var devObj = {
      user: this.useridd,
      imei: this.IMEI,
      command: "ON"
    }
    this.app_api.startLoading();
    this.app_api.addcommandQueue(devObj).subscribe(response => {
      if (response.message == "Device is not within geofence") {
        this.app_api.stopLoading();
        this.toastfunc(response.message);
      }
      else if (response.message == "Device Info updated and trip started!!!") {
        // this.app_api.stopLoading();
        this.checkBikeAvailability(response._id);
      }
    }, err => {
      this.app_api.stopLoading();
      this.toastfunc("no device found");
    })
  }

  checkBikeAvailability(cmdqId) {
    var that = this;
    // this.app_api.startLoading();
    var status_1 = setInterval(function () {
      that.app_api.checkTripStatus(cmdqId)
        .subscribe(res => {
          if (res.status == "SUCCESS") {
            // that.app_api.stopLoading();
            clearInterval(status_1);
            that.updateRide();
          }
        }, err => {
          that.app_api.stopLoading();
          that.toastfunc("Internal Server Error , Please Try after sometime !!!");
          clearInterval(status_1);
        })
    }, 5000);
  }

  updateRide() {
    var tempDetail = localStorage.getItem('tripDetails');
    var tempRouteDetail = JSON.parse(tempDetail);
    // this.app_api.startLoading();
    var startObj = {
      _id: tempRouteDetail._id,
      status: "LOADING",
      AcctualStartTime: new Date(),
      imei: this.IMEI,
      user: this.useridd
    }

    this.app_api.updatetripDetails(startObj)
      .subscribe(res => {
        this.app_api.stopLoading();
        var device_Info = JSON.parse(res['_body']).device;
        var resMessage = JSON.parse(res['_body']).message;
        if (resMessage == "Device Info updated and trip started!!!") {
          localStorage.removeItem('lockRide');
          localStorage.setItem("active_Device", JSON.stringify(device_Info));
          localStorage.setItem("tripstarted", "SUCCESS")
          localStorage.setItem("tripStartTime", new Date().toISOString());
          this.navCtrl.push("ConfirmBookingPage", { 'imei': this.IMEI }, this.navOptions);
        } else {
          this.toastfunc(resMessage);
        }
      }, err => {
        this.app_api.stopLoading();
        this.toastfunc("Please Try after sometime !!!");
      })
  }

  releaseAMT() {
    this.app_api.startLoading();
    var releaseObj = {
      CUST_ID: this.useridd,
      app_id: 'zogo'
    }
    this.app_api.releaseAmount(releaseObj)
      .subscribe(res => {
        if (res.STATUS == "TXN_SUCCESS") {
          this.app_api.stopLoading();
          this.canceltripRide();
        } 
      }, err => {
        this.app_api.stopLoading();
      })
  }

  canceltripRide() {
    let toast = this.toastCtrl.create({
      message: 'Trip cancelled',
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    });
    var reason = "plan changed"
    var cancellationTime = new Date();
    var tempDetail = localStorage.getItem('tripDetails');
    var tempRouteDetail = JSON.parse(tempDetail);
    var cancelObj = {
      "_id": tempRouteDetail._id,
      "cancelledAt": cancellationTime,
      "status": "CANCELLED",
      "device": tempRouteDetail.device,
      "cancelledBy": this.useridd,
      "cancellationReason": reason
    };
    this.app_api.startLoading();
    this.app_api.canceltripDetails(cancelObj)
      .subscribe(res => {
        this.app_api.stopLoading();
        toast.present();
        localStorage.removeItem('tripDetails');
        localStorage.removeItem('pickupLocation');
        localStorage.removeItem("dropOffLocation");
        localStorage.removeItem('timerTIme');
        localStorage.removeItem('lockRide');
        this.navCtrl.setRoot("LivePage");
      }, err => {
        this.app_api.stopLoading();
        this.navCtrl.setRoot("TripPage");
      })
  }
}

// savtrip is the first method call when coming from drop location page 
//scanqr method calls after scaning QR trip started is attached with scanQR method (Abinash )


