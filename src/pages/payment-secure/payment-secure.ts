import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { NetworkInterface } from '@ionic-native/network-interface';

@IonicPage()
@Component({
  selector: 'page-payment-secure',
  templateUrl: 'payment-secure.html',
})
export class PaymentSecurePage {
  paytmregNum: string;
  amounttobepaid: any;
  imp_id: any;
  walletBal: any;
  wifiIpAdd: any;
  carrerIpAdd: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apiCall: ApiServiceProvider, private toastCtrl: ToastController, private networkInterface: NetworkInterface) {
    this.amounttobepaid = navParams.get("amtPayble");
    this.imp_id = navParams.get("imp_id");
    this.getNetworkDetail();
  }

  ionViewDidLoad() {
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
   
    this.getPaytmBalance(userId);
  }

  getPaytmBalance(uId) {
    let toast_validation = this.toastCtrl.create({
      duration: 3000,
      position: 'middle'
    });

    this.apiCall.startLoading();
    this.apiCall.paytmbalance(uId)
      .subscribe(res => {
        this.apiCall.stopLoading();
        console.log("response");
        if (res.code == "ETIMEDOUT") {
          toast_validation.setMessage("Etimeout server error");
          toast_validation.present();
        } else {
          this.walletBal = res.response.amount;
        }
      })
  }

  

  proceedSecurely() {
    this.paytmregNum = localStorage.getItem('paytmregNum');
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    let toastCtrl = this.toastCtrl.create({
      message: "Trip amount paid, Thanks for using zogo ride !!",
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    })
    let toastCtrlfailure = this.toastCtrl.create({
      message: "Payment failed , Try again",
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    })

    let toastCtrlServerError = this.toastCtrl.create({
      message: "Internal Server Error, Try Again",
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    })

    let updatePaymentStatus = this.toastCtrl.create({
      message: "Unable to update Payment Status ",
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    })
    var app_ip_address : any;
    if(this.wifiIpAdd){
        app_ip_address = this.wifiIpAdd;
        console.log("wifiaddress=>",app_ip_address);   
    }
    else if(this.carrerIpAdd){
      app_ip_address = this.carrerIpAdd;
      console.log("carrerIpaddress", app_ip_address);
    }else{
      var Networkmsg = "Plese check your Internet connection" ; 
      console.log(Networkmsg);
   
    }
    var paymentWithdraw = {
      CUST_ID: userId,
      app_ip: app_ip_address,
      deviceId: this.paytmregNum,
      "app_id": "zogo",
      "TXN_AMOUNT": this.amounttobepaid
    }
    this.apiCall.startLoading();
    this.apiCall.proceedSecurely(paymentWithdraw)
      .subscribe(res => {
        this.apiCall.stopLoading();
        if (res.Status == "TXN_SUCCESS") {
          this.apiCall.updateTripStatus(userId, this.imp_id, "payment", this.amounttobepaid).subscribe(res => {
            if (res.payment_status == true) {
              toastCtrl.present()
              toastCtrl.onDidDismiss(() => {

                localStorage.removeItem('flag');
                localStorage.removeItem('tripFare');
                localStorage.removeItem('totalrideTime');
                localStorage.removeItem('rideStatus');
                localStorage.removeItem('pickupLocation');
                localStorage.removeItem('paytmBalance');
                localStorage.removeItem('droplocatoingeoId');
                localStorage.removeItem('active_Device');
                localStorage.removeItem("fareTime");
                localStorage.removeItem("tripStartTime");
                localStorage.removeItem('ignitionLock');
                // this.navCtrl.setRoot('LivePage');
                this.navCtrl.push('FeedbackPage', {
                  "imp_id" : this.imp_id
                });
              })

            } else {
              updatePaymentStatus.present();
            }
          })
        } else if (res.STATUS == "TXN_FAILURE") {
          toastCtrlfailure.present();
        } else {
          toastCtrlServerError.present();
        }
      },
      err => {
        console.log("error: ", err)
      });
  }

  getNetworkDetail() {
    var outerThis = this;
    this.networkInterface.getWiFiIPAddress()
    .then(address => {
      outerThis.wifiIpAdd = address.ip;
      console.log("WifiAddress",outerThis.wifiIpAdd);
      console.info(`IP: ${address.ip}, Subnet: ${address.subnet}`)
  })
    .catch(error => console.error(`Unable to get IP: ${error}`));
    this.networkInterface.getCarrierIPAddress()
      .then(address => {
        outerThis.carrerIpAdd = address.ip;
        console.log("careerAddress",outerThis.carrerIpAdd);
        console.info(`IP: ${address.ip}, Subnet: ${address.subnet}`);
      })
      .catch(error => console.error(`Unable to get IP: ${error}`));
  }

  toasterror(errstatus:any) {
    console.log(errstatus);
    let toastCtrl1 = this.toastCtrl.create({
      duration: 3000,
      position: 'bottom',
      cssClass: 'toastStyle'
    })

    toastCtrl1['message'] = errstatus;
   
  return toastCtrl1.present();
  }

  releaseAMT() {
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    this.apiCall.startLoading();
    var releaseObj = {
      CUST_ID: userId,
      app_id: 'zogo'
    }
    this.apiCall.releaseAmount(releaseObj)
      .subscribe(res => {
        this.apiCall.stopLoading();
        if (res.STATUS == "TXN_SUCCESS") {     
          this.proceedSecurely();
        } else {
          this.toasterror("try again");
        }
      }, err => {
        this.apiCall.stopLoading();
        this.toasterror("try again");
      })
  }
}
