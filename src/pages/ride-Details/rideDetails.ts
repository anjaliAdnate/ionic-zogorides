import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ModalController, AlertController, ToastController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-rideDetails',
  templateUrl: 'rideDetails.html',
})
export class RideDetailspage implements OnInit {

  @ViewChild('map') mapElement: ElementRef;

  constructor(public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public elementRef: ElementRef,
    public alertCtrl: AlertController, public geolocation: Geolocation, public app_api: ApiServiceProvider) {

    platform.ready().then(() => { });
  }

  ngOnInit() { }
}

