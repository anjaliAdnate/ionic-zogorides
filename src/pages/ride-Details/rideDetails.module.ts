import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RideDetailspage } from './rideDetails';

@NgModule({
  declarations: [
    RideDetailspage,
  ],
  imports: [
    IonicPageModule.forChild(RideDetailspage),
  ],
})
export class RideDetailsPageModule {}
