import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DropLocationSearchPage } from './drop-location-search';

@NgModule({
  declarations: [
    DropLocationSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(DropLocationSearchPage),
  ],
})
export class DropLocationSearchPageModule {}
