import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, MenuController } from 'ionic-angular';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-drop-location-search',
  templateUrl: 'drop-location-search.html',
})
export class DropLocationSearchPage {

  SearchLocation: any;
  autocompleteItems: any;
  autocomplete: any;
  acService: any;
  placesService: any;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public menu: MenuController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DropLocationSearchPage');
  }

  ionViewDidEnter() {
    this.menu.enable(false);
  }

  ngOnInit() {
    this.acService = new google.maps.places.AutocompleteService();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  updateSearch() {
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let self = this;
    let config = {
      input: this.autocomplete.query,
      componentRestrictions: {}
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      self.autocompleteItems = [];
      predictions.forEach(function (prediction) {
        self.autocompleteItems.push(prediction);
      });
    });
  }

  chooseItem(item) {
    let dropOff_location = item.description;
    let dropObj = {
      'id': item.place_id,
      'name': item.description
    }
    localStorage.setItem("dropOffLocation", JSON.stringify(dropObj));
    this.navCtrl.pop()
  }

  searchInput(ev) {
    console.log(this.SearchLocation)
  }

  onCancel($event) {
    this.viewCtrl.dismiss()
  }
}
