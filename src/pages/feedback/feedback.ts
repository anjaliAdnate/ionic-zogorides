import { Component } from '@angular/core';
import { IonicPage, NavController, Events, ToastController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class FeedbackPage {
  additionalText: string;
  convinient: boolean = false;
  drive: boolean = false;
  pricing: boolean = false;
  booking: boolean = false;
  starRatinginput: any = 5;
  islogin: any;
  triID: any;
  imp_id: any;
  TripDetailsdata: any;

  constructor(public navParams: NavParams, public navCtrl: NavController, public events: Events, public apiCall: ApiServiceProvider, public toastCtrl: ToastController) {
    this.imp_id = navParams.get("imp_id");
    if (localStorage.getItem("tripDetails")) {
      this.triID = JSON.parse(localStorage.getItem("tripDetails"))._id;
    }

    events.subscribe('star-rating:changed', (starRating) => { this.starRatinginput = starRating; console.log("start Rating output: " + starRating) });
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  ionViewDidLoad() {
    this.rideStatus();
    console.log('ionViewDidLoad FeedbackPage');
  }
  comment() { }

  done() {
    console.log("==============Input feedback===========")
    console.log("convinient: ", this.convinient)
    console.log("drive: ", this.drive)
    console.log("pricing: ", this.pricing)
    console.log("booking: ", this.booking)
    console.log("starRatinginput: ", this.starRatinginput)
    console.log("additionalText: ", this.additionalText)
    // this.navCtrl.setRoot("LivePage");
    if(this.triID == undefined) {
      this.triID = this.TripDetailsdata._id;
    }
    var payload = {
      "tripID": this.triID,
      "feedbackStars": this.starRatinginput,
      "convinient": this.convinient,
      "eace_of_drive": this.drive,
      "eache_of_booking": this.booking,
      "pricing": this.pricing,
      "additionalText": this.additionalText
    }
    this.apiCall.feedbackData(payload)
      .subscribe(data => {
        console.log("feedback service response: ", data);
        if (data) {
          let toast = this.toastCtrl.create({
            message: "Thank you for your valuable feedback!",
            duration: 2500,
            position: "bottom"
          });
          toast.present();
          this.statusApi();

        }
      })
  }

  statusApi() {
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    this.apiCall.startLoading();
    this.apiCall.updateTripStatus(userId, this.imp_id, "feedback", null).subscribe(res => {
      this.apiCall.stopLoading();
      if (res.feedbackStatus == true) {
        localStorage.removeItem("tripDetails");
          this.navCtrl.setRoot('LivePage');

      } else {
        
      }
    },
    err => {
      this.apiCall.stopLoading();
    })

  }

  skip() {
    localStorage.removeItem("tripDetails");
    this.navCtrl.setRoot("LivePage");
  }

  rideStatus() {
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    this.apiCall.checkRideStatus(userId)
      .subscribe(res => {
        this.TripDetailsdata = res;
      }, err => {
        console.log("Internal Server Error, Please try again !!")
      })
  }
}
