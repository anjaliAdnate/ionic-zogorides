import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportMisusePage } from './report-misuse';

@NgModule({
  declarations: [
    ReportMisusePage,
  ],
  imports: [
    IonicPageModule.forChild(ReportMisusePage),
  ],
})
export class ReportMisusePageModule {}
