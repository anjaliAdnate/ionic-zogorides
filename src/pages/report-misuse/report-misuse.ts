import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Geolocation } from '@ionic-native/geolocation';
declare var google;

@IonicPage()
@Component({
  selector: 'page-report-misuse',
  templateUrl: 'report-misuse.html',
})
export class ReportMisusePage {
  ReportMisuseForm: FormGroup;
  scannedCode: string;
  address: any;
  address_show: any;
  locationShow: any;

  constructor(public navCtrl: NavController, public barcodeScanner: BarcodeScanner, public navParams: NavParams, public formBuilder: FormBuilder, public geolocation: Geolocation) {
    this.ReportMisuseForm = formBuilder.group({
      name: ['', Validators.required],
      bikenumber: ['', Validators.required],
      note: ['', Validators.required],
      location: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportMisusePage');
  }

  ScanQrcode() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
    }, (err) => {
      console.log('Error: ', err);
    });
  }

  BikeLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      var that = this;
      var initLat = resp.coords.latitude;
      var initLng = resp.coords.longitude;

      var geocoder = new google.maps.Geocoder();
      var latlng = new google.maps.LatLng(initLat, initLng);

      var request = {
        latLng: latlng
      };

      geocoder.geocode(request, function (data, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (data[0] != null) {
            that.address_show = data[0].formatted_address;
            that.locationShow = that.address_show;
          } else {
            that.address_show = data[0].formatted_address;
          }
        }
      })
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }
}
