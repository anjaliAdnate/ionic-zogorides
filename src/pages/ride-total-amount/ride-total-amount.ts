import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-ride-total-amount',
  templateUrl: 'ride-total-amount.html',
})
export class RideTotalAmountPage {
  rideTime: any;
  fareTime: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public apiCall: ApiServiceProvider, private toastCtrl: ToastController) {
    if (navParams.get("fareTime")) {
      this.fareTime = navParams.get("fareTime");
      localStorage.setItem("fareTime", this.fareTime);
    } else {
      if (this.fareTime == undefined) {
        this.fareTime = JSON.parse(localStorage.getItem("fareTime"));
      }
    }
  }

  ionViewDidLoad() {
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    var minu = new Date(this.fareTime).getMinutes();
    this.rideTime = minu;
    this.rideFare(userId);
  }

  makePayment() {
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;

    this.apiCall.paytmValidation(userId)
      .subscribe(res => {
        if ((res.Status == "Token Not Found") || (res.message == "Invalid Token") || (res.status == "FAILURE")) {
          this.navCtrl.push("PaytmwalletloginPage");
        } else if ((res.mobile) && (res.id)) {
          this.getPaytmBalance(userId);
          var paytmNumber = res.mobile;
          localStorage.setItem('paytmregNum', paytmNumber);
        }
      })
  }

  getPaytmBalance(uId) {
    this.apiCall.paytmbalance(uId)
      .subscribe(res => {
        if (res.code == "ETIMEDOUT") {
          this.toasterror(res.code)
        } else {
          localStorage.setItem('paytmBalance', JSON.stringify(res));
          this.navCtrl.push("WalletPage");
        }
      })
  }

  toasterror(errstatus) {
    let toastCtrl = this.toastCtrl.create({
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    })
    if (errstatus == "ETIMEDOUT") {
      toastCtrl['message'] = "Internal Server error";
      toastCtrl.present()
    }
  }
  amtToBePaid: any;
  rideFare(userId) {
    this.apiCall.rideFare(userId)
      .subscribe(res => {
        this.amtToBePaid = res.billAmount;
        localStorage.setItem("tripFare", JSON.stringify(this.amtToBePaid));
        localStorage.removeItem("remainingTime");
      })
  }

}
