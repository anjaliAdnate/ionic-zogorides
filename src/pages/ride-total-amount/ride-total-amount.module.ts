import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RideTotalAmountPage } from './ride-total-amount';

@NgModule({
  declarations: [
    RideTotalAmountPage,
  ],
  imports: [
    IonicPageModule.forChild(RideTotalAmountPage),
  ],
})
export class RideTotalAmountPageModule {}
