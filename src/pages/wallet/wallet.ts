import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage {
  ride_id: any;
  enteredAmt: any;
  paytmregNum: any;
  paytmAmount: number;
  showbutton: boolean = false;
  hidebutton: any;
  orderID: string = "1fgddfasf45445fdgdfg3dfsfsdew";
  amounttobepaid: any;
  hidePaybutton: boolean = true;
  hideAddbutton: boolean;
  blockedAmt: any;
  showCoupenDiv: boolean = false;
  promocodeentered: any;
  imp_id: any;
  buttonStr:any= "Pay"
  disablehideMoney: boolean = true;
  // promocodePassCode: boolean;
   promocodePassCode: boolean;
  paytmlowbalance: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public menu: MenuController, private iab: InAppBrowser, public apiCall: ApiServiceProvider, public toastCtrl: ToastController) {
    this.paytmlowbalance = this.navParams.get("lowBalance");
  
    this.menu.enable(true);
  }

  toast = this.toastCtrl.create({
    duration: 5000,
    position: 'middle'
  });

  ionViewDidLoad() {
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    this.getPaytmBalance(userId);
    this.amounttobepaid = localStorage.getItem('tripFare') ? JSON.parse(localStorage.getItem('tripFare')) : 0;
    this.paytmregNum = localStorage.getItem('paytmregNum');

  }

  amount(cash) {
    this.disablehideMoney = false;
    this.enteredAmt = cash;
  }

  checkAmount(entrdamt){
  
    if(entrdamt){
      this.disablehideMoney = false;
    }else{
      this.disablehideMoney = true;
    }
  }

  changeNum() {
    this.navCtrl.push("PaytmwalletloginPage", {
      "chnagenumParam": "chnagenumParam"
    });
  }

  addMoney() {
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    this.apiCall.startLoading();
    var addAmountObj =
    {
      "CUST_ID": userId,
      "ORDER_ID": this.orderID,
      "TXN_AMOUNT": this.enteredAmt,
      "app_id": "zogo"
    }
    this.apiCall.addMoneyPaytm(addAmountObj)
      .subscribe(res => {
        this.apiCall.stopLoading();
        console.log(res);
        var resTemplate = res['_body'];
        this.InappBrowserWindow(resTemplate);
      }, err => {
        this.apiCall.stopLoading();
        this.toast['message'] = "Server error on adding amount";
        this.toast.present()
      })
  }

  InappBrowserWindow(ress) {
    var outerThis = this;
    var pageContentUrl = 'data:text/html;base64,' + btoa(ress);
    let browser = this.iab.create(pageContentUrl, '_self');
    browser.on('loadstop').subscribe(event => {
      console.log(event);
      if (event.url == 'https://www.oneqlik.in/paytm/callback') {
        console.log("Inside paytm callback");
        browser.close();
        outerThis.chacktransectionStatus();
      }
    });
  }

  chacktransectionStatus() {
    this.apiCall.startLoading();
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    this.apiCall.paytmbalance(userId)
      .subscribe(res => {
        this.apiCall.stopLoading();
        var amountAfterCashAdded = res.response.amount;
        var statusCode = res.statusCode;
        this.paytmAmount += amountAfterCashAdded;
        if (amountAfterCashAdded > this.paytmAmount) {
          this.toast.setMessage("Amount added successfuly");
          if (localStorage.getItem("navigationFrom") == "NotRegisteredWithPaytm") {
            this.navCtrl.setRoot("DropLocationPage");
          }
          this.toast.present();
        } else if (amountAfterCashAdded == this.paytmAmount) {
          this.toast.setMessage("Transection failure ");
          this.toast.present();
        } else {
          this.toast.setMessage("Transection failure");
          this.toast.present();
        }
      }, err => {
        this.toast.setMessage("Server Error while checking balance");
        this.toast.present();
        this.apiCall.stopLoading();
      })
  }

  promocodeFunc() {
    this.showCoupenDiv = true;
  }

  applyCode() {
    if (this.promocodeentered != undefined) {
      var useDetails = JSON.parse(localStorage.getItem('details'));
      var userId = useDetails._id;
      this.apiCall.startLoading();
      this.apiCall.promocodeService(userId, this.amounttobepaid, this.orderID, this.promocodeentered)
        .subscribe(data => {
          this.apiCall.stopLoading();
          let that = this;
          if (data.status == 'available') {
            that.imp_id = data._id;
            if (data.isPercent) {
              that.amounttobepaid = that.amounttobepaid - ((data.amount / 100) * that.amounttobepaid);
            } else {
              if (that.amounttobepaid >= data.amount) {
                that.amounttobepaid = that.amounttobepaid - data.amount;
              } else {
                if (that.amounttobepaid < data.amount) {
                  that.amounttobepaid = 0;
                }
              }
            }
            that.showCoupenDiv = false;
            let toast = this.toastCtrl.create({
              message: "Code applied successfully.",
              duration: 2000,
              position: 'bottom'
            });
            toast.present();
         
            console.log("Amount to be paid",this.amounttobepaid)
            if(this.amounttobepaid == 0){
              this.buttonStr = "Proceed";
              this.promocodePassCode = false;
            }

            if(this.amounttobepaid > this.paytmAmount){
              this.disablehideMoney = false;
            }else{
              this.disablehideMoney = true;
              // this.promocodePassCode = false;
            }
          
          } else {
            if (data.status == 'No code found') {
              let toast = this.toastCtrl.create({
                message: "Invalid code",
                duration: 2000,
                position: 'bottom'
              });
              toast.present();
              toast.onDidDismiss(() => {
                that.showCoupenDiv = false;
              })
            }
          }
        },
          err => {
            this.apiCall.stopLoading();
            console.log("error found=> ", err)
          })
    } else {
      let toast = this.toastCtrl.create({
        message: 'Please enter valid coupen code',
        duration: 2500,
        position: 'bottom'
      });
      toast.present();
    }
  }
  // ==================================================================================

  // method added after removing preauth==================================================
  releaseAMT() {
    let that = this;
    if (that.amounttobepaid == 0) {
      this.relAMT()
    } else {
      that.navCtrl.push('PaymentSecurePage', {
        'amtPayble': that.amounttobepaid,
        "imp_id": that.imp_id
      });
    }
  }

  relAMT() {
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    var releaseObj = {
      CUST_ID: userId,
      app_id: 'zogo'
    }
    this.apiCall.startLoading();
    this.apiCall.releaseAmount(releaseObj)
      .subscribe(res => {
        this.apiCall.stopLoading();
        if (res.STATUS == "TXN_SUCCESS") {
          this.proceedSecurely();
        } else {
          this.toasterror1("try aghain");
        }
      }, err => {
        this.apiCall.stopLoading();
        this.toasterror1("try aghain");
      })
  }

  toasterror1(errstatus) {
    let toastCtrl1 = this.toastCtrl.create({
      duration: 3000,
      position: 'bottom',
      cssClass: 'toastStyle'
    })

    toastCtrl1['message'] = errstatus;
    return toastCtrl1.present();
  }

  proceedSecurely() {
    this.paytmregNum = localStorage.getItem('paytmregNum');
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    let toastCtrl = this.toastCtrl.create({
      message: "Trip amount paid, Thanks for using zogo ride !!",
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    })
    let toastCtrlfailure = this.toastCtrl.create({
      message: "Payment failed , Try again",
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    })

    let updatePaymentStatus = this.toastCtrl.create({
      message: "Unable to update Payment Status ",
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    })

    this.apiCall.startLoading();
    this.apiCall.updateTripStatus(userId, this.imp_id, "payment", "0").subscribe(res => {
      this.apiCall.stopLoading();
      if (res.payment_status == true) {
        toastCtrl.present()
        toastCtrl.onDidDismiss(() => {
          localStorage.removeItem('flag');
          this.navCtrl.setRoot('FeedbackPage');
        })
      } else {
        updatePaymentStatus.present();
      }
    },
      err => {
        this.apiCall.stopLoading();
        console.log("updateTripStatus: ", err)
      })
  }

  // ===========================================================================
  toasterror(errstatus) {
    let toastCtrl1 = this.toastCtrl.create({
      duration: 3000,
      position: 'bottom',
      cssClass: 'toastStyle'
    })
    toastCtrl1['message'] = errstatus;
    return toastCtrl1.present();
  }

  tempfunc = function () {
    var responseMsg = "Inside function";
    return responseMsg;
  }

  goBack() {
    this.navCtrl.pop();
  }


  getPaytmBalance(uId) {
    let toast_validation = this.toastCtrl.create({
      duration: 3000,
      position: 'middle'
    });
    this.apiCall.startLoading();
    this.apiCall.paytmbalance(uId)
      .subscribe(res => {
        this.apiCall.stopLoading();
        if(this.paytmlowbalance=="addAmt"){
          console.log("localstorage",this.paytmlowbalance);
          this.hidePaybutton = true;     
        }else if (res.code == "ETIMEDOUT") {
          toast_validation.setMessage("Etimeout server error");
          toast_validation.present();
        } else {
          console.log("Inside");
          this.paytmAmount = res.response.amount;
          if(Number(this.amounttobepaid) < Number(this.paytmAmount)){        
            this.promocodePassCode = false;
          }else{
            this.promocodePassCode =true;
          }
          if ((Number(this.paytmAmount) != 0) && (Number(this.amounttobepaid) < Number(this.paytmAmount))) {
            this.hidePaybutton = false;
          }
        }
      })
  }

  getBlockedAmt() {
    this.apiCall.startLoading();
    this.apiCall.getblockamt()
      .subscribe(res => {
        this.apiCall.stopLoading();
        this.blockedAmt = res['Block Amount'];
        if (this.paytmAmount < this.blockedAmt) {
          this.hideAddbutton = true;
        }
      }, err => {
        this.apiCall.stopLoading();
      })
  }
}


// http://localhost:3000/paytm/preAuth
// {
// "CUST_ID":"5bc86c5285303603e1046b27",
// "ORDER_ID":"465213434335345332434366523123647",
// "TXN_AMOUNT":"23.00",
// "DURATIONHRS":"70"
// }
