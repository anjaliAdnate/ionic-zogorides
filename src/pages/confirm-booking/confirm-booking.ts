import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ModalController, AlertController, ToastController, ActionSheetController, LoadingController } from 'ionic-angular';
import { LatLng } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as io from 'socket.io-client';
import * as moment from 'moment';
import { SocialSharing } from '@ionic-native/social-sharing';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-confirm-booking',
  templateUrl: 'confirm-booking.html',
})
export class ConfirmBookingPage {
  map: any;
  mapOptions: { backgroundColor: string; };
  latLng: LatLng;
  useridd: any;
  comptripObj: any;
  _io: any;
  drawerHidden = true;
  drawerHidden1 = false;
  shouldBounce = true;
  dockedHeight = 150;
  bounceThreshold = 500;
  distanceTop = 56;
  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};
  data: any = {};
  socketSwitch: any = {};
  socketChnl: any = [];
  socketData: any = {};
  selectedFlag: any = { _id: '', flag: false };
  allData: any = {};
  userdetails: any;
  showBtn: boolean;
  SelectVehicle: string;
  selectedVehicle: any;
  titleText: any;
  portstemp: any;
  showActionSheet: boolean;
  gpsTracking: any;
  power: any;
  currentFuel: any;
  last_ACC: any;
  today_running: string;
  today_stopped: string;
  timeAtLastStop: string;
  distFromLastStop: any;
  lastStoppedAt: string;
  fuel: any;
  total_odo: any;
  todays_odo: any;
  vehicle_speed: any;
  liveVehicleName: any;
  onClickShow: boolean;
  address: any;
  isEnabled: boolean = false;
  liveDataShare: any;
  showShareBtn: boolean = false;
  resToken: any;
  mapData: any[];
  menuActive: boolean;
  motionActivity: string;
  last_ping_on: any;
  timeInSeconds: any;
  time: any;
  runTimer: boolean;
  hasStarted: boolean;
  hasFinished: boolean;
  remainingTime: any;
  total_min: number;
  devInfo: any;
  devIMEI: any;
  tempRouteDetail: any;
  showDistance: any;
  tripDistanceCovered: any = 0;
  initialDistanceCovered: any;
  totalDistanceCovered: any;
  totalTripdistance: any;
  parked: boolean = true;
  resume: boolean = false;
  tripStartTime: any;
  remainingTimeinSec: any;
  timeoutId: any;
  POPDFLink: any;
  tripStart: any;
  loggedinuser: any;

  constructor(public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public elementRef: ElementRef,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public geolocation: Geolocation,
    public app_api: ApiServiceProvider,
    private toastCtrl: ToastController,
    private socialSharing: SocialSharing,
    public loadingCtrl: LoadingController) {

    this.loggedinuser = JSON.parse(localStorage.getItem("details"));
  }
  rideStatus: any = {
    "parked": this.parked,
    "resume": this.resume
  }

  ionViewDidLoad() {
    // if (this.loggedinuser) {
    //   this.app_api.getVehicleListCall(this.loggedinuser._id, this.loggedinuser.email)
    //     .subscribe((data) => {
    //       console.log("JSON stringify: ", JSON.stringify(data));
    //     },
    //       err => {
    //         console.log("json stringify err: ", err)
    //       });
    // }
    this.tripStartTime = localStorage.getItem("tripStartTime");
    this.checkRideStat();
    this.showActionSheet = true;
    // this.initTimer();

    this.initializeMap();
  }

  checkRideStat() {
    var uID = JSON.parse(localStorage.getItem('details'));
    var u_id = uID._id;
    this.app_api.checkRideStatus(u_id)
      .subscribe(data => {
        console.log("atadata=>", data);
        this.tripStart = data.startSiteEnterAt;
        if (typeof (this.tripStart == Number)) {
          this.startTimer();
          this.initTimer()
        }

        if (data.device.vehicleDoc) {
          var strpdf = data.device.vehicleDoc.split("public");
          var updatePdf = strpdf[1];
          // console.log("pdf link => ", updatePdf)
          this.POPDFLink = "https://www.oneqlik.in/" + updatePdf;
          console.log("pdf link => ", this.POPDFLink)
        }
        if (data.status == 'LOADING') {
          if (data.vehicle_status == 'PAUSED' || data.vehicle_status == 'OFF') {
            this.resume = true;
            this.parked = false;
          } else {
            if (data.vehicle_status == 'RESTARTED' || data.vehicle_status == 'ON') {
              this.parked = true;
              this.resume = false;
            }
          }
        }
      },
        err => {
          console.log("check status error=> ", err)
        })
  }

  // parseMillisecondsIntoReadableTime(milliseconds) {
  //   //Get hours from milliseconds
  //   var hours = milliseconds / (1000 * 60 * 60);
  //   var absoluteHours = Math.floor(hours);
  //   var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

  //   //Get remainder from hours and convert to minutes
  //   var minutes = (hours - absoluteHours) * 60;
  //   var absoluteMinutes = Math.floor(minutes);
  //   var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

  //   //Get remainder from minutes and convert to seconds
  //   var seconds = (minutes - absoluteMinutes) * 60;
  //   var absoluteSeconds = Math.floor(seconds);
  //   var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
  //   return h + ':' + m + ':' + s;
  // }

  count = 0;
  vehSpeed: any;

  info(mark, cb) {
    mark.addListener('click', cb);
  }

  mapHideTraffic: boolean = false;

  confirmBookingAlert() {
    let alert_complete = this.alertCtrl.create({
      title: 'Confirm Booking',
      message: 'Do you want to End this ride?',
      cssClass: 'alertStyle1',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            console.log('Ok clicked');

          }
        }
      ]
    });
    alert_complete.present();
  }

  // ====================================Ride End NewMethod Starts ===================================================
  toastMessages(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    });

    return toast.present();
  }

  endRideNewMethod() {
    let alert = this.alertCtrl.create({
      title: 'Complete Trip',
      message: 'Are you sure, you want to End this ride ?',
      cssClass: 'alertStyle1',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.checkTripCommand();
          }
        }
      ]
    });
    alert.present();


  }

  checkTripCommand() {
    var devObj = {
      user: this.useridd,
      imei: this.devId.Device_ID,
      command: "OFF"
    }
    this.app_api.startLoading();
    this.app_api.addcommandQueue(devObj).subscribe(response => {
      if (response.message == "Device Info updated and trip started!!!") {
        this.app_api.stopLoading();
        this.checkBikeStatus(response._id);
      }
      if (response.message == "Device is not within geofence") {
        this.app_api.stopLoading();
        this.toastMessages("you are not within gofence");
      }
    }, err => {
      this.app_api.stopLoading();
      this.toastMessages("no device found");
    })
  }

  checkBikeStatus(cmdqId) {
    var that = this;
    this.app_api.startLoading();
    var status = setInterval(function () {
      that.app_api.checkTripStatus(cmdqId)
        .subscribe(res => {
          if (res.status == "SUCCESS") {
            that.app_api.stopLoading();
            clearInterval(status);
            that.closeRide(res.trip);
          }
        }, err => {
          that.app_api.stopLoading();
          that.toastMessages("Internal Server Error , Please Try after sometime !!!");
          clearInterval(status);
        })
    }, 5000);
  }

  closeRide(tripId) {
    // var tempDetail = localStorage.getItem('tripDetails');
    // var tempRouteDetail = JSON.parse(tempDetail);
    var startObj = {
      _id: tripId,
      status: "COMPLETED",
      AcctualEndTime: new Date(),
      imei: this.devId.Device_ID,
      user: this.useridd
    }
    this.app_api.startLoading();
    this.app_api.completetripDetails(startObj)
      .subscribe(res => {
        this.app_api.stopLoading();
        var ID = JSON.parse(res['_body'])._id;
        this.resume = false;
        this.parked = true;
        this.rideStatus = {
          "parked": this.parked,
          "resume": this.resume
        }
        localStorage.setItem('rideStatus', JSON.stringify(this.rideStatus));
        localStorage.setItem('totalrideTime', JSON.stringify(this.total_min));
        // localStorage.removeItem('tripDetails');
        localStorage.removeItem('pickupLocation');
        localStorage.removeItem('tripstarted');
        localStorage.removeItem("dropOffLocation");
        localStorage.removeItem('timerTIme');
        localStorage.removeItem('ignitionLock');
        // localStorage.removeItem("remainingTime");
        clearTimeout(this.timeoutId);
        this.navCtrl.setRoot("RideTotalAmountPage", {
          "fareTime": this.remainingTime
        });
      }, err => {
        this.app_api.stopLoading();
        this.toastMessages(err);
      })
  }

  // ====================================Ride End NewMethod Starts ===================================================
  displayTime: any;
  initTimer() {
    var tempTime = new Date(this.tripStart);
    var curttime = new Date();
    var diffTime = curttime.getTime() - tempTime.getTime();
    var timeseconds = ((diffTime) / 1000).toFixed(0);
    this.timeInSeconds = timeseconds;
    this.runTimer = false;
    this.hasStarted = false;
    this.hasFinished = false;
    this.remainingTime = this.timeInSeconds;
    this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
  }

  timerTick() {
    this.timeoutId = setTimeout(() => {
      // if (!this.runTimer) { return; }
      this.remainingTime++;
      this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
      if (this.remainingTime > 0) {
        // localStorage.setItem("remainingTime", this.remainingTime);
        this.timerTick();
      }
      else {
        this.hasFinished = true;
      }
    }, 1000);
  }

  getSecondsAsDigitalClock(inputSeconds: number) {

    var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);
    var hoursString = '';
    var minutesString = '';
    var secondsString = '';
    hoursString = (hours < 10) ? "0" + hours : hours.toString();
    minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
    secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
    return hoursString + ':' + minutesString + ':' + secondsString;
  }

  shareLive() {
    var data = {
      id: this.liveDataShare._id,
      imei: this.liveDataShare.Device_ID,
      sh: this.useridd,
      ttl: 60   // set to 1 hour by default
    };
  }

  liveShare() {
    var uID = JSON.parse(localStorage.getItem('details'));
    let that = this;
    var link = "https://www.oneqlik.in/share/liveShare?t=" + that.resToken;
    that.socialSharing.share(uID.fn + " " + uID.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
  }

  startTimer() {
    this.runTimer = true;
    this.hasStarted = true;
    this.timerTick();
  }

  // =============================================Map function===============================================

  initializeMap() {
    let that = this;
    that.data.marker = [];
    that._io = io.connect('https://www.oneqlik.in/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    that._io.on('connect', function (data) { console.log('Ignition IO Connected', data); })

    that.data.status = {};
    var uID = localStorage.getItem('details');
    that.useridd = JSON.parse(uID)._id;
    that.app_api.checkRideStatus(that.useridd).subscribe(resp => {

      that.devId = resp.device;
      // console.log("JSON stringify: ", JSON.stringify(that.devId));
      console.log("JSON parse: ", that.devId);
      localStorage.setItem("ignitionLock", that.devId.ignitionLock);

      that.data.map = that.newMap(that.devId);
      that.totalDistanceCovered = that.devId.total_odo;
      let key = that.devId._id;

      if (that.devId && that.devId.last_location && that.devId.last_location.lat && that.devId.last_location.long) {
        let ddd: any = { lat: that.devId.last_location.lat, lng: that.devId.last_location.long };

        let icon = "";
        var urlicon;
        if (that.platform.is('ios')) {
          urlicon = 'www/assets/imgs/vehicles/scooter.png';
        } else {
          urlicon = './assets/imgs/vehicles/scooter.png';
        }
        if (moment().diff(moment(that.devId.last_ping_on), 'minutes') < 60)
          icon = urlicon;
        else
          icon = urlicon;
        that.data.marker[that.devId._id] = new google.maps.Marker({
          position: ddd,
          map: that.data.map,
          title: that.devId.Device_Name,
          icon: icon
        });
        that.data.marker[that.devId._id].dicon = icon;
      }

      that._io.emit('acc', that.devId.Device_ID);
      that._io.on(that.devId.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {

        if (d4) {
          if (d4.total_odo != null) {
            var distancecovered: Number = (d4.total_odo - that.totalDistanceCovered);
            that.totalTripdistance = distancecovered.toFixed(2);
          }
        }
        var icon;

        if (that.platform.is('ios')) {
          icon = 'www/assets/imgs/vehicles/scooter.png';
        } else {
          icon = './assets/imgs/vehicles/scooter.png';
        }
        let dd1: any = { lat: d4.last_location.lat, lng: d4.last_location.long };
        let dd2: any = { lat: d4.sec_last_location.lat, lng: d4.sec_last_location.long };
        if (that.data.marker[d4._id] == undefined) {

          that.data.marker[d4._id] = new google.maps.Marker({
            position: dd1,
            map: that.data.map,
            title: d4.Device_Name,
            icon: icon
          });
          that.data.marker[d4._id].dicon = icon;
        } else {

          if (moment().diff(moment(d4.last_ping_on), 'minutes') < 60)
            that.data.marker[d4._id].icon = icon;
          else
            that.data.marker[d4._id].icon = icon;

          that.data.marker[d4._id].setIcon(that.data.marker[d4._id].icon);
          if (that.data.marker[d4._id]._moveMarker)
            clearTimeout(that.data.marker[d4._id]._moveMarker);
          if (that.data.marker[d4._id]._goToPoint)
            clearTimeout(that.data.marker[d4._id]._goToPoint);
          that.data.marker[d4._id].setPosition(dd2);
          that.liveTrack(that.data.map, that.data.marker[key], [dd1], 50, 10);
        }
      });
    })

    // -----------------------------Map Code-----------------------------------
  }

  liveTrack(map, mark, coords, speed, delay) {
    var target = 0;
    function _goToPoint() {
      var lat = mark.position.lat();
      var lng = mark.position.lng();
      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target])
        var dest = new google.maps.LatLng(coords[target].lat, coords[target].lng);

      else
        return;
      var distance = google.maps.geometry.spherical.computeDistanceBetween(dest, mark.position); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function _moveMarker() {
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        if (i < distance) {
          var head = google.maps.geometry.spherical.computeHeading(mark.getPosition(), new google.maps.LatLng(lat, lng))
          if ((head != 0) || (head == NaN)) {
            var dltemp: any = document.querySelector('img[src="' + mark.dicon + '"]');
            if (dltemp != null && dltemp != undefined) {
              dltemp.style.padding = "5%";
              dltemp.style.transform = 'rotate(' + head + 'deg)';
            }
          }
          var cnt = new google.maps.LatLng(lat, lng);
          map.setCenter(cnt);
          mark.setPosition(cnt)
          mark._moveMarker = setTimeout(_moveMarker, delay);
        }
        else {
          var head = google.maps.geometry.spherical.computeHeading(mark.getPosition(), new google.maps.LatLng(lat, lng));
          if ((head != 0) || (head == NaN)) {
            var dltemp: any = document.querySelector('img[src="' + mark.dicon + '"]');
            if (dltemp != null && dltemp != undefined)
              dltemp.style.transform = 'rotate(' + head + 'deg)';
          }
          map.setCenter(dest);
          target++;
          mark._goToPoint = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }

  newMap(schoolData) {
    let map = new google.maps.Map(document.getElementById('maptrack'), {
      mapTypeControl: false,
      center: { lat: schoolData.last_location.lat, lng: schoolData.last_location.long },
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoom: 18
    });
    return map;
  }

  // =============================================Map function===============================================

  DevicePark(flag) {
    var that = this;
    var parkingPayload = {
      user: this.useridd,
      imei: this.devId.Device_ID,
    };

    if (flag === 0) {
      this.app_api.startLoading();
      parkingPayload['command'] = "OFF";
      this.app_api.pauseRide(parkingPayload).subscribe(res => {
        var status_pause = setInterval(function () {
          that.app_api.checkTripStatus(res._id)
            .subscribe(res => {
              if (res.status == "SUCCESS") {
                that.app_api.stopLoading();
                clearInterval(status_pause);
                that.parked = false;
                that.resume = true;
                that.rideStatus = {
                  "parked": that.parked,
                  "resume": that.resume
                }
                localStorage.setItem('rideStatus', JSON.stringify(that.rideStatus));
                that.toastMessages("Ride Paused");
              }
            }, err => {
              that.app_api.stopLoading();
              that.toastMessages("Internal Server Error , Please Try after sometime !!!");
              clearInterval(status_pause);
            })
        }, 5000);
      }, err => {
        this.app_api.stopLoading();
        console.log(err);
      })

    }
    if (flag === 1) {
      this.app_api.startLoading();
      parkingPayload['command'] = "ON"
      this.app_api.resumeRide(parkingPayload).subscribe(res => {
        var status_resume = setInterval(function () {
          that.app_api.checkTripStatus(res._id)
            .subscribe(res => {
              if (res.status == "SUCCESS") {
                that.app_api.stopLoading();
                clearInterval(status_resume);
                that.resume = false;
                that.parked = true;
                that.rideStatus = {
                  "parked": that.parked,
                  "resume": that.resume
                }
                localStorage.setItem('rideStatus', JSON.stringify(that.rideStatus));
                that.toastMessages("Ride Resume");
              }
            }, err => {
              that.app_api.stopLoading();
              that.toastMessages("Internal Server Error , Please Try after sometime !!!");
              clearInterval(status_resume);
            })
        }, 5000);
      }, err => {
        this.app_api.stopLoading();
        console.log(err);
      })
    }
  }

  devId =
    {
      "_id": "5bb06d7fa6512f0dafe3dc53",
      "Device_Name": "CG04JB7896",
      "Device_ID": "868003030734904",
      "Email_ID": "amit.shukla@gmail.com",
      "token": "\\tQ'!Bm1'Nq~|c-@",
      "type_of_device": "Tracker",
      "sim_number": "8390252870",
      "user": "5ba4a4b79f07b458d6a70097",
      "SpeedAlert": false,
      "device_model": "5b3e588f17811f45a47a181e",
      "expiration_date": "2019-09-29T18:30:00.000Z",
      "vehicleType": "5b458e8726988510e7ee6905",
      "created_by": "59cbbdbe508f164aa2fef3d8",
      "sim_provider": "VODAFONE",
      "overspeeding": false,
      "sosTo": [],
      "created_on": "2018-09-30T06:30:23.107Z",
      "engine_status": true,
      "currentTripStatus": "UNASSIGNED",
      "maxStoppageAlerted": false,
      "maxStoppageAlert": false,
      "status": "RUNNING",
      "maxSpeed": 70,
      "tripCount": 287,
      "total_odo": 308354.210843497,
      "today_odo": 499.518552772235,
      "last_loc": {
        "coordinates": [
          85.42722,
          23.4407877777778
        ],
        "type": "Point"
      },
      "iconType": "truck",
      "maintenance": false,
      "HardWare": "8390252870",
      "__v": 0,
      "activated": "0",
      "alarm": "000",
      "batteryStatus": "6",
      "gpsTracking": "0",
      "gsmSignal": "0",
      "ignitionLock": "0",
      "last_ACC": "1",
      "last_ACC_on": "2019-01-05T18:07:01.923Z",
      "power": "1",
      "last_speed": "9",
      "last_ping_on": "2019-01-05T18:07:34.946Z",
      "last_location": {
        "lat": 23.4407877777778,
        "long": 85.42722
      },
      "sec_last_location": {
        "lat": 23.4405777777778,
        "long": 85.4269561111111
      },
      "sec_last_speed": "5",
      "status_updated_at": "2019-01-05T18:07:01.925Z",
      "lastStoppedAt": "2019-01-04T18:30:00.000Z",
      "distFromLastStop": 499.518552772235,
      "timezone": "Asia/Kolkata",
      "currentFuel": null,
      "currentFuelVoltage": null,
      "satellites": "7",
      "currentPOI": null,
      "currentPOIName": null,
      "currentRoute": null,
      "lastIgnChangeAt": "2019-01-05T18:00:41.022Z",
      "timeAtLastStop": 20286087,
      "today_running": 37394258,
      "today_stopped": 49809631,
      "ac": null,
      "heading": "45",
      "vehicle": "5bc6eca87dccd11964658a49",
      "speedChart": {
        "0-20": 18959.0,
        "20-40": 22648.0,
        "40-60": 25232.0,
        "60-80": 68.0,
        "80-100": 0.0,
        ">100": 0.0
      },
      "last_device_time": "2019-01-05T18:07:29.000Z",
      "ignitionSource": "ACC",
      "today_overspeeds": 0.0,
      "today_routeViolations": 0.0,
      "today_trips": 66
    }
}

