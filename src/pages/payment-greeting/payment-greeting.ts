import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-payment-greeting',
  templateUrl: 'payment-greeting.html',
})
export class PaymentGreetingPage {
  paymentTime = new Date()
  constructor(public navCtrl: NavController) { }

  ionViewDidLoad() {
    this.paymentTime;
  }

  feedback() {
    this.navCtrl.push("FeedbackPage");
  }
}
