import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { SignupPage } from '../signup/signup';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SignupOtp } from '../signup/signupOtp';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;
  submitAttempt: boolean;
  otpMess: any;
  responseMessage: string;
  data: {};
  logindata: any;
  userDetails: string;
  details: any;
  showPassword: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public apiservice: ApiServiceProvider,
    public toastCtrl: ToastController
  ) {
    this.loginForm = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  userlogin() {
    this.submitAttempt = true;
    if (this.loginForm.valid) {
      var UserName = isNaN(this.loginForm.value.username);
      if (UserName == false) {
        this.data = {
          "psd": this.loginForm.value.password,
          "ph_num": this.loginForm.value.username
        };
      } else {
        this.data = {
          "psd": this.loginForm.value.password,
          "emailid": this.loginForm.value.username
        };
      }
      this.apiservice.startLoading();
      this.apiservice.loginApi(this.data)
        .subscribe(response => {
          this.logindata = response;
          this.logindata = JSON.stringify(response);
          var logindetails = JSON.parse(this.logindata);
          this.userDetails = window.atob(logindetails.token.split('.')[1]);
          this.details = JSON.parse(this.userDetails);
          localStorage.setItem("loginflag", "loginflag");
          localStorage.setItem('details', JSON.stringify(this.details));
          localStorage.setItem('condition_chk', this.details.isDealer);
          this.apiservice.stopLoading();
          this.checkPageStatus(this.details._id);
        },
          error => {
            var body = error._body;
            if (body) {
              var msg = JSON.parse(body);
              if(msg.message == "User account is InActive"){
                // show alert
                
                  const alert = this.alertCtrl.create({
                    title: 'Account Inactive',
                    message: 'DL is pending for approval and within 12 Hours at max it will be approved.',
                    cssClass: 'alertStyle',
                    buttons: ['OK']
                  });
                  alert.present();
                
                  this.apiservice.stopLoading();
              }else{
                this.toastMsg(msg.message);
                this.responseMessage = "Something Wrong";
                this.apiservice.stopLoading();
              }
            }
            
          
          });
    }
  }

  presentToast(msg) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 2500
    });
    toast.present();
  }

  gotosignuppage() {
    this.navCtrl.push("SignupPage");
  }

  type = "password";
  show = false;

  toggleShow() {
    this.show = !this.show;
    if (this.show) {
      this.type = "text";
    }
    else {
      this.type = "password";
    }
  }

  resendOtp(pnum) {
    console.log("resend");
    console.log(pnum);
    let toast = this.toastCtrl.create({
      message: 'OTP Sent !!!',
      duration: 3000,
      position: 'top',
      cssClass: 'toastStyle'
    });
    let alert = this.alertCtrl.create({
      title: 'Verify Account',
      message: 'Please Verify User Detail for use App',
      cssClass: 'alertStyle',
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.navCtrl.push(SignupOtp);
            var phonenumber = { ph_num: pnum };
            this.apiservice.resendOtp(phonenumber)
              .subscribe(res => {
              })
          }
        }
      ]
    });
    alert.present();
  }

  checkPageStatus(uID) {
    let toast = this.toastCtrl.create({
      message: "Welcome! You're logged In successfully.",
      duration: 3000,
      position: 'bottom'
    });
    this.responseMessage = "LoggedIn  successfully";
    this.apiservice.checkRideStatus(uID)
      .subscribe(res => {
        var currentStatus = res.status;
        if (currentStatus == "COMPLETED") {
          if (res.payment_status == false) {
            console.log(res.payment_status);
            toast.onDidDismiss(() => {
              this.navCtrl.setRoot("RideTotalAmountPage");
            });
            toast.present();
          }
          else if (res.payment_status == true) {
            toast.onDidDismiss(() => {
              if(res.feedbackStatus == true) {
                this.navCtrl.setRoot("LivePage");
              } else {
                this.navCtrl.setRoot("FeedbackPage");
              }
            });
            toast.present();
          }
        }
        else if (currentStatus == "ASSIGNED") {
          toast.onDidDismiss(() => {
            this.navCtrl.setRoot("BookRidesPage");
          });

          toast.present()
        } else if (currentStatus == "LOADING") {
          toast.onDidDismiss(() => {
            this.navCtrl.setRoot("ConfirmBookingPage");
          });

          toast.present();
        } else {
          toast.onDidDismiss(() => {
            this.navCtrl.setRoot("LivePage");
          });
          toast.present();
        }
      }, err => {
        toast.setMessage("Internal Server Error, Please try again !!");
        toast.present();
      })
  }

  toastMsg(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    if (msg == "Mobile Phone Not Verified") {
      toast.onDidDismiss(() => {
        localStorage.setItem('mobnum', this.loginForm.value.username)
        this.navCtrl.push(SignupOtp);
      });
      return toast.present();
    }
    if (msg == "User is not registered") {
      toast.onDidDismiss(() => {
        this.navCtrl.push("SignupPage");
      });
      return toast.present();
    }
    return toast.present();
  }
}
