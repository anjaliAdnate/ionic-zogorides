import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RideBillingPage } from './ride-billing';

@NgModule({
  declarations: [
    RideBillingPage,
  ],
  imports: [
    IonicPageModule.forChild(RideBillingPage),
  ],
})
export class RideBillingPageModule {}
