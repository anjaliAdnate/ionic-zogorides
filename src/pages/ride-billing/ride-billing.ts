import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-ride-billing',
  templateUrl: 'ride-billing.html',
})
export class RideBillingPage {
  data: { title: string; details: string; icon: string; showDetails: boolean; }[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.data=[
      {
              title: '1. Unable to end ride?',
              details: 'Please restart the app and try to end the ride. Generally, end ride process may take 10 seconds to 30 seconds depending on your operator mobile network. Still Unable to End the ride Raise the issue to our support team support@zogorides.com',
              icon: 'ios-arrow-forward',
              showDetails: false
            },
            {
              title: '2. Pause the ride?',
              details: 'Please restart the app and try to end the ride. Generally, end ride process may take 10 seconds to 30 seconds depending on your operator mobile network. Still Unable to End the ride Raise the issue to our support team support@zogorides.com',
              icon: 'ios-arrow-forward',
              showDetails: false
            },
            {
              title: '3. I was overcharged for my ride?',
              details: 'Please write to us with your ride details to our support team, we will resolve the issue within 24 hours. support@zogorides.com',
              icon: 'ios-arrow-forward',
              showDetails: false
            }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RideBillingPage');
  }

  toggleDetails(data) {
    if (data.showDetails) {
        data.showDetails = false;
        data.icon = 'ios-arrow-forward';
    } else {
        data.showDetails = true;
        data.icon = 'ios-arrow-down';
    }
  }
}
