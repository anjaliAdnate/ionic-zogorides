import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-trip',
  templateUrl: 'trip.html',
})
export class TripPage implements OnInit {

  islogin: any;
  TripData: any;
  deviceId: any;
  distanceBt: number;
  TripStart_Time: string;
  TripEnd_Time: string;
  Durations: string;
  TripAllData: any[];
  tripdata: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,

    public apicalligi: ApiServiceProvider,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public modalCtrl: ModalController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  ionViewDidLoad() {
    this.getTrip();
  }

  ngOnInit() { }

  getTrip() {
    this.TripAllData = [];
    var baseURLp = 'https://www.oneqlik.in/trackRouteMap/getTrips?user=' + this.islogin._id + "&purpose=" + 'zogo';
    this.apicalligi.trip_details(baseURLp)
      .subscribe(data => {
        this.TripData = data;
        var fetchTime = this.TripData;
        for (var i = 0; i < this.TripData.length; i++) {
          this.TripStart_Time = new Date(this.TripData[i].startSiteEnterAt).toLocaleString();
          this.TripEnd_Time = new Date(this.TripData[i].endSiteExitAt).toLocaleString();
          var fd = new Date(this.TripStart_Time).getTime();
          var td = new Date(this.TripEnd_Time).getTime();
          var time_difference = td - fd;
          var total_min = time_difference / 60000;
          var hours = total_min / 60
          var rhours = Math.floor(hours);
          var minutes = (hours - rhours) * 60;
          var rminutes = Math.round(minutes);
          this.Durations = rhours + ':' + rminutes
          this.TripAllData.push({ '_id': this.TripData[i]._id, 'Actual_startTime': this.TripData[i].startsOn, 'Actual_endTime': this.TripData[i].expiresOn, 'Status': this.TripData[i].status, 'Unloading_Site': this.TripData[i].endSite.geoname, 'Loading_Site': this.TripData[i].startSite.geoname, 'durations': this.Durations, 'plannedStartTime': this.TripData[i].startsOn, 'purpose': this.TripData[i].purpose, 'createdOn': this.TripData[i].createdOn });
        }
      },
        err => {
          this.apicalligi.stopLoading();
        });
  }

  goto() {
    this.navCtrl.setRoot("BookRidesPage");
  }

  ShowTrip(tripdata) {
    if (tripdata.Status == "ASSIGNED") {
      var timerTime = tripdata.createdOn;
      localStorage.setItem('timerTIme', JSON.stringify(timerTime));
      localStorage.setItem("tripDetails", JSON.stringify(tripdata));
      this.navCtrl.push("BookRidesPage", {
        tripLocations: tripdata
      });
    } else if ((tripdata.Status == "COMPLETED") || (tripdata.Status == "CANCELLED")) {
      this.navCtrl.push("LivePage");
    } else if (tripdata.Status == "LOADING") {
      this.navCtrl.push("ConfirmBookingPage");
    } else {
      let toast = this.toast.create({
        message: 'check different Trip !!!',
        duration: 3000,
        position: 'top',
        cssClass: 'toastStyle'
      });
      toast.present();
    }
  }
}
