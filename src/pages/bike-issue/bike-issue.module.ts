import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BikeIssuePage } from './bike-issue';

@NgModule({
  declarations: [
    BikeIssuePage,
  ],
  imports: [
    IonicPageModule.forChild(BikeIssuePage),
  ],
})
export class BikeIssuePageModule {}
