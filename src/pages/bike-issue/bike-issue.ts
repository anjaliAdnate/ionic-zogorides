import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation';
declare var google;

@IonicPage()
@Component({
  selector: 'page-bike-issue',
  templateUrl: 'bike-issue.html',
})
export class BikeIssuePage {
  BikeIssueForm: FormGroup;
  scannedCode: string;
  address_show: any;
  locationShow: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public barcodeScanner: BarcodeScanner, public formBuilder: FormBuilder, public geolocation: Geolocation) {

    this.BikeIssueForm = formBuilder.group({
      name: ['', Validators.required],
      bikenumber: ['', Validators.required],
      note: ['', Validators.required],
      location: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BikeIssuePage');
  }

  ScanQrcode() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
    }, (err) => {
      console.log('barcodeScanner Error: ', err);
    });
  }

  BikeLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      var that = this;
      var initLat = resp.coords.latitude;
      var initLng = resp.coords.longitude;

      var geocoder = new google.maps.Geocoder();
      var latlng = new google.maps.LatLng(initLat, initLng);

      var request = {
        latLng: latlng
      };

      geocoder.geocode(request, function (data, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (data[0] != null) {
            that.address_show = data[0].formatted_address;
            that.locationShow = that.address_show;
          } else {
            that.address_show = data[0].formatted_address;
          }
        }
        else {
        }
      })
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }
}
