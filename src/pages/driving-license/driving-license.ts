import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ToastController, LoadingController, Platform } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-driving-license',
  templateUrl: 'driving-license.html',
})
export class DrivingLicensePage {
  lastImage: string = null;
  Imgloading: any;
  imgString: string;
  imgLink: string;
  signupObject: any;
  mobilenumber: any;
  docString: string;
  showButton: boolean;
  docImge: any;
  placeString: string = "upload document";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private transfer: Transfer,
    public transferObj: TransferObject,
    private file: File,
    public platform: Platform,
    private filePath: FilePath,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public apiCall: ApiServiceProvider) {
  }

  ionViewDidLoad() {
    this.signupObject = this.navParams.get('signupObj');
    this.mobilenumber = this.signupObject.phoneNum;
  }
  goBack() {
    this.navCtrl.pop();
  }

  docChange(ev) {
    console.log("doc key: ", ev)
    if (ev == "Adhar Card") {
      this.docString = "adharCard";
    } else {
      if (ev == "Pan Card") {
        this.docString = "panCard";
      } else {
        if (ev == "Driving Licence") {
          this.docString = "drivingLicence";
        } else {
          if (ev == "Selfie") {
            this.docString = "selfie";
          } else {
            let toast = this.toastCtrl.create({
              message: "Please choose valid doc",
              duration: 2000,
              position: "bottom"
            })
            toast.present();
          }
        }
      }
    }
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  public pathForImage(img) {
    console.log("Image=>", img);
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  public uploadImage() {
    var url = "https://www.oneqlik.in/users/uploadImage";
    var targetPath = this.pathForImage(this.lastImage);
    var filename = this.lastImage;
    var options = {
      fileKey: "photo",
      fileName: filename,
      chunkedMode: false,
      mimeType: "image/jpeg",
      params: { 'fileName': filename }
    };
    this.transferObj = this.transfer.create();
    this.Imgloading = this.loadingCtrl.create({
      content: 'Uploading...',
    });
    this.Imgloading.present();
    this.transferObj.upload(targetPath, url, options).then(data => {
      this.Imgloading.dismissAll();
      this.dlUpdate(data.response);
    }, err => {
      console.log("uploadError=>", err)
      this.lastImage = null;
      this.Imgloading.dismissAll();
      this.presentToast('Error while uploading file, Please try again !!!');
    });
  }

  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  public takePicture(sourceType) {
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then((imagePath) => {
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  dlUpdate(dllink) {
    var dlObj = {
      image_path: dllink,
      phone: this.mobilenumber,
      img_type: this.docString
    }
    this.apiCall.startLoading();
    this.apiCall.updateDL(dlObj)
      .subscribe(res => {
        this.apiCall.stopLoading();
        this.presentToast('Image succesful uploaded.');
        this.lastImage = null;
        this.docImge = undefined;
        this.docString = undefined;
        this.placeString = "upload another document"
        this.showButton = true;
        // this.navCtrl.setRoot("LoginPage");
      }, err => {
        this.apiCall.stopLoading();
        this.presentToast('Internal server Error !!!');
      })
  }

  subDone() {
    this.showButton = false;
    this.navCtrl.setRoot("LoginPage");
  }
}
