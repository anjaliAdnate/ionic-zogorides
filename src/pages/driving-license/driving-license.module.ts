import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DrivingLicensePage } from './driving-license';

@NgModule({
  declarations: [
    DrivingLicensePage,
  ],
  imports: [
    IonicPageModule.forChild(DrivingLicensePage),
  ],
})
export class DrivingLicensePageModule {}
