import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DropLocationPage } from './drop-location';

@NgModule({
  declarations: [
    DropLocationPage,
  ],
  imports: [
    IonicPageModule.forChild(DropLocationPage),
  ],
})
export class DropLocationPageModule {}
