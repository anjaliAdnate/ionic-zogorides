import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { GoogleMaps, Marker } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
declare var google;

@IonicPage()
@Component({
  selector: 'page-drop-location',
  templateUrl: 'drop-location.html',
})
export class DropLocationPage {
  cartCount: any;
  drop_locationShow: any;
  mapdata: any = {};
  drop_location: any;
  hideButton: boolean;
  navOptions = {
    animation: 'ios-transition'
  };
  dropOffObj: any;
  dstLattitude: any;
  dstLongitude: any;
  locations = [];
  total_km: any;
  Vehicle_count: any;
  dropLocationgeoId: any;
  useridd: any;
  plannedEndTIme: Date;
  deviceId: any;
  pickupgeofenceId: any;
  changeColor: boolean = false;
  polydatageo: any[];
  cordin: any[];
  bounds: any[];
  generalPolygon: any;
  polyRef: any;
  total_distance: string;
  total_veh: any;
  assignVehicle: any;
  showActionSheet: boolean;
  blockedAmt: any;
  constructor(public navCtrl: NavController, private geolocation: Geolocation, public app_api: ApiServiceProvider, public navParams: NavParams, public apiCall: ApiServiceProvider, public toast: ToastController) {

    var newObj = {}
    newObj = this.apiCall.getGeofence();
    this.total_km = newObj['totalDist'];
    this.Vehicle_count = newObj['totalVeh'];
    this.hideButton = true;
    var uID = localStorage.getItem('details');
    this.plannedStartTime.setMinutes(this.plannedStartTime.getMinutes() + 10);
    var getData = this.app_api.getGeofence();
    this.pickupgeofenceId = getData['geofenceId'];
    this.useridd = JSON.parse(uID)._id;
    var a = new Date();
    a.setHours(this.plannedStartTime.getHours() + 1, this.plannedStartTime.getMinutes(), this.plannedStartTime.getSeconds());
    this.plannedEndTIme = new Date(a);
    var geofenceOBJ = this.apiCall.getGeofence();
    this.deviceId = geofenceOBJ['device'];
    this.loadMap();
  }

  ionViewDidLoad() {
    this.getBlockedAmt();
  }

  ionViewDidEnter() {
    this.drop_location = localStorage.getItem("dropOffLocation");
    if (this.drop_location) {
      var d = JSON.parse(this.drop_location);
      this.drop_location = JSON.parse(this.drop_location).id;
      this.drop_locationShow = d['name'];
      this.dropOffObj = this.drop_location;
      this.chooseEnd();
    }
  }

  loadMap() {
    this.mapdata.map = GoogleMaps.create('map_canvas1', {
      camera: {
        target: { lat: 20.5937, lng: 78.9629 },
        zoom: 18,
        tilt: 30
      }
    });

    let marker: Marker = this.mapdata.map.addMarkerSync({
      title: 'Current location',
      icon: 'blue',
      animation: 'DROP',
      position: {
        lat: 43.0741904,
        lng: -89.3809802
      }
    });

    this.geolocation.getCurrentPosition().then((resp) => {
    }).catch((error) => {
    });
  }

  seeNotifications() { }

  dropOffLocation() {
    this.navCtrl.push("DropLocationSearchPage", null, this.navOptions)
  }

  chooseEnd() {
    this.app_api.startLoading();
    let that = this;
    var geocoderEnd = new google.maps.Geocoder;
    geocoderEnd.geocode({ 'placeId': this.drop_location }, (results, status) => {
      if (status === 'OK' && results[0]) {
        that.dstLattitude = results[0].geometry.location.lat();
        that.dstLongitude = results[0].geometry.location.lng();
        var baseURLp = 'https://www.oneqlik.in/zogo/nearby/dst?l=' + this.dstLattitude + ',' + this.dstLongitude;
        that.apiCall.getgeofenceCall(baseURLp)
          .subscribe(data => {
            that.app_api.stopLoading();
            that.locations = data.result;
          })
      } else {
        that.app_api.stopLoading();
        let toasterr = this.toast.create({
          message: status,
          duration: 3000,
          position: 'top',
          cssClass: 'toastStyle'
        });
        toasterr.present();
      }
    })
  }
  selectedStaion: any;
  autoManufacturers: any;
  plannedStartTime = new Date();

  authanticationBeforeBooking() {
    if (!this.dropStation) {
      let toastmsg = this.toast.create({
        message: 'Please select drop station',
        duration: 3000,
        position: 'top',
        cssClass: 'tStyle'
      });
      toastmsg.present();
    } else {
      var useDetails = JSON.parse(localStorage.getItem('details'));
      var userId = useDetails._id;
      this.apiCall.startLoading();
      this.apiCall.paytmValidation(userId)
        .subscribe(res => {
          if ((res.Status == "Token Not Found") || (res.message == "Invalid Token") || (res.status == "FAILURE")) {
            this.apiCall.stopLoading();
            localStorage.setItem("navigationFrom", "NotRegisteredWithPaytm")
            var tripDetail123 = {
              "user": this.useridd,
              "device": this.deviceId,
              "deviceName": "Testingzogo",
              "driver": this.useridd,
              "createdOn": Date.now(),
              "startsOn": this.plannedStartTime,
              "expiresOn": this.plannedEndTIme,
              "tripType": 0,
              "poi": [],
              "purpose": "zogo",
              "startSite": this.pickupgeofenceId,
              "endSite": this.dropLocationgeoId
            }
            this.navCtrl.push("PaytmwalletloginPage", {
              "tripParams": tripDetail123
            });
          } else if ((res.mobile) && (res.id)) {
            this.getPaytmBalance(userId);
            var paytmNumber = res.mobile;
            localStorage.setItem('paytmregNum', paytmNumber);
          }
        })
    }
  }

  getPaytmBalance(uId) {
    let toast_validation = this.toast.create({
      duration: 3000,
      position: 'middle'
    });

    this.apiCall.paytmbalance(uId)
      .subscribe(res => {
        if (res.code == "ETIMEDOUT") {
          this.apiCall.stopLoading();
          toast_validation.setMessage("Etimeout server error");
          toast_validation.present();
        } else {
          if (res.response.amount >= this.blockedAmt) {
            this.bookRides();
          } else {
            this.apiCall.stopLoading();
            localStorage.setItem("navigationFrom", "NotRegisteredWithPaytm")
            this.navCtrl.push("WalletPage",{"lowBalance":"addAmt"});
          }
        }
      })
  }

  payRideFare(rideID) {
    let toast_validation1 = this.toast.create({
      duration: 3000,
      position: 'middle'
    });
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    var preAuthObj =
    {
      "CUST_ID": userId,
      "TXN_AMOUNT": this.blockedAmt,
      "DURATIONHRS": "70",
      "app_id": "zogo",
      "ride": rideID._id
    }
    this.apiCall.preAuthantication(preAuthObj)
      .subscribe(res => {
        this.apiCall.stopLoading();
        var responseMsg = res;
        var StatusMsg = responseMsg ? responseMsg.STATUSMESSAGE : "";
        var SuccessRes = responseMsg ? responseMsg.STATUS : "";
        var checksumError = responseMsg ? responseMsg.ErrorMsg : '';
        if (SuccessRes == 'TXN_SUCCESS') {
          this.navCtrl.push("BookRidesPage", null, this.navOptions);
        } else if (checksumError) {
          toast_validation1.setMessage("Internal Server Error");
          toast_validation1.present();
        } else if (StatusMsg) {
          toast_validation1.setMessage(StatusMsg);
        } else {
          toast_validation1.setMessage("Internal Server Error");
          toast_validation1.present();
        }
      })
  }

  toastMessages(msg) {
    let toastmsgVar = this.toast.create({
      message: msg,
      duration: 5000,
      position: 'top',
      cssClass: 'tStyle'
    })
    return toastmsgVar.present();
  }

  bookRides() {
    let toastmsg = this.toast.create({
      message: 'Please select drop station',
      duration: 3000,
      position: 'top',
      cssClass: 'tStyle'
    })
    if (this.dropStation) {
      var tripDetail = {
        "user": this.useridd,
        "device": this.deviceId,
        "deviceName": "Testingzogo",
        "driver": this.useridd,
        "createdOn": Date.now(),
        "startsOn": this.plannedStartTime,
        "expiresOn": this.plannedEndTIme,
        "tripType": 0,
        "poi": [],
        "purpose": "zogo",
        "startSite": this.pickupgeofenceId,
        "endSite": this.dropLocationgeoId
      }
      this.app_api.savetripDetails(tripDetail)
        .subscribe(res => {
          var timerTime = new Date();
          var tempRes = res;
          if (tempRes) {
            localStorage.setItem('timerTIme', JSON.stringify(timerTime));
            localStorage.setItem("tripDetails", JSON.stringify(tempRes));
            // localStorage.setItem("tripID_imp", tempRes._id)
            this.payRideFare(tempRes);
          } else {
            this.apiCall.stopLoading();
          }
        }, err => {
          if (err.status == 400) {
            this.app_api.stopLoading();
            var abtemp = JSON.parse(err._body)
            this.toastMessages(abtemp.message);
          }
        })
    } else {
      this.app_api.stopLoading();
      toastmsg.present();
    }
  }

  dropStation: any;
  selectedOption(ev) {
    this.dropStation = ev;
    this.dropLocationgeoId = ev._id
    for (var i = 0; i < this.locations.length; i++) {
      if (this.locations[i]._id != ev._id)
        this.locations[i].changeColor = false;
      else
        this.locations[i].changeColor = true;
    }
    localStorage.setItem("droplocatoingeoId", JSON.stringify(ev));
  }

  goBack() {
    this.navCtrl.pop();
  }

  getBlockedAmt() {
    this.app_api.getblockamt()
      .subscribe(res => {
        this.blockedAmt = res['Block Amount'];
      }, err => {
      })
  }

  cancelride() {
    var reason = "plan changed"
    var cancellationTime = new Date();
    var tempDetail = localStorage.getItem('tripDetails');
    console.log("kkk", tempDetail)
    var tempRouteDetail = JSON.parse(tempDetail);
    console.log(tempRouteDetail);
    console.log("identifier=>", tempDetail)
    var cancelObj = {
      "_id": tempRouteDetail._id,
      "cancelledAt": cancellationTime,
      "status": "CANCELLED",
      "device": tempRouteDetail.device,
      "cancelledBy": this.useridd,
      "cancellationReason": reason

    };
    this.app_api.startLoading();
    this.app_api.canceltripDetails(cancelObj)
      .subscribe(res => {
        this.app_api.stopLoading();
        localStorage.removeItem('tripDetails');
        localStorage.removeItem('pickupLocation');
        localStorage.removeItem("dropOffLocation");
        localStorage.removeItem('timerTIme');
        localStorage.removeItem('lockRide');
      }, err => {
        this.app_api.stopLoading();
      })
  }
}
