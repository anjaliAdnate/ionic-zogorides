import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-zogomodal',
  templateUrl: 'zogomodal.html',
})
export class ZogomodalPage {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ZogomodalPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  lockIssues() {
    this.navCtrl.push("LockIssuePage");
  }

  reportMisuse() {
    this.navCtrl.push("ReportMisusePage");
  }
  bikeIssue() {
    this.navCtrl.push("BikeIssuePage");
  }
  rideBilling() {
    this.navCtrl.push("RideBillingPage");
  }
}
