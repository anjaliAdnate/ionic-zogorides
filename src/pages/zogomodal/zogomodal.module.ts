import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ZogomodalPage } from './zogomodal';

@NgModule({
  declarations: [
    ZogomodalPage,
  ],
  imports: [
    IonicPageModule.forChild(ZogomodalPage),
  ],
})
export class ZogomodalPageModule {}
