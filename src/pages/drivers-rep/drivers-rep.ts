import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { ConfirmBookingPage } from '../confirm-booking/confirm-booking';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-drivers-rep',
  templateUrl: 'drivers-rep.html',
})
export class DriversRepPage implements OnInit {

  navOptions = {
    animation: 'ios-transition'
  };
  scannedCode = null;
  actualStartTime: Date;
  temptrip: any;
  constructor(public navCtrl: NavController, public barcodeScanner: BarcodeScanner, public app_api: ApiServiceProvider) { }

  ngOnInit() {
    this.ScanQrcode();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DriversRepPage');
  }

  ScanQrcode() {
    var that = this;
    this.barcodeScanner.scan().then(barcodeData => {
      this.actualStartTime = new Date();
      that.tipStarted();
      this.BikeUnlock();
      localStorage.setItem("newDevId", "5b49b12b6c496770dfd288d4");
    }, (err) => {
      console.log('Error: ', err);
    });
  }

  BikeUnlock() {
    this.actualStartTime = new Date();
    localStorage.removeItem('timerTIme');
  }

  tipStarted() {
    var tempDetail = localStorage.getItem('tripDetails');
    var tempRouteDetail = JSON.parse(tempDetail);
    var startObj = {
      _id: tempRouteDetail._id,
      status: "LOADING",
      AcctualStartTime: new Date(),
      device: tempRouteDetail.device
    }
    this.app_api.updatetripDetails(startObj)
      .subscribe(res => {
        if (res) {
          localStorage.removeItem("lockRide");
          this.navCtrl.push(ConfirmBookingPage, null, this.navOptions);
        }
      }, err => {
      })
  }
}
