import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriversRepPage } from './drivers-rep';

@NgModule({
  declarations: [
    DriversRepPage,
  ],
  imports: [
    IonicPageModule.forChild(DriversRepPage),
  ],
})
export class DriversRepPageModule {}
