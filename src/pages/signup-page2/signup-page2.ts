import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-signup-page2',
  templateUrl: 'signup-page2.html',
})
export class SignupPage2Page {
  DlNo: any;
  Name: any;
  signupFormNew: FormGroup;
  DlType = [{
    value: 'dl',
    viewValue: "Driving License"
  }, {
    value: 'Adhar',
    viewValue: "Adhar Card"
  }, {
    value: 'PAN',
    viewValue: "PAN Card"
  }, {
    value: 'voterCard',
    viewValue: "Voter ID Card"
  }

  ];
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {

    this.signupFormNew = formBuilder.group({
      DlNo: [""],
      Name: [""]
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage2Page');
  }

  continue() { }

  gotoLogin() {
    this.navCtrl.setRoot("LoginPage");
  }
}
