import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupPage2Page } from './signup-page2';

@NgModule({
  declarations: [
    SignupPage2Page,
  ],
  imports: [
    IonicPageModule.forChild(SignupPage2Page),
  ],
})
export class SignupPage2PageModule {}
