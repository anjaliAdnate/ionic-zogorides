import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-paytmwalletlogin',
  templateUrl: 'paytmwalletlogin.html',
})
export class PaytmwalletloginPage {
  paytmmail: any;
  paytmnumber: any;
  paytmotp: any;
  successresponse: boolean = false;
  inputform: boolean = false;
  blockedAmt: any;
  tripDetails: any;
  checkPageNav: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public apiCall: ApiServiceProvider, private menu: MenuController, public toast: ToastController) {
    if (navParams.get("chnagenumParam")) {
      this.checkPageNav = navParams.get("chnagenumParam");
    }
    var num = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
    this.paytmnumber = num.phn;
    if (navParams.get("tripParams")) {
      this.tripDetails = navParams.get("tripParams");
    }
  }
  ionViewDidEnter() {
    this.menu.enable(true);
    this.getBlockedAmt();
  }

  getBlockedAmt() {
    this.apiCall.startLoading();
    this.apiCall.getblockamt()
      .subscribe(res => {
        this.apiCall.stopLoading();
        this.blockedAmt = res['Block Amount'];
      }, err => {
        this.apiCall.stopLoading();
      })
  }

  paytmSignupOTPResponse: any;
  
  paytmwallet() {

    var useDetails = localStorage.getItem('details');
    var userId = JSON.parse(useDetails)._id;

    var paytmUserCredentials = {
      "user": userId,
      "app_id": "zogo",
      "phone": this.paytmnumber,
      "scope": "wallet",
      "responseType": "token",
    }

    this.apiCall.paytmLoginWallet(paytmUserCredentials)
      .subscribe(res => {
        this.successresponse = true;
        this.inputform = true;
        this.paytmSignupOTPResponse = res;
        let errToast_success = this.toast.create({
          message: 'OTP sent successfully, Please type OTP in next field !!!',
          duration: 3000,
          position: 'top',
          cssClass: "toastStyle"
        })
        errToast_success.present();
      }, err => {
        let errToast = this.toast.create({
          message: 'Server error , Please try after somtime !!!',
          duration: 3000,
          position: 'top',
          cssClass: "toastStyle"
        })
        if (err) {
          errToast.present();
        }
      })
  }

  goBack() {
    this.navCtrl.pop();
  }

  resndOTP() {
    var useDetails = localStorage.getItem('details');
    var userId = JSON.parse(useDetails)._id;
    var paytmUserCredentials = {
      "user": userId,
      "app_id": "zogo",
      "phone": this.paytmnumber,
      "scope": "wallet",
      "responseType": "token"
    }
    this.apiCall.paytmLoginWallet(paytmUserCredentials)
      .subscribe(res => {
        this.successresponse = true;
        this.inputform = true;
        this.paytmSignupOTPResponse = res;
        let errToast_success = this.toast.create({
          message: 'OTP sent successfully, Please type OTP in next field !!!',
          duration: 3000,
          position: 'top',
          cssClass: "toastStyle"
        })
        errToast_success.present();

      }, err => {
        let errToast = this.toast.create({
          message: 'Server error , Please try after somtime !!!',
          duration: 3000,
          position: 'top',
          cssClass: "toastStyle"
        })
        if (err) {
          console.log("error occured !!!");
          errToast.present();
        }
      })
  }

  navOptions = {
    animation: 'ios-transition'
  };

  paytmAuthantication() {
    this.apiCall.startLoading();
    var otpCredentials = {
      "otp": this.paytmotp,
      "transac_id": this.paytmSignupOTPResponse.transac_id,
      "app_id": "zogo"
    }
    this.apiCall.paytmOTPvalidation(otpCredentials)
      .subscribe(res => {
        // this.apiCall.stopLoading();
        localStorage.setItem('paytmregNum', this.paytmnumber);
        if (res.status == 'SUCCESS') {
          var useDetails = JSON.parse(localStorage.getItem('details'));
          var userId = useDetails._id;
          this.getPaytmBalance(userId);
        } else {
          this.apiCall.stopLoading();
          var resp = res.response;
          var resp1 = JSON.parse(resp);
          if (resp1.status == "FAILURE") {
            let toast = this.toastCtrl.create({
              message: resp1.message,
              duration: 3000,
              position: 'top'
            });
            toast.present();
          }
        }
      }, err => {
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: 'internal server Error, Please try after sometime !!!',
          duration: 3000,
          position: 'top'
        });
        toast.present();
      })
  }

  getPaytmBalance(uId) {
    let toast_validation = this.toastCtrl.create({
      duration: 3000,
      position: 'middle'
    });

    // this.apiCall.startLoading();
    this.apiCall.paytmbalance(uId)
      .subscribe(res => {
       
        if (res.code == "ETIMEDOUT") {
          this.apiCall.stopLoading();
          toast_validation.setMessage("Etimeout server error");
          toast_validation.present();
        } else {
          if (res.response.amount >= this.blockedAmt) {
            if (this.checkPageNav != undefined) {
              this.apiCall.stopLoading();
              localStorage.setItem("paytmregNum", this.paytmnumber);
              this.navCtrl.push('WalletPage', null, this.navOptions);
            } else {
              this.bookRides();
            }
          } else {
            this.apiCall.stopLoading();
            this.navCtrl.push('WalletPage', null, this.navOptions);
          }
        }
      })
  }

  bookRides() {
    // this.apiCall.startLoading();
    this.apiCall.savetripDetails(this.tripDetails)
      .subscribe(res => {
       
        var timerTime = new Date();
        var tempRes = res;
        if (tempRes) {
          this.payRideFare(tempRes);
          localStorage.setItem('timerTIme', JSON.stringify(timerTime));
          localStorage.setItem("tripDetails", JSON.stringify(tempRes));
        } else {
          this.apiCall.stopLoading();
        }
      }, err => {
        if (err.status == 400) {
          this.apiCall.stopLoading();
          var abtemp = JSON.parse(err._body)
          this.toastMessages(abtemp.message);
        }
      })
  }


  toastMessages(msg) {
    let toastmsgVar = this.toast.create({
      message: msg,
      duration: 5000,
      position: 'top',
      cssClass: 'tStyle'
    })
    return toastmsgVar.present();
  }

  payRideFare(rideID) {
    // this.apiCall.startLoading();
    let toast_validation1 = this.toast.create({
      duration: 3000,
      position: 'middle'
    });
    var useDetails = JSON.parse(localStorage.getItem('details'));
    var userId = useDetails._id;
    var preAuthObj =
    {
      "CUST_ID": userId,
      "TXN_AMOUNT": this.blockedAmt,
      "DURATIONHRS": "70",
      "app_id": "zogo",
      "ride": rideID._id
    }
    this.apiCall.preAuthantication(preAuthObj)
      .subscribe(res => {
        this.apiCall.stopLoading();
        var responseMsg = res;
        var StatusMsg = responseMsg ? responseMsg.STATUSMESSAGE : "";
        var SuccessRes = responseMsg ? responseMsg.STATUS : "";
        var checksumError = responseMsg ? responseMsg.ErrorMsg : '';

        if (SuccessRes == 'TXN_SUCCESS') {
          this.navCtrl.push("BookRidesPage", null, this.navOptions);
        } else if (checksumError) {
          toast_validation1.setMessage("Internal Server Error");
          toast_validation1.present();
        } else if (StatusMsg) {
          toast_validation1.setMessage(StatusMsg);
        } else {
          toast_validation1.setMessage("Internal Server Error");
          toast_validation1.present();
        }
      })
  }
}
