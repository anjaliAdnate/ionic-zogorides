import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LockIssuePage } from './lock-issue';

@NgModule({
  declarations: [
    LockIssuePage,
  ],
  imports: [
    IonicPageModule.forChild(LockIssuePage),
  ],
})
export class LockIssuePageModule {}
