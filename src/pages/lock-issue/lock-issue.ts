import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-lock-issue',
  templateUrl: 'lock-issue.html',
})
export class LockIssuePage {
  data: Array<{ title: string, details: string, icon: string, showDetails: boolean }> = [];
  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.data = [
      {
        title: '1. I am unable to open lock?',
        details: "Please make sure the QR code you are scanning that is visible clearly. If you are still unable to scan the QR code and unlock the bike, please connect to our support team on support@zogorides.com or call us at 8178076078.",
        icon: 'ios-arrow-forward',
        showDetails: false
      },
      {
        title: '2. Bike with Private lock?',
        details: 'Our Bikes do not have any private lock. If you find any bike with private lock please raise an issue to our support team, we will immediately help you to resolve the issue- support@zogorides.com or inform us on the call 8178076078.',
        icon: 'ios-arrow-forward',
        showDetails: false
      }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LockIssuePage');
  }

  toggleDetails(data) {
    if (data.showDetails) {
      data.showDetails = false;
      data.icon = 'ios-arrow-forward';
    } else {
      data.showDetails = true;
      data.icon = 'ios-arrow-down';
    }
  }
}
