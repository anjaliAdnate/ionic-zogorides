import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";
import { LoadingController } from 'ionic-angular';

@Injectable()
export class ApiServiceProvider {
  private headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
  link: string = "https://www.oneqlik.in/users/";
  loading: any;
  appId = "zogo";
  constructor(
    public http: Http,
    public loadingCtrl: LoadingController) {
    console.log('Hello ApiServiceProvider Provider');
  }
  // 13.126.36.205
  ////////////////// LOADING SERVICE /////////////////
  startLoading() {
    console.log("loading Start");
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      spinner: "bubbles"
    });
    return this.loading.present();
  }

  startLoading123(msgstr) {
    console.log("loading Start");
    this.loading = this.loadingCtrl.create({
      content: msgstr,
      spinner: "bubbles"
    });
    return this.loading.present();
  }

  stopLoading() {
    return this.loading.dismiss();
  }
  ////////////////// END LOADING SERVICE /////////////

  feedbackData(payload) {
    return this.http.post("https://www.oneqlik.in/trackRouteMap/ZogorideFeedback", payload, { headers: this.headers })
      .map(res => res.json());
  }

  promocodeService(_id, amt, ride_id, code) {
    
    return this.http.get("https://www.oneqlik.in/discountCode/applycode?user=" + _id + "&amount=" + amt + "&ride_id=" + ride_id + "&code=" + code, { headers: this.headers })
      .map(res => res.json());
  }

  getDriverList(_id) {
    return this.http.get("https://www.oneqlik.in/driver/getDrivers?userid=" + _id, { headers: this.headers })
      .map(res => res.json());
  }

  filterByDateCall(_id, skip: Number, limit: Number, dates) {
    // console.log("from date => "+ dates.fromDate.toISOString())
    // console.log("new date "+ new Date(dates.fromDate).toISOString())
    var from = new Date(dates.fromDate).toISOString();
    var to = new Date(dates.toDate).toISOString();
    return this.http.get('https://www.oneqlik.in/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
      .map(res => res.json());
  }

  filterByType(_id, skip: Number, limit: Number, key) {
    return this.http.get('https://www.oneqlik.in/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&type=' + key, { headers: this.headers })
      .map(res => res.json());
  }

  getFilteredcall(_id, skip: Number, limit: Number, key) {
    return this.http.get('https://www.oneqlik.in/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
      .map(res => res.json());
  }

  getDataOnScroll(_id, skip: Number, limit: Number) {
    return this.http.get('https://www.oneqlik.in/notifs/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
      .map(res => res.json());
  }

  getVehicleListCall(_id, email) {
    return this.http.get('https://www.oneqlik.in/devices/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
      .map(res => res.json());
  }

  trip_detailCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  trip_details(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  trackRouteDataCall(data) {
    return this.http.post("https://www.oneqlik.in/trackRoute", data, { headers: this.headers })
      .map(res => res.json());
  }

  savetripDetails(data) {
    return this.http.post("https://www.oneqlik.in/trackRouteMap", data)
      .map(res => res.json());
  }

  canceltripDetails(a) {

    return this.http.post("https://www.oneqlik.in/trackRouteMap/cancelRide", a)
      .map(res => res);
  }

  updatetripDetails(a) {
    console.log("update trip")
    return this.http.post("https://www.oneqlik.in/trackRouteMap/updateRide", a)
      .map(res => res);
  }

  completetripDetails(a) {
    return this.http.post("https://www.oneqlik.in/trackRouteMap/rideComplete", a)
      .map(res => res);
  }

  updateRoute(edata) {
    return this.http.post("https://www.oneqlik.in/trackRouteMap/updateRoute_map", edata)
      .map(res => res.json());
  }

  gettrackRouteCall(link, data) {
    return this.http.post(link, data, { headers: this.headers })
      .map(res => res.json());
  }

  trackRouteCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getRoutesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getgeofenceCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  loginApi(userdata) {
    return this.http.post(this.link + "LoginWithOtp", userdata, { headers: this.headers })
      .map(res => res.json());
  }

  signupApi(usersignupdata) {
      return this.http.post('https://www.oneqlik.in/users/signUpZogo', usersignupdata, { headers: this.headers })
      .map(res => res.json());
  }

  updateDL(updateDL) {
    return this.http.post('https://www.oneqlik.in/users' + "/zogoUserUpdate", updateDL)
      .map(res => res.json());
  }

  resendOtp(phnum) {
    return this.http.post('https://www.oneqlik.in/users' + "/sendOtpZRides", phnum)
      .map(res => res.json());
  }

  pushnotifyCall(pushdata) {
    return this.http.post("https://www.adnateiot.com/users/PushNotification", pushdata, { headers: this.headers })
      .map(res => res.json());
  }

  siginupverifyCall(usersignup) {
    return this.http.post('https://www.oneqlik.in/users' + "/signUpZogo", usersignup, { headers: this.headers })
      .map(res => res.json());
  }

  pickupGeofence = {};

  setGeofenceData(pickGeoObj) {
    this.pickupGeofence = pickGeoObj;
    console.log(this.pickupGeofence);
  }

  getGeofence() {
    console.log("getFunc")
    return this.pickupGeofence;
  }

  // paytm integration


  // https://www.oneqlik.in/paytm/sendOTP

  paytmValidation(userCred) {
    return this.http.get('https://www.oneqlik.in/paytm/ValidateToken?CUST_ID=' + userCred + "&app_id=" + this.appId, { headers: this.headers })
      .map(res => res.json());
  }

  paytmbalance(userCred) {
    return this.http.get('https://www.oneqlik.in/paytm/checkBalance?CUST_ID=' + userCred + "&app_id=" + this.appId, { headers: this.headers })
      .map(res => res.json());
  }

  addMoneyPaytm(amtObj) {
    return this.http.post('https://www.oneqlik.in/paytm/addMoney', amtObj, { headers: this.headers })
    
      .map(res => res);
  }

  paytmLoginWallet(walletcredentials) {
    return this.http.post('https://www.oneqlik.in/paytm/sendOTP', walletcredentials, { headers: this.headers })
      .map(res => res.json());
  }
  paytmOTPvalidation(otpObj) {
    return this.http.post('https://www.oneqlik.in/paytm/validateOTP', otpObj, { headers: this.headers })
      .map(res => res.json());
  }

  paytmAddMount(amountObj) {
    return this.http.post('https://pguat.paytm.com/oltp-web/processTransaction', amountObj, { headers: this.headers })
      .map(res => res);
  }

  preAuthantication(preAuthObj) {
    return this.http.post('https://www.oneqlik.in/paytm/preAuth', preAuthObj, { headers: this.headers })
      .map(res => res.json());
  }

  proceedSecurely(withdrawObj) {
    return this.http.post('https://www.oneqlik.in/paytm/Withdraw', withdrawObj, { headers: this.headers })
      .map(res => res.json());
  }

  getblockamt() {
    //   http://localhost:3000/zogo/getInnitialBlockAmount
    return this.http.get('https://www.oneqlik.in/zogo/getInnitialBlockAmount')
      .map(res => res.json());
  }

  releaseAmount(uid) {
    return this.http.post('https://www.oneqlik.in/paytm/releaseAmount', uid, { headers: this.headers })
      .map(res => res.json());
  }

  rideFare(uid) {
    return this.http.get('https://www.oneqlik.in/zogo/getBillingAmount?userid=' + uid, { headers: this.headers })
      .map(res => res.json());
  }

  checkTripStatus(id) {
    console.log(id);
    return this.http.get('https://www.oneqlik.in/trackRouteMap/getRideStatus?_id=' + id, { headers: this.headers })
      .map(res => res.json());
  }

  pauseRide(payloadPause) {
    console.log(payloadPause);
    return this.http.post('https://www.oneqlik.in/trackRouteMap/zogoPause', payloadPause, { headers: this.headers })
      .map(res => res.json());
  }

  resumeRide(payloadRestart) {
    console.log(payloadRestart);
    return this.http.post('https://www.oneqlik.in/trackRouteMap/zogoRestart', payloadRestart, { headers: this.headers })
      .map(res => res.json());
  }

  checkRideStatus(uId) {
    return this.http.get('https://www.oneqlik.in/zogo/getLastRideStatusByUser?user=' + uId, { headers: this.headers })
      .map(res => res.json());
  }

  addcommandQueue(devImei) {
    return this.http.post("https://www.oneqlik.in/trackRouteMap/addCommandQueue", devImei, { headers: this.headers })
      .map(res => res.json());
  }

  updateTripStatus(uId, imp_id, field, amt) {
    return this.http.get('https://www.oneqlik.in/zogo/updateZogoPaymentStatus?user=' + uId + '&imp_id=' + imp_id + "&field=" + field + "&amt=" + amt, { headers: this.headers })
      .map(res => res.json());
  }

  navigateTO() {
    return this.http.get('https://www.google.com/maps/search/?api=1&query=47.5951518,-122.3316393&query_place_id=ChIJKxjxuaNqkFQR3CK6O1HNNqY')
      .map(res => res.json());
  }

}


