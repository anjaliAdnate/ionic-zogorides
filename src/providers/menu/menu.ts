
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MenuProvider {

  constructor(public http: Http) { }

  getSideMenus() {
    return [{
      title: 'Home', component: 'DashboardPage', icon: 'home'
    }, {
      title: 'Groups', component: 'GroupsPage', icon: 'people'
    }, {
      title: 'Customers', component: 'CustomersPage', icon: 'contacts'
    } ];
  }
}
