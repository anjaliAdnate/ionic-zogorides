import { TransferObject, Transfer } from '@ionic-native/transfer';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { MyApp } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NetworkInterface } from '@ionic-native/network-interface';
import { GoogleMaps, Spherical } from '@ionic-native/google-maps';
import { IonBottomDrawerModule } from '../../node_modules/ion-bottom-drawer';
import { SideMenuContentComponent } from '../../shared/side-menu-content/side-menu-content.component';
import { Geolocation } from '@ionic-native/geolocation';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { ComponentsModule } from '../components/components.module';
import { BookingDetailComponent } from '../components/booking-detail/booking-detail';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { SMS } from '@ionic-native/sms';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { SignupOtp } from '../pages/signup/signupOtp';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { NetworkProvider } from '../providers/network/network';
import { MenuProvider } from '../providers/menu/menu';
import { OfflineModalPage } from '../pages/offline-modal/offline-modal';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';

@NgModule({
  declarations: [
    MyApp,
    SideMenuContentComponent,
    SignupOtp,
    OfflineModalPage
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonBottomDrawerModule,
    SelectSearchableModule,
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BookingDetailComponent,
    SignupOtp,
    OfflineModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiServiceProvider,
    Network,
    NetworkProvider,
    NetworkInterface,
    MenuProvider,
    GoogleMaps,
    Spherical,
    Geolocation,
    BarcodeScanner,
    LocationAccuracy,
    SocialSharing,
    InAppBrowser,
    FileTransfer,
    FileTransferObject,
    SMS,
    Transfer,
    TransferObject,
    File,
    Camera,
    FilePath,
    AndroidPermissions,
    BluetoothSerial
  ]
})
export class AppModule { }
