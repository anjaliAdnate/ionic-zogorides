import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController, ModalController, AlertController, App, ToastController, Keyboard } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SideMenuOption } from '../../shared/side-menu-content/models/side-menu-option';
import { SideMenuContentComponent } from '../../shared/side-menu-content/side-menu-content.component';
import { SideMenuSettings } from '../../shared/side-menu-content/models/side-menu-settings';
import { ReplaySubject } from "rxjs/ReplaySubject";
import { MenuProvider } from '../providers/menu/menu';
import { NetworkProvider } from '../providers/network/network';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { OfflineModalPage } from '../pages/offline-modal/offline-modal';

@Component({
  selector: 'app-main-page',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  @ViewChild(SideMenuContentComponent) sideMenu: SideMenuContentComponent;
  rootPage: any;
  pages: any;
  selectedMenu: any;
  islogin: any;
  setsmsforotp: string;
  DealerDetails: any;
  dealerStatus: any;
  public options: Array<SideMenuOption>;
  public sideMenuSettings: SideMenuSettings = {
    accordionMode: true,
    showSelectedOption: true,
    selectedOptionClass: 'active-side-menu-option'
  };
  private unreadCountObservable: any = new ReplaySubject<number>(0);
  userdetails: any;
  tripStatus: any;
  menuStat: boolean;
  modalPage: any;
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public events: Events,
    public networkProvider: NetworkProvider,
    public menuProvider: MenuProvider,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public app: App,
    public appAPi: ApiServiceProvider,
    public toast: ToastController,
    public keyboard: Keyboard,
    // public viewCtrl: ViewController
  ) {
    // var s = 0;

    this.events.subscribe('user:updated', (udata) => {
      this.islogin = udata;
    });

    events.subscribe('menu:opened', () => {
      console.log("menu Opened")
      this.menuStat = true;
    });

    events.subscribe('menu:closed', () => {
      console.log("menu closed")
      this.menuStat = false;
    });

    platform.ready().then(() => {
      statusBar.styleDefault();
      statusBar.hide();
      this.splashScreen.hide();
      this.checkRideStatus();

    });
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.setsmsforotp = localStorage.getItem('setsms');
    this.getSideMenuData();
    this.initializeApp();
  }



  menuClosed() {
    this.events.publish('menu:closed', '');
  }

  menuOpened() {
    this.events.publish('menu:opened', '');
  }

  getSideMenuData() {
    this.pages = this.menuProvider.getSideMenus();
  }

  backBtnHandler() {
    this.platform.registerBackButtonAction(() => {

      let nav = this.app.getActiveNavs()[0];
      let activeView = nav.getActive();

      if (activeView.name === "LivePage") {

        if (nav.canGoBack()) { //Can we go back?
          nav.pop();
        } else {
          if (this.menuStat) {
            this.menuCtrl.close();
          } else {
            const alert = this.alertCtrl.create({
              title: 'App termination',
              message: 'Do you want to close the app?',
              buttons: [{
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                  console.log('Application exit prevented!');
                }
              }, {
                text: 'Close App',
                handler: () => {
                  this.platform.exitApp(); // Close this application
                }
              }]
            });
            alert.present();
          }

        }
      } else {
        if (activeView.name === "SignupPage") {
          nav.pop();
        } else {
          if (activeView.name === "DrivingLicensePage") {
            nav.pop();
          }
        }
      }
    });
  }

  initializeApp() {
    let that = this;
    that.platform.ready().then(() => {

      that.networkProvider.initializeNetworkEvents();

      // Offline event
      that.events.subscribe('network:offline', () => {
        let toast = that.toast.create({
          message: "Oops your offline, please check your connection..",
          duration: 2000,
          position: "bottom"
        })
        toast.present();
        // that.modalCtrl.create(OfflineModalPage);
        if(localStorage.getItem("ignitionLock")) {
          var data = { key: localStorage.getItem("ignitionLock") };
          this.modalPage = this.modalCtrl.create(OfflineModalPage, data);
          this.modalPage.present();
        }
        
      });

      // Online event
      that.events.subscribe('network:online', () => { 
        // this.viewCtrl.dismiss();
        this.modalPage.dismiss();
        that.toast.create({
          message: "Your online.. enjoy",
          duration: 1500,
          position: "bottom"
        }).present();
      });

      that.backBtnHandler();
    });

    // Initialize some options
    that.initializeOptions();
    // Change the value for the batch every 5 seconds
    setInterval(() => {
      this.unreadCountObservable.next(Math.floor(Math.random() * 10) + 1);
    }, 5000);
  }


  checkRideStatus() {
    var uID = this.islogin._id;
    if (uID) {
      this.appAPi.checkRideStatus(uID)
        .subscribe(res => {
          debugger
          var currentStatus = res.status;
          if (currentStatus == "COMPLETED") {
            if (res.payment_status == false) {
              this.rootPage = "RideTotalAmountPage";
            }
            else if (res.payment_status == true) {
              if (res.feedbackStatus == true) {
                this.rootPage = "LivePage";
              } else {
                this.rootPage = "FeedbackPage";
              }
            }
          }
          else if (currentStatus == "ASSIGNED") {
            this.rootPage = "BookRidesPage";
          } else if ((currentStatus == "LOADING") || (currentStatus == "TRANSIT")) {
            this.rootPage = "ConfirmBookingPage";
          }
          else if (localStorage.getItem("loginflag") == "loginflag") {
            this.rootPage = "LivePage";
          } else {
            this.rootPage = "AppEntryPage";
          }
        }, err => {
          console.log("error occured: ", err);
          if (localStorage.getItem("loginflag") == "loginflag") {
            this.rootPage = "LivePage";
          } else {
            this.rootPage = "AppEntryPage";
          }
        })
    } else {
      this.rootPage = "AppEntryPage";
    }
  }

  private initializeOptions(): void {
    this.options = new Array<SideMenuOption>();
    this.options.push({
      iconName: 'map',
      displayText: 'Book a trip',
      component: 'LivePage'
    });
    this.options.push({
      iconName: 'map',
      displayText: 'My Trip',
      component: 'TripPage'
    });
    // this.options.push({
    //   iconName: 'map',
    //   displayText: 'Feedback',
    //   component: 'FeedbackPage'
    // });
  }

  public onOptionSelected(option: SideMenuOption): void {
    this.menuCtrl.close().then(() => {
      if (option.custom && option.custom.isLogin) {
        this.presentAlert('You\'ve clicked the login option!');
      } else if (option.custom && option.custom.isLogout) {
        this.presentAlert('You\'ve clicked the logout option!');
      } else if (option.custom && option.custom.isExternalLink) {
        let url = option.custom.externalUrl;
        window.open(url, '_blank');
      } else {
        const params = option.custom && option.custom.param;
        this.nav.setRoot(option.component, params);
      }
    });
  }

  public collapseMenuOptions(): void {
    this.sideMenu.collapseAllOptions();
  }

  public presentAlert(message: string): void {
    let alert = this.alertCtrl.create({
      title: 'Information',
      message: message,
      buttons: ['Ok']
    });
    alert.present();
  }

  openPage(page, index) {
    if (page.component) {
      this.nav.setRoot(page.component);
      this.menuCtrl.close();
    } else {
      if (this.selectedMenu) {
        this.selectedMenu = 0;
      } else {
        this.selectedMenu = index;
      }
    }
  }

  logout() {
    localStorage.clear();
    localStorage.setItem('count', null)
    this.menuCtrl.close();
    this.nav.setRoot("LoginPage");
  }

  gotoUserPage() {
    this.nav.push("MyProfilePage");
  }

}
