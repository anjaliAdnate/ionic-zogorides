import { NgModule } from '@angular/core';
import { BookingDetailComponent } from './booking-detail/booking-detail';
@NgModule({
	declarations: [BookingDetailComponent],
	imports: [],
	exports: [BookingDetailComponent]
})
export class ComponentsModule {}
