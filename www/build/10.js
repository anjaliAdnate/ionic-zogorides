webpackJsonp([10],{

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RideDetailsPageModule", function() { return RideDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__rideDetails__ = __webpack_require__(522);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RideDetailsPageModule = /** @class */ (function () {
    function RideDetailsPageModule() {
    }
    RideDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__rideDetails__["a" /* RideDetailspage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__rideDetails__["a" /* RideDetailspage */]),
            ],
        })
    ], RideDetailsPageModule);
    return RideDetailsPageModule;
}());

//# sourceMappingURL=rideDetails.module.js.map

/***/ }),

/***/ 522:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RideDetailspage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RideDetailspage = /** @class */ (function () {
    function RideDetailspage(navCtrl, platform, navParams, modalCtrl, elementRef, alertCtrl, geolocation, app_api) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.elementRef = elementRef;
        this.alertCtrl = alertCtrl;
        this.geolocation = geolocation;
        this.app_api = app_api;
        platform.ready().then(function () { });
    }
    RideDetailspage.prototype.ngOnInit = function () { };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], RideDetailspage.prototype, "mapElement", void 0);
    RideDetailspage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-rideDetails',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/ride-Details/rideDetails.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Ride Details</ion-title>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <ion-list>\n\n        <ion-row style="border: 1px solid; border-color: #e0e0e0;">\n\n            <ion-col width-33 style="text-align:center">\n\n                <span style="color: black;font-size:27px;">\n\n                    <ion-icon name="pin" style="color:#07ce82;font-size:27px;"></ion-icon>\n\n                </span>\n\n                <p style="font-size: 11px;"> purple pride square,kalewadi phata Kalewadi, Pune, Maharashtra 411057</p>\n\n            </ion-col>\n\n            <ion-col width-20 style="text-align:center">\n\n                <img src="assets/imgs/arrow.jpeg" width="30" height="35" style="margin-top: 0%">\n\n            </ion-col>\n\n            <ion-col width-33 style="text-align:center">\n\n                <span style="color: black;font-size:27px;">\n\n                    <ion-icon name="pin" style="color:#b60817;font-size:27px;"></ion-icon>\n\n                </span>\n\n                <p style="font-size: 11px;">Hadapsar, Pune, Maharashtra</p>\n\n\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row style="border: 1px solid; border-color: #e0e0e0;margin-top: 8%;">\n\n            <ion-col width-40 style="text-align:center">\n\n                <span style="color: black;font-size:27px;">\n\n                    <ion-icon name="clock" style="color:#07ce82;font-size:27px;"></ion-icon>\n\n                </span>\n\n                <p style="font-size: 11px;"> Thursday October 25, 12:20 pm</p>\n\n            </ion-col>\n\n            <ion-col width-20 style="text-align:center">\n\n                <img src="assets/imgs/arrow.jpeg" width="30" height="35" style="margin-top: 0%">\n\n                <p style="font-size: 11px;color:#e63939;"> 0 hrs 56 min</p>\n\n            </ion-col>\n\n            <ion-col width-40 style="text-align:center">\n\n                <span style="color: black;font-size:27px;">\n\n                    <ion-icon name="clock" style="color:#b60817;font-size:27px;"></ion-icon>\n\n                </span>\n\n                <p style="font-size: 11px;"> Thursday October 25, 1:14 pm</p>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row style="border: 1px solid; border-color: #e0e0e0;margin-top: 8%;">\n\n            <ion-col width-33>\n\n                <img src="assets/imgs/milestone-png-1.png" width="25" height="30"\n\n                    style="margin-top: 9%;margin-left:34%;">\n\n            </ion-col>\n\n\n\n            <ion-col width-33>\n\n                <p style="font-size: 18px;margin-top: 15%;text-align: center;"> 18.6 Km</p>\n\n            </ion-col>\n\n            <ion-col width-33>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-list>\n\n\n\n\n\n    <ion-row style="background:#f4f4f4;margin-top:3px;">\n\n        <ion-col width-33>\n\n            <img src="assets/imgs/rupees2.png" width="50" height="50" style="margin-top:3%;margin-left:25%;">\n\n        </ion-col>\n\n\n\n        <ion-col width-33>\n\n            <p style="text-align:center;font-size:18px;">\n\n                <span style="color: green;font-size:18px;font-weight: 600;"> 120</span>\n\n            </p>\n\n        </ion-col>\n\n        <ion-col width-33>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <ion-row style="width:100%;margin:0%;height:12%;background:#87c23f;">\n\n        <ion-col>\n\n            <button ion-button full style="margin: 0%;height:50px;background:#87c23f;font-size: 14px"\n\n                (click)="Payment()">Make Payment</button>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/ride-Details/rideDetails.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], RideDetailspage);
    return RideDetailspage;
}());

//# sourceMappingURL=rideDetails.js.map

/***/ })

});
//# sourceMappingURL=10.js.map