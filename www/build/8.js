webpackJsonp([8],{

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RideTotalAmountPageModule", function() { return RideTotalAmountPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ride_total_amount__ = __webpack_require__(523);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RideTotalAmountPageModule = /** @class */ (function () {
    function RideTotalAmountPageModule() {
    }
    RideTotalAmountPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__ride_total_amount__["a" /* RideTotalAmountPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__ride_total_amount__["a" /* RideTotalAmountPage */]),
            ],
        })
    ], RideTotalAmountPageModule);
    return RideTotalAmountPageModule;
}());

//# sourceMappingURL=ride-total-amount.module.js.map

/***/ }),

/***/ 523:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RideTotalAmountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RideTotalAmountPage = /** @class */ (function () {
    function RideTotalAmountPage(navCtrl, navParams, apiCall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        if (navParams.get("fareTime")) {
            this.fareTime = navParams.get("fareTime");
            localStorage.setItem("fareTime", this.fareTime);
        }
        else {
            if (this.fareTime == undefined) {
                this.fareTime = JSON.parse(localStorage.getItem("fareTime"));
            }
        }
    }
    RideTotalAmountPage.prototype.ionViewDidLoad = function () {
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        var minu = new Date(this.fareTime).getMinutes();
        this.rideTime = minu;
        this.rideFare(userId);
    };
    RideTotalAmountPage.prototype.makePayment = function () {
        var _this = this;
        var useDetails = JSON.parse(localStorage.getItem('details'));
        var userId = useDetails._id;
        this.apiCall.paytmValidation(userId)
            .subscribe(function (res) {
            if ((res.Status == "Token Not Found") || (res.message == "Invalid Token") || (res.status == "FAILURE")) {
                _this.navCtrl.push("PaytmwalletloginPage");
            }
            else if ((res.mobile) && (res.id)) {
                _this.getPaytmBalance(userId);
                var paytmNumber = res.mobile;
                localStorage.setItem('paytmregNum', paytmNumber);
            }
        });
    };
    RideTotalAmountPage.prototype.getPaytmBalance = function (uId) {
        var _this = this;
        this.apiCall.paytmbalance(uId)
            .subscribe(function (res) {
            if (res.code == "ETIMEDOUT") {
                _this.toasterror(res.code);
            }
            else {
                localStorage.setItem('paytmBalance', JSON.stringify(res));
                _this.navCtrl.push("WalletPage");
            }
        });
    };
    RideTotalAmountPage.prototype.toasterror = function (errstatus) {
        var toastCtrl = this.toastCtrl.create({
            duration: 3000,
            position: 'top',
            cssClass: 'toastStyle'
        });
        if (errstatus == "ETIMEDOUT") {
            toastCtrl['message'] = "Internal Server error";
            toastCtrl.present();
        }
    };
    RideTotalAmountPage.prototype.rideFare = function (userId) {
        var _this = this;
        this.apiCall.rideFare(userId)
            .subscribe(function (res) {
            _this.amtToBePaid = res.billAmount;
            localStorage.setItem("tripFare", JSON.stringify(_this.amtToBePaid));
            localStorage.removeItem("remainingTime");
        });
    };
    RideTotalAmountPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ride-total-amount',template:/*ion-inline-start:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/ride-total-amount/ride-total-amount.html"*/'<ion-content no-padding>\n\n  <div width="100%" height="100%"></div>\n\n  <div style="background: #8eda4a;padding-top: 15%;padding-bottom: 10%;" height="50%">\n\n      <ion-card style="margin: 0px;width: 28%;border-radius: 15px; padding-top: 0%;margin-left: 37%;">\n\n          <img src="assets/icon/zogologo.png" style="width: 60%;margin: 18%;">\n\n      </ion-card>\n\n      <h5 style="color: white;font-weight: 500;text-align: center;font-size: 2.4rem;padding-top: 0%;padding-bottom: 7%;">RIDE COMPLETED</h5>\n\n  </div>\n\n  <div class="row">\n\n  <ion-card style="width: 60%;height: 20%;margin-top: -54px;margin-left: 22%;border-radius: 8%;" class="col-sm-12 col-12 col-md-12">\n\n    <p style="text-align: center;font-size: 15px; margin-top: 12%;">Amount to be paid :</p>\n\n    <h4\n\n      style="text-align: center;color: #8eda4a;font-family: fantasy;font-size: 36px;margin-top: 6px;letter-spacing: 3px;">\n\n      <i class="fa fa-rupee"></i> {{amtToBePaid}}</h4>\n\n  </ion-card>\n\n</div>\n\n  <ion-grid no-padding>\n\n    <img src="assets/icon/rideComplete.png" style="width: 50%;margin: 3% 0% 0% 24%;">\n\n  </ion-grid>\n\n</ion-content>\n\n<ion-footer style="width: 90%;\n\nheight: 100px;\n\nbackground: #ffffff;\n\nmargin-left: 5%;\n\nborder-top-left-radius: 8px;\n\nborder-top-right-radius: 8px;\n\nbox-shadow: 0px 1px 15px 3px #cfcfcf;">\n\n  <ion-row style="margin-top: 22px;width: 90%;margin-left: 5%;">\n\n    <button ion-button full style="margin: 0%;height:50px;background:#87c23f;font-size: 14px;border-radius: 6px;"\n\n      (click)="makePayment()">Make Payment</button>\n\n  </ion-row>\n\n</ion-footer>'/*ion-inline-end:"/Users/admin/Desktop/IONIC_PROJECTS/IONIC_APPS/zogoridesIonicApp/src/pages/ride-total-amount/ride-total-amount.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], RideTotalAmountPage);
    return RideTotalAmountPage;
}());

//# sourceMappingURL=ride-total-amount.js.map

/***/ })

});
//# sourceMappingURL=8.js.map